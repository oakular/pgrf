<?php

use Illuminate\Http\Request;
use Illuminate\Http\FileHelpers;
use Illuminate\Filesystem\Filesystem;

/// ----- AUTH ROUTES -----
Route::get('login/', 'Auth\LoginController@showLoginForm')->name('login');
Route::get('login/success', 'Auth\LoginController@login')->name('login.success');

/// ----- HOME ROUTES -----
Route::redirect('/', 'login');
Route::get('/home', 'HomeController@show')->name('home');
/*      ->middleware('lucs.login')->name('home'); */
Route::redirect('intrahome', 'http://intranet.csc.liv.ac.uk' )->name('intrahome');

/// ----- USER ROUTES -----
Route::resource('user', 'UserController', ['only' => [
    'index'
]]);

/// ----- STUDENT ROUTES -----
Route::resource('student', 'StudentController', ['except' => [
    'destroy'
]])->middleware('lucs.login');
Route::post('student/bulk', 'StudentController@storeBulk')->name('student.bulk.store');
Route::get('student/bulk/create', 'StudentController@createBulk')->name('student.bulk.create');

/// ----- PHD PLAN ROUTES -----
Route::resource('plan/approval', 'PhdPlanApprovalController', ['only' => [
    'create', 'store'
]]);

/// ----- WORKFLOW ROUTES -----
Route::get('workflow/create/{student}', 'WorkflowController@create')->name('workflow.create');
Route::resource('workflow', 'WorkflowController', ['only' => [
    'store', 'update', 'destroy'
]]);

/// ----- WORKFLOW TYPE ROUTES -----
Route::resource('workflow_type', 'WorkflowTypeController', [ 'only' => [
    'show'
]]);

/// ----- FEEDBACK ROUTES -----
Route::name('feedback.')->group(function() {
    Route::get('{userId}/feedback', 'FeedbackController@create')->name('enter');
    Route::get('{feedback}/{studentId}/feedback/', 'FeedbackController@show')->name('view');
    Route::post('feedback/upload', 'FeedbackController@store')->name('upload');
    Route::get('feedback/committee/comments/create/{feedback}', 'DataForProgressCommitteeController@create')->name('committee_data.create');
    Route::post('feedback/committee/comments/store', 'DataForProgressCommitteeController@store')->name('committee_data.store');
    Route::get('feedback/committee/comments/show/{data}', 'DataForProgressCommitteeController@show')->name('committee_data.show');
});

/// ----- DOCUMENT ROUTES -----
Route::resource('document', 'DocumentController', ['only' => [
    'index', 'store', 'show'
]]);

/// ----- SUPERVISOR ROUTES -----
Route::name('supervisor.')->group(function() {
    Route::delete('supervisor/{staff}/{student}', 'SupervisorController@destroy')->name('destroy');
    Route::post('supervisor/', 'SupervisorController@store')->name('store');
});

/// ----- IPAP ROUTES -----
Route::name('ipap.')->group(function() {
    Route::delete('ipap/{staff}/{student}', 'IPAPController@destroy')->name('destroy');
    Route::post('ipap/', 'IPAPController@store')->name('store');
});

Route::resource('meeting', 'IPAPMeetingController', ['only' => [
    'store', 'edit', 'update'
]]);

/// ----- STAFF VIEW ROUTES -----
Route::name('staff_views.')->group(function() {
    Route::get('students/ipap/show', 'StaffViewsController@showIpapStudentsView')->name('students.ipap.show');
    Route::get('students/supervising/show', 'StaffViewsController@showSupervisingStudentsView')->name('students.supervising.show');
});

/// ----- MASQUERADE ROUTES -----
Route::get('masquerade/{username}', 'MasqueradeController@masqueradeAs')->name('masquerade');
Route::get('stop/masquerade', 'MasqueradeController@stopMasquerade')->name('masquerade.stop');

/// ----- EXEMPTION ROUTES -----
Route::get('exemption/{student_username}', 'ExemptionController@request')->name('exemption.request');
Route::get('exemption/grant/{student_username}', 'ExemptionController@grant')->name('exemption.grant');

?>

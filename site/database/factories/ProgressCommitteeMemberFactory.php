<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\ProgressCommitteeMember;
use Faker\Generator as Faker;

$factory->define(ProgressCommitteeMember::class, function (Faker $faker) {
    return [
        'username' => str_random(5),
        'firstnames' => $faker->firstName(),
        'surname' => $faker->lastName,
        'email' => $faker->email
    ];
});

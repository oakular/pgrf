<?php

use Faker\Generator as Faker;

$factory->define(App\Questionnaire::class, function (Faker $faker) {
    return [
        'workflow_type_id' => rand(0,6),
        'school_id' => rand(0,1)
    ];
});

$factory->afterCreating(App\Questionnaire::class, function (\App\Questionnaire $questionnaire, $faker) {
    $questions = factory(\App\Question::class, 5)->create();
    
    foreach ($questions as $question) {
        $questionnaire->questions()->attach($question->id);
    }
});

<?php

use Faker\Generator as Faker;

$factory->define(App\Staff::class, function (Faker $faker) {
    $statuses = ['prof', 'academic'];
    
    return [
        'username' => str_random(5),
        'office' => $faker->buildingNumber,
        'status' => $statuses[rand(0,1)]
    ];
});

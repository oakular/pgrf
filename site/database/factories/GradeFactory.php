<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Grade;
use Faker\Generator as Faker;

$factory->define(App\Grade::class, function (Faker $faker) {
    return [
        'value' => $faker->word,
        'feedback_id' => rand(0,2),
        'student_detail_username' => rand(0,2)
    ];
});

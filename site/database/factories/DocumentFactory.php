<?php

use Faker\Generator as Faker;

$factory->define(App\Document::class, function (Faker $faker) {
    return [
        'student_username' => str_random(5),
        'Location' => str_random(10),
        'workflow_type_id' => rand(1,4)
    ];
});

$factory->afterMaking(App\Document::class, function ($document) {
    $type = factory(\App\WorkflowType::class)->create();

    $document->type()->associate($type)->save();
});

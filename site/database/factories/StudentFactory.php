<?php

use Faker\Generator as Faker;

$factory->define(App\Student::class, function (Faker $faker) {
    return [
        'username' => function () {
            return factory(\App\LUCS_User::class)->create()->username;
        },
        'office' => $faker->buildingNumber,
        'status' => 'phd'
    ];
});

$factory->afterMaking(App\Student::class, function (\App\Student $student, $faker) {
    $student->detail()->save(factory(\App\StudentDetail::class)->make());
    $student->workflows()->save(factory(\App\Workflow::class)->make());
});

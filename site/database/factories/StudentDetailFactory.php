<?php

use Faker\Generator as Faker;

$factory->define(App\StudentDetail::class, function (Faker $faker) {
    return [
        'username' => str_random(5),
        'StartDate' => $faker->dateTimeBetween('- 3 years', '2018-03-18'),
        'YearOfStudy' => rand(1, 3),
        'status_id' => rand(1, 8),
        'department_id' => rand(1,2)
    ];
});

$factory->state(App\StudentDetail::class, 'XJTLU', [
    'is_xjtlu' => true
]);

$factory->state(App\StudentDetail::class, 'PartTime', [
    'is_part_time' => true
]);

<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\IPAPMeeting;
use Faker\Generator as Faker;

$factory->define(IPAPMeeting::class, function (Faker $faker) {
    return [
        'date' => $faker->dateTimeBetween('now', '+9 months'),
        'workflow_id' => rand(0,9),
    ];
});

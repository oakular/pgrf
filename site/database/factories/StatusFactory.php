<?php

use Faker\Generator as Faker;

$factory->define(App\Status::class, function (Faker $faker) {
    return [
        'name' => title_case($faker->words(rand(1,3), true))
    ];
});

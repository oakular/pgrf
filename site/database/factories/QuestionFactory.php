<?php

use Faker\Generator as Faker;

$factory->define(App\Question::class, function (Faker $faker) {
    return [
        'value' => $faker->sentences(1, true),
        'input_type_id' => rand(0,4)
    ];
});

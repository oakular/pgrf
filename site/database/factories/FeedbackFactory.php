<?php

use Faker\Generator as Faker;

$factory->define(App\Feedback::class, function (Faker $faker) {
    return [
        'DocumentId' => rand(0,5),
        'Location' => str_random(8),
        'staff_username' => str_random(5)
    ];
});

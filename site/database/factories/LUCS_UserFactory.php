<?php

use Faker\Generator as Faker;

$factory->define(App\LUCS_User::class, function (Faker $faker) {
    return [
        'username' => str_random(5),
        'firstnames' => $faker->firstName(),
        'surname' => $faker->lastName,
        'email' => $faker->email
    ];
});

$factory->state(\App\LUCS_User::class, 'Admin', []);
$factory->state(\App\LUCS_User::class, 'Staff', []);

$factory->afterCreatingState(App\LUCS_User::class, 'Admin', function ($user, $faker) {
    \App\Admin::create([
        'username' => $user->username
    ]);
});

$factory->afterCreatingState(App\LUCS_User::class, 'Staff', function ($user, $faker) {
    $user->staff()->save(factory(\App\Staff::class)->create());
});

<?php

use Faker\Generator as Faker;

$factory->define(App\WorkflowType::class, function (Faker $faker) {
    return [
        'value' => str_random(8),
        'requires_feedback' => rand(0,1)
    ];
});

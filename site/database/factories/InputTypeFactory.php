<?php

use Faker\Generator as Faker;

$factory->define(App\InputType::class, function (Faker $faker) {
    return [
        'value' => str_random(8)
    ];
});

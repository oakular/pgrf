<?php

use Faker\Generator as Faker;

$factory->define(App\IPAP::class, function (Faker $faker) {
    return [
        'staff_username' => str_random(5),
        'student_username' => str_random(5)
    ];
});

<?php

use Faker\Generator as Faker;

$factory->define(App\Workflow::class, function (Faker $faker) {
    return [
        'workflow_type_id' => rand(1,4),
        'student_username' => str_random(5),
        'status_id' => rand(1,9),
        'deadline' =>  $faker->dateTimeBetween('now', '+ 6 months'),
    ];
});

$factory->state(\App\Workflow::class, 'IPAP Meeting', []);

$factory->afterCreatingState(\App\Workflow::class, 'IPAP Meeting', function ($wflow, $faker) {
    $wflow->ipapMeeting()->save(factory(\App\IPAPMeeting::class)->make());
});

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateGradeToNullableInStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Student', function (Blueprint $table) {
            $table->dropColumn('Grade');
        });

        Schema::table('Student', function (Blueprint $table) {
            $table->enum('Grade', ['Excellent', 'Good', 'Satisfactory'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Student', function (Blueprint $table) {
            $table->dropColumn('Grade');
        });

        Schema::table('Student', function (Blueprint $table) {
            $table->enum('Grade', ['Excellent', 'Good', 'Satisfactory']);
        });
    }
}

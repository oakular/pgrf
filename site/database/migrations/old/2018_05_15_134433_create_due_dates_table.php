<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDueDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('due_dates', function (Blueprint $table) {
            $table->increments('id');
            $table->char('username', 16);
            $table->date('IPAPMeetingDate')->nullable();
            $table->date('EndOfYearReport')->nullable();
            $table->boolean('IPAPMeetingCompleted')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('due_dates');
    }
}

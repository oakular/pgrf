<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProjectPlanDateToDueDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('DueDates', function (Blueprint $table) {
            $table->date('ProjectPlan')->after('EndOfYearReport');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('DueDates', function (Blueprint $table) {
            $table->dropColumn('ProjectPlan');
        });
    }
}

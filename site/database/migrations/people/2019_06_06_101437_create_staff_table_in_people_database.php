<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffTableInPeopleDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('people')->hasTable('staff')) {
            Schema::connection('people')->create('staff', function (Blueprint $table) {
                $table->char('username', 16)->primary('username');
                $table->char('office', 20)->nullable();
                $table->enum('status', [
                    'prof', 'academic', 'ra', 'phd', 'hon', 'visitor', 'support', 'gone', 'other'
                ])->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('people')->dropIfExists('staff');
    }
}

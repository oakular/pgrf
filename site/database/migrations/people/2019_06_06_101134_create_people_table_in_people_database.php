<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTableInPeopleDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('people')->hasTable('people')) {
            Schema::connection('people')->create('people', function (Blueprint $table) {
                $table->char('username', 16)->primary('username');
                $table->string('firstnames', 50)->nullable();
                $table->string('surname', 20)->nullable();
                $table->string('email', 50)->unique()->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('people')->dropIfExists('people');
    }
}

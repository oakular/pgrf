<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsCompletedColumnToIpapMeetingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ipap_meetings', function (Blueprint $table) {
            $table->boolean('is_completed')->default(false)->after('workflow_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ipap_meetings', function (Blueprint $table) {
            $table->dropColumn('is_completed');
        });
    }
}

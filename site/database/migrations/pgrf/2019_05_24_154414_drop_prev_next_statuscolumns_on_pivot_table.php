<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropPrevNextStatuscolumnsOnPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('status_workflow_type', function (Blueprint $table) {
            $table->dropColumn('prev_status_id');
            $table->dropColumn('next_status_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('status_workflow_type', function (Blueprint $table) {
            $table->integer('prev_status_id');
            $table->integer('next_status_id');
        });
    }
}

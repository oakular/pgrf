<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhDPlanApprovalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phd_plan_approvals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('plan_description');
            $table->text('resource_description')->nullable();
            $table->date('risk_assessment_date')->nullable();
            $table->boolean('is_confirmed');
            $table->integer('workflow_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phd_plan_approvals');
    }
}

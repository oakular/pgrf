<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusWorkflowTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_workflow_type', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status_id');
            $table->integer('workflow_type_id');
            $table->integer('prev_status_id');
            $table->integer('next_status_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status_workflow_type');
    }
}

<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = factory(App\LUCS_User::class)->create([
            'username' => 'cal',
            'firstnames' => 'Admin',
            'surname' => 'User',
            'email' => 'admin@liv.ac.uk'
        ]);

        \App\Admin::create([
            'username' => $user->username
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class ProgressCommitteeRecommendationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $recommendationsArray = ['3 month reassessment', '6 month reassessment'];

        foreach ($recommendationsArray as $recommendationValue) {
            $recommendation = new \App\ProgressCommitteeRecommendation;
            $recommendation->value = $recommendationValue;
            $recommendation->save();
        }
    }
}

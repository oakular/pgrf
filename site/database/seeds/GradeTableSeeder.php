<?php

use Illuminate\Database\Seeder;

class GradeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gradingArray = ['Excellent', 'Good', 'Satisfactory', 'Unsatisfactory'];

        foreach ($gradingArray as $gradeValue) {
            $grade = new \App\Grade;
            $grade->value = $gradeValue;
            $grade->save();
        }
    }
}

<?php

use Illuminate\Database\Seeder;

class WorkflowTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = array(
            0 => 'PhD Plan',
            1 => 'Year 1 End of Year Report',
            2 => 'Year 2 End of Year Report',
            3 => 'Year 3 End of Year Report',
            4 => 'PhD Workshop',
            5 => 'CS Research Presentation'
        );

        $statuses = array(
            'PhD Plan' => range(1,9),
            'Year 1 End of Year Report' => array_merge(range(1,9), [12, 13]),
            'Year 2 End of Year Report' => array_merge(range(1,9), [12, 13]),
            'Year 3 End of Year Report' => array_merge(range(1,9), [12, 13]),
            'PhD Workshop' => array_merge([1,2,14], range(6,8)),
            'CS Research Presentation' => array_merge([1,2], range(15,18)),
        );

        foreach ($types as $type_value)
        {
            $type = new \App\WorkflowType;
            $type->value = $type_value;

            if ($type_value == $types[5]) {
                $type->requires_feedback = false;
            }

            $type->save();

            $type->statuses()->saveMany(\App\Status::find($statuses[$type_value]));
            $questionnaire = factory(\App\Questionnaire::class)->create();
            $type->questionnaire()->save($questionnaire);
        }
    }
}

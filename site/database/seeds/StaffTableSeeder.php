<?php

use Illuminate\Database\Seeder;

class StaffTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $staff = factory(App\LUCS_User::class, 100)->create();

        $staff->each(function ($member) {
            $member->staff()->save(factory(\App\Staff::class)->make());
        });
    }
}

<?php

use Illuminate\Database\Seeder;

class DocumentTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $doc_types = array(
            1 => 'PhD Plan',
            2 => 'End of Year Report',
            3 => 'CS Workshop'
        );

        foreach ($doc_types as $idx=>$value) {
            DB::table('document_types')->insert([
                'id' => $idx,
                'value' => $value
            ]);
        }
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusTableSeeder extends Seeder
{
    /**
     * Seed the Statuses for Workflows
     *
     * @return void
     */
    public function run()
    {
        $statuses = array(
            // Report, PhD Plan
            1 => 'Not Due',
            2 => 'Due',
            3 => 'Reminder Sent',
            4 => 'Report Submitted',
            5 => 'Report Late',
            6 => 'Awaiting IPAP Feedback',
            7 => 'IPAP Feedback Incomplete',
            8 => 'IPAP Feedback Complete',
            9 => 'Needs Attention of Progress Committee',

            // Report
            12 => 'Exemption Requested',
            13 => 'Exemption Granted',


            // PhD Workshop
            14 => 'Slides Submitted',

            // Research Presentation
            15 => 'Form Submitted',
            16 => 'Form Late',
            17 => 'Form Complete and Presentation Incomplete',
            18 => 'Form Complete and Presentation Complete'
        );

        foreach ($statuses as $idx=>$status) {
            DB::table('status')->insert([
                'id' => $idx,
                'name' => $status
            ]);
        }
    }
}

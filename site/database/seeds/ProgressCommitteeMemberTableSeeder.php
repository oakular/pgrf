<?php

use Illuminate\Database\Seeder;

class ProgressCommitteeMemberTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\ProgressCommitteeMember::class, 8)->create();
    }
}

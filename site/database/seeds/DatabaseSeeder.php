<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            AdminSeeder::class,
            ProgressCommitteeMemberTableSeeder::class,
            ProgressCommitteeRecommendationTableSeeder::class,
            DepartmentTableSeeder::class,
            StaffTableSeeder::class,
            WorkflowTypesTableSeeder::class,
            StatusTableSeeder::class,
            StudentTableSeeder::class,
        ]);
    }
}

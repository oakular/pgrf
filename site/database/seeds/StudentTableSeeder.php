<?php

use Illuminate\Database\Seeder;

class StudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $staffRelationships = function ($user) {
            $supervisors = \App\Staff::all()->random(2);

            $supervisors->each(function ($supervisor) use ($user) {
                $user->supervisors()->attach($supervisor->username);
            });

            $ipaps = \App\Staff::all()->random(2);

            $ipaps->each(function ($ipap) use ($user) {
                $user->ipaps()->attach($ipap->username);
            });

        };
        
        factory(App\Student::class, 25)->create()->each($staffRelationships);
        $studentsXjtlu = factory(App\Student::class, 15)->create()->each($staffRelationships);
        $studentsPartTime = factory(App\Student::class, 5)->create()->each($staffRelationships);

        $studentsXjtlu->each(function ($student) {
            $student->detail->is_xjtlu = true;
            $student->detail->save();
        });

        $studentsPartTime->each(function ($student) {
            $student->detail->is_part_time = true;
            $student->detail->save();
        });

        $allStudents = \App\Student::all();
        $allStudents->each(function ($student) {
            $student->workflows()->delete();

            switch ($student->detail->YearOfStudy) {
            case 1:
                $student->workflows()->save(
                    factory(\App\Workflow::class)->create([
                        'workflow_type_id' => \App\WorkflowType::where('value', 'LIKE', 'PhD Plan')->first()->id
                    ])
                );
                
                $student->workflows()->save(
                    factory(\App\Workflow::class)->create([
                        'workflow_type_id' => \App\WorkflowType::where('value', 'LIKE', 'Year 1 End of Year Report')->first()->id
                    ])
                );
                break;
            case 2:
                $student->workflows()->save(
                    factory(\App\Workflow::class)->create([
                        'workflow_type_id' => \App\WorkflowType::where('value', 'LIKE', 'Year 2 End of Year Report')->first()->id
                    ])
                );
                
                $student->workflows()->save(
                    factory(\App\Workflow::class)->create([
                        'workflow_type_id' => \App\WorkflowType::where('value', 'LIKE', 'PhD Workshop')->first()->id
                    ])
                );
                break;
            case 3:
                $student->workflows()->save(
                    factory(\App\Workflow::class)->create([
                        'workflow_type_id' => \App\WorkflowType::where('value', 'LIKE', 'Year 3 End of Year Report')->first()->id
                    ])
                );
                break;
            }
        });
    }

    private function createMyStudent()
    {
        $user = factory(App\LUCS_User::class)->create([
            'username' => 'u5cw1',
            'firstnames' => 'Callum',
            'surname' => 'Warrilow',
            'email' => 'sgcwarri@liv.ac.uk',
        ]);

        $this->createStudent($user);

        $staff = App\Staff::whereIn(
            'username',
            App\LUCS_User::where('username', 'rbc')->pluck('username')->toArray()
        )->first();

        $student = $user->student;
        $student->detail->YearOfStudy = 3;
        $student->detail->save();
        $workflow_type = factory(\App\WorkflowType::class)->create([
            'value' => 'End of Year Report'
        ]);
        $student->workflow->type()->associate($workflow_type)->save();
        $student->save();

        $student->supervisors()->save(factory(App\Supervisor::class)->make([
            'student_username' => $student->username,
            'staff_username' => $staff->username
        ]));

        $staff = App\Staff::whereIn(
            'username',
            App\LUCS_User::where('username', 'adm')->pluck('username')->toArray()
        )->first();

        $student->ipaps()->save(factory(App\IPAP::class)->make([
            'student_username' => $student->username,
            'staff_username' => $staff->username
        ]));
    }

    private function createStudent($user)
    {
        $user->student()->save(factory(App\Student::class)->make([
            'username' => $user->username
        ]));
    }
}

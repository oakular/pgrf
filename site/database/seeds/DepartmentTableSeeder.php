<?php

use Illuminate\Database\Seeder;

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $department_names = array('CS', 'EEE');

        foreach ($department_names as $department_name) {
            factory(\App\Department::class)->create([
                'value' => $department_name
            ]);
        }
    }
}

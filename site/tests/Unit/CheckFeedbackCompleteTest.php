<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;use Illuminate\Support\Facades\Notification;

class CheckFeedbackCompleteTest extends TestCase
{
    use RefreshDatabase;

    private $student;
    private $staff;

    protected function setUp():void
    {
        parent::setUp();
        Notification::fake();
        
        $this->staff = factory(\App\Staff::class, 2)->create();
        $this->student = factory(\App\Student::class)->create();
        foreach ($this->staff as $ipap) {
            $this->student->ipaps()->attach($ipap);
        }

        factory(\App\Status::class, 9)->create();

        $status = \App\Status::all()->last();
        $status->name = 'IPAP Feedback Complete';
        $status->save();
        
        $this->student->workflows->first()->status()->associate(
            \App\Status::all()->first()
        )->save();

        $type = factory(\App\WorkflowType::class)->create();
        $this->student->workflows->first()->type()->associate($type)->save();

        factory(\App\LUCS_User::class, 2)->create();
    }
    
    /**
     * A basic test example.
     * @dataProvider jobDispatchConfig
     * @return void
     */
    public function testFeedbackLateNotificationSendsCorrectly($submission_date, $should_send)
    {
        Notification::fake();
        
        $staff = factory(\App\Staff::class, 2)->create();
        $student = factory(\App\Student::class)->create();
        foreach ($staff as $ipap) {
            $student->ipaps()->attach($ipap->username);
        }

        $student->documents()->save(factory(\App\Document::class)->create([
            'created_at' => date_create($submission_date)
        ]));

        $job = new \App\Jobs\CheckFeedbackComplete($student->documents);
        $job->handle();
        
        if ($should_send) {
            Notification::assertSentTo($staff, \App\Notifications\FeedbackLate::class);
        } else {
            Notification::assertNotSentTo($staff, \App\Notifications\FeedbackLate::class);
        }
    }

    public function jobDispatchConfig()
    {
        return array(
            ['-2 weeks', true],
            ['-1 week', false],
        );
    }

    public function testFeedbackLateNotificationDoesNotSendIfFeedbackSubmitted()
    {
        $document = factory(\App\Document::class)->create([
            'created_at' => date_create('-2 weeks')
        ]);
        $document->type()->associate($this->student->workflows->first()->type)->save();
        $this->student->documents()->save($document);
        
        factory(\App\Feedback::class)->create([
            'staff_username' => $this->staff->first()->username,
            'DocumentId' => $this->student->documents->first()->id
        ]);

        $job = new \App\Jobs\CheckFeedbackComplete($this->student->documents);
        $job->handle();

        Notification::assertNotSentTo($this->staff->first(), \App\Notifications\FeedbackLate::class);
        Notification::assertSentTo($this->staff->last(), \App\Notifications\FeedbackLate::class);
    }
}

<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
class DatabaseTest extends TestCase
{
    use RefreshDatabase;

    protected $connectionsToTransact = ['people', 'pgrf'];

    public function testInsertIntoQuestionnaire()
    {
        $questionnaires = factory(\App\Questionnaire::class, 5)->create();

        foreach ($questionnaires as $questionnaire)
        {
            $this->assertDatabaseHas('questionnaires', $questionnaires->first()->toArray(), 'pgrf');    
        }
    }

    public function testInsertIntoQuestion()
    {
        $questions = factory(\App\Question::class, 5)->create();

        foreach ($questions as $question)
        {
            $this->assertDatabaseHas('pgrf.questions', $questions->first()->toArray(), 'pgrf');    
        }
    }

    public function testQuestionCanAccessInputType()
    {
        $input_type = factory(\App\InputType::class)->create();

        $question = factory(\App\Question::class)->create([
            'input_type_id' => $input_type->first()->id
        ]);

        $this->assertEquals($question->inputType->id, $input_type->first()->id);
    }

    public function testQuestionnaireCanAccessDocumentType()
    {
        $workflow_type = factory(\App\WorkflowType::class)->create();

        $questionnaire = factory(\App\Questionnaire::class)->create();
        $questionnaire->workflowType()->associate($workflow_type)->save();
        
        $this->assertEquals($questionnaire->workflowType->id, $workflow_type->id);
    }

    public function testWorkflowTypeCanAccessQuestionnaires()
    {
        $workflow_type = factory(\App\WorkflowType::class)->create();

        $questionnaire = factory(\App\Questionnaire::class)->create([
            'workflow_type_id' => $workflow_type->id
        ]);
        
        $this->assertEquals($questionnaire->id, $workflow_type->questionnaire->id);
    }

    public function testDocumentInsertion()
    {
        $doc = \App\Document::create([
            'student_username' => 'test',
            'Location' => 'testPath',
            'workflow_type_id' => 1
        ]);

        $this->assertDatabaseHas('documents', $doc->toArray());
    }

    public function testDocumentCanAccessType()
    {
        $type = factory(\App\WorkflowType::class)->create([
            'value' => 'End of Year Report'
        ]);
        
        $doc = \App\Document::create([
            'student_username' => 'test',
            'Location' => 'testPath',
            'workflow_type_id' => $type->first()->id
        ]);

        $this->assertEquals($type->first()->id, $doc->type->id);
    }

}

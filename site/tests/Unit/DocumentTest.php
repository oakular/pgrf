<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Notification;
use Illuminate\Foundation\Testing\RefreshDatabase;
class DocumentTest extends TestCase
{
    use WithoutMiddleware;
    use RefreshDatabase;
    
    private $file;
    private $user;



    /** @beforeClass */
    protected function setUp():void
    {
        parent::setUp();

        Storage::fake('public');
        Notification::fake();
        
        $this->workflow_types = factory(\App\WorkflowType::class, 4)->create();
        $this->workflow_types->first()->value = 'PhD Plan';
        $this->workflow_types->first->save();

        $this->user = factory(\App\LUCS_User::class)->create();
        
        factory(\App\Student::class)->create([
            'username' => $this->user->username
        ]);
        $this->user->student->detail->YearOfStudy = 1;
        $this->user->student->detail->save();
        
        $status = factory(\App\Status::class)->create();
        $this->user->student->workflows->first()->status()->associate(
            \App\Status::all()->first()
        )->save();

        $this->user->student->workflows->first()->type()->associate(
            $this->workflow_types->first()
        )->save();

        $ipaps = factory(\App\LUCS_User::class)->create();
        $ipaps->each(function ($ipap) {
            $this->user->student->ipaps()->attach($ipap->username);
        });
        
        $this->file = UploadedFile::fake()->create('document.pdf', 2000);
    }

    /**
     * @after
     */
    public function tearDown():void
    {
        $this->user->delete();
        $this->file = null;
        
        parent::tearDown();
    }
    
    public function testUserCanUploadDocument()
    {
        Event::fake();

        $response = $this->actingAs($this->user)->json('POST', '/document', [
            'documentType' => $this->workflow_types->last()->id,
            'document' => $this->file
        ]);

        $response->assertRedirect('home');

        Event::assertDispatched(\App\Events\DocumentSubmitted::class);
    }

    public function testFirstYearUserCanUploadPhdPlan()
    {
        Event::fake();
        
        $response = $this->actingAs($this->user)->json('POST', '/document', [
            'documentType' => $this->workflow_types->first()->id,
            'document' => $this->file
        ]);

        $response->assertRedirect('home');
    }

    public function testNonFirstYearUserCannotUploadPhDPlan()
    {
        $this->changeStudentYearOfStudy(2);

        $response = $this->actingAs($this->user)->json('POST', '/document', [
            'documentType' => $this->workflow_types->first()->id,
            'document' => $this->file
        ]);

        // Document upload is forbidden
        $response->assertStatus(403);

        $this->changeStudentYearOfStudy(1);
    }

    public function testStudentCanOnlyStoreOneInstanceOfADocumentType()
    {
        Event::fake();
        
        for ($idx = 0; $idx < 2; $idx++) {
            $response = $this->actingAs($this->user)->json('POST', '/document', [
                'documentType' => $this->workflow_types->last()->id,
                'document' => $this->file
            ]);
            
            $response->assertRedirect('home');
            $this->user = \App\LUCS_User::find($this->user->username);
        }

        $this->user = \App\LUCS_User::find($this->user->username);
        $this->assertEquals(1, $this->user->student->documents->count());
    }

    public function testDocumentRetrieval()
    {
        $this->actingAs($this->user)->json('POST', '/document', [
            'documentType' => $this->workflow_types->last()->id,
            'document' => $this->file
        ]);

        $document = $this->user->student->documents->first;

        $response = $this->actingAs($this->user)->get('/document', ['document' => $document->id]);

        $response->assertSuccessful();
    }

    private function changeStudentYearOfStudy(Int $year)
    {
        $this->user->student->detail->YearOfStudy = $year;
        $this->user->student->detail->save();
    }
}

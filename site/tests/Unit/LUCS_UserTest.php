<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LUCS_UserTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testGetFullNameOrUsername()
    {
        $user = factory(\App\LUCS_User::class)->create();

        $this->assertEquals("{$user->firstnames} {$user->surname}", $user->getFullNameOrUsername());

        $user->firstnames = NULL;
        $user->surname = NULL;

        $this->assertEquals("{$user->username}", $user->getFullNameOrUsername());
    }
    public function testGetFullNameOrUsernameReversed()
    {
        $user = factory(\App\LUCS_User::class)->create();

        $this->assertEquals("{$user->surname} {$user->firstnames}", $user->getFullNameOrUsernameReversed());

        $user->firstnames = NULL;
        $user->surname = NULL;

        $this->assertEquals("{$user->username}", $user->getFullNameOrUsernameReversed());
    }
}

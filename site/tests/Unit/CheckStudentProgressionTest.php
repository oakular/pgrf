<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;use Illuminate\Support\Facades\Notification;

class CheckStudentProgressionTest extends TestCase
{
    use RefreshDatabase;



    /**
     * @dataProvider jobDispatchConfig
     * @return void
     */
    public function testCheckStudentProgressionNotifiesCorrectly($grade_value, $will_be_blocked)
    {
        Notification::fake();

        $student = factory(\App\Student::class)->create();

        $supervisors = factory(\App\Staff::class, 2)->create();

        foreach ($supervisors as $supervisor) {
            $student->supervisors()->attach($supervisor->username);
        }

        $grades = factory(\App\Grade::class, 2)->make();
        $grades->first()->value = 'Unsatisfactory';
        $grades->last()->value = $grade_value;

        $grades->each(function ($item, $key) {
            $item->save();
        });

        $student->detail->grades()->save(\App\Grade::where('value', 'LIKE', $grade_value)->first());

        $to_notify = collect([$student])->push($supervisors);

        $job = new \App\Jobs\CheckStudentProgression($student);
        $job->handle();

        if ($will_be_blocked) {
            Notification::assertSentTo($to_notify, \App\Notifications\ProgressBlocked::class);
        } else {
            Notification::assertSentTo($to_notify, \App\Notifications\ProgressAllowed::class);
        }
    }

    public function jobDispatchConfig()
    {
        return array(
            ['Unsatisfactory', true],
            [str_random(8), false]
        );
    }
}

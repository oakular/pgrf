<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Event;

class CheckDueDocumentsTest extends TestCase
{
    use RefreshDatabase;


    
    /**
     * @dataProvider jobDispatchConfig
     * @return void
     */
    public function testCheckDueDocumentsSendsNotificationCorrectly($workflow_type_value, $deadline, $should_send)
    {
        Notification::fake();
        Event::fake();
                
        $students = factory(\App\Student::class, 5)->create();
        $type = factory(\App\WorkflowType::class)->create();

        $students->each(function ($student) use ($deadline, $type) {
            $student->workflows()->save(factory(\App\Workflow::class)->create([
                'deadline' => $deadline,
                'workflow_type_id' => $type->id
            ]));
        });

        $job = new \App\Jobs\CheckDueDocuments(\App\Workflow::all());
        $job->handle();

        if ($should_send) {
            Notification::assertSentTo($students, \App\Notifications\DocumentDue::class);
        } else {
            Notification::assertNotSentTo($students, \App\Notifications\DocumentDue::class);
        }

    }

    public function jobDispatchConfig()
    {
        $eoy = 'End of Year Report';
        $plan = 'PhD Plan';
             
        return array(
            [$eoy, date_create('+1 month'), true],
            [$plan, date_create('+1 month'), true],
            [$eoy, date_create('+2 months'), false],
            [$plan, date_create('+2 months'), false],
            [$eoy, date_create('+27 days'), false],
            [$plan, date_create('+27 days'), false],
        );
    }

}

<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\RefreshDatabase;
class WorkflowTest extends TestCase
{
    use RefreshDatabase;



    private $workflow;
    private $student;

    protected function setUp(): void
    {
        parent::setUp();

        $statuses = array(
            // Report, PhD Plan
            1 => 'Not Due',
            2 => 'Due',
            3 => 'Reminder Sent',
            4 => 'Report Submitted',
            5 => 'Document Late',
            6 => 'Awaiting IPAP Feedback',
            7 => 'IPAP Feedback Incomplete',
            8 => 'IPAP Feedback Complete',
            9 => 'Needs Attention of Progress Committee',
            10 => 'IPAP Feedback Late',

            // Report
            12 => 'Exemption Requested',
            13 => 'Exemption Granted',


            // PhD Workshop
            14 => 'Slides Submitted',

            // Research Presentation
            15 => 'Form Submitted',
            16 => 'Form Late',
            17 => 'Form complete and presentation incomplete',
            18 => 'Form complete and presentation complete'
        );

        foreach ($statuses as $status_name) {
            factory(\App\Status::class)->create(['name' => $status_name]);
        }
        
        $types = array(
            1 => 'PhD Plan',
            2 => 'End of Year Report',
            3 => 'PhD Workshop',
            4 => 'CS Research Presentation'
        );

        $statuses = array(
            'PhD Plan' => range(1,9),
            'End of Year Report' => array_merge(range(1,9), [12, 13]),
            'PhD Workshop' => array_merge([1,2,14], range(6,8)),
            'CS Research Presentation' => array_merge([1,2], range(15,18)),
        );

        foreach ($types as $type_value)
        {
            $type = new \App\WorkflowType;
            $type->value = $type_value;
            $type->save();
            $type->statuses()->saveMany(\App\Status::find($statuses[$type_value]));
        }

        // $this->workflow = factory(\App\Workflow::class)->create([
        //     'workflow_type_id' => \App\WorkflowType::all()->random(1)->first()->id
        // ]);
        $this->student = factory(\App\Student::class)->create();
        $this->student->workflows->first()->type()->associate(\App\WorkflowType::all()->random(1)->first());
        // $this->student->workflows()->save($this->workflow);
    }

 // /**
 //     * @after
 //     */
 //    protected function tearDown():void
 //    {
 //        $this->workflow->delete();
 //        $this->student->delete();
 //    }


    public function testWorkflowStatusChange()
    {
        $status = \App\Status::all()->last();

        $this->student->workflows->first()->status()->associate($status);

        $this->assertEquals($this->student->workflows->first()->status->id, $status->id);
    }

    /** @dataProvider feedbackStatusProvider */
    public function testFeedbackSubmittedEventChangesStatus($current_status, $next_status)
    {
        $ipap = factory(\App\LUCS_User::class)->states('Staff')->create();
        $this->student->ipaps()->attach($ipap->username);

        $this->student->workflows->first()->status()
            ->associate(\App\Status::where(
                'name', 'LIKE', $current_status)->firstOrFail()
            )->save();
        
        $document = factory(\App\Document::class)->create([
            'student_username' => $this->student->username,
        ]);
        $document->type()->associate($this->student->workflows->first()->type)->save();
        
        $feedback = \App\Feedback::create([
            'staff_username' => $ipap->username,
            'DocumentId' => $document->id,
            'Location' => 'path'
        ]);

        $event = new \App\Events\FeedbackSubmitted($this->student, $feedback);

        $listener = new \App\Listeners\ChangeStatus;

        $listener->handle($event);

        $this->assertEquals(
            \App\Status::where('name', 'LIKE', $next_status)->first()->id,
            \App\Student::find($this->student->username)->workflows->first()->status->id,
            'Failed asserting that ' . $this->student->workflows->first()->status->name . ' is the same as ' . $next_status);
    }

    public function feedbackStatusProvider()
    {
        return [
            ['Awaiting IPAP Feedback', 'IPAP Feedback Incomplete'],
            ['IPAP Feedback Incomplete', 'IPAP Feedback Complete'],
            ['IPAP Feedback Late', 'IPAP Feedback Complete']
        ];
    }

    public function testFeedbackCompleteEventChangesStatus()
    {
        $document = factory(\App\Document::class)->create([
            'student_username' => $this->student->username,
        ]);

        $document->type()->associate($this->student->workflows->first()->type)->save();

        $event = new \App\Events\FeedbackComplete($this->student, $document);

        $listener = new \App\Listeners\ChangeStatus;

        $listener->handle($event);

        $this->assertEquals(
            \App\Status::where('name', 'LIKE', 'IPAP Feedback Complete')->first()->id,
            \App\Student::find($this->student->username)->workflows->first()->status->id
        );
    }

    public function testExemptionGrantedEventChangesStatus()
    {
        
        $event = new \App\Events\ExemptionGranted($this->student->workflows->first());

        $listener = new \App\Listeners\ChangeStatus;

        $listener->handle($event);

        $this->assertEquals(
            \App\Status::where('name', 'LIKE', 'Exemption Granted')->first()->id,
            $this->student->workflows->first()->status->id
        );
    }

    public function testDocumentDueEventChangesStatus()
    {
        $event = new \App\Events\DocumentDue($this->student->workflows->first());

        $listener = new \App\Listeners\ChangeStatus;

        $listener->handle($event);

        $this->assertEquals(
            \App\Status::where('name', 'LIKE', 'Due')->first()->id,
            $this->student->workflows->first()->status->id
        );

    }

    public function testDocumentLateEventChangesStatus()
    {
        $event = new \App\Events\DocumentLate($this->student->workflows->first());

        $listener = new \App\Listeners\ChangeStatus;

        $listener->handle($event);

        $this->assertEquals(
            \App\Status::where('name', 'LIKE', 'Document Late')->first()->id,
            $this->student->workflows->first()->status->id
        );
    }

    public function testExemptionRequestedEventChangesStatus()
    {
        $event = new \App\Events\ExemptionRequested($this->student->workflows->first());

        $listener = new \App\Listeners\ChangeStatus;

        $listener->handle($event);

        $this->assertEquals(
            \App\Status::where('name', 'LIKE', 'Exemption Requested')->first()->id,
            $this->student->workflows->first()->status->id
        );
    }

    public function testDocumentSubmittedEventChangesStatus()
    {
        $document = factory(\App\Document::class)->create();
        $document->type()->associate($this->student->workflows->first()->type);
        
        $event = new \App\Events\DocumentSubmitted($this->student, $document);

        $listener = new \App\Listeners\ChangeStatus;

        $listener->handle($event);

        $this->assertEquals(
            \App\Status::where('name', 'LIKE', 'Report Submitted')->first()->id,
            $this->student->workflows->first()->status->id
        );
    }

    /** @dataProvider workflowIsDueDataProvider */
    public function testWorkflowIsDue($deadline, $expected)
    {
        $workflow = factory(\App\Workflow::class)->create([
            'deadline' =>  $deadline
        ]);

        $this->assertEquals($expected, $workflow->isDue());
    }

    public function workflowIsDueDataProvider()
    {
        return array(
            [date_create('+1 month'), true],
            [date_create('+1 month'), true],
            [date_create('+2 months'), false],
            [date_create('+2 months'), false],
            [date_create('+27 days'), false],
            [date_create('+27 days'), false],
        );
    }

    /** @dataProvider workflowIsLateDataProvider */
    public function testWorkflowIsLate($deadline, $expected)
    {
        $workflow = factory(\App\Workflow::class)->create([
            'deadline' =>  $deadline
        ]);

        $this->assertEquals($expected, $workflow->isLate());
    }

    public function workflowIsLateDataProvider()
    {
        return array(
            [date_create('yesterday'), true],
            [date_create('yesterday'), true],
            [date_create('today'), false],
            [date_create('today'), false],
            [date_create('-2 days'), false],
            [date_create('-2 days'), false],
        );
    }
}

?>

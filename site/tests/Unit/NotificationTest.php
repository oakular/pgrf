<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Notification;
use Illuminate\Foundation\Testing\RefreshDatabase;
class NotificationTest extends TestCase
{
    use RefreshDatabase;



    private $student;
    private $staff;
    private $workflow_type;

    protected function setUp():void
    {
        parent::setUp();

        Notification::fake();
        $user = factory(\App\LUCS_User::class, 2)->create();
        $this->student = factory(\App\Student::class)->create([
            'username' => $user->first()->username
        ]);
        $this->staff = factory(\App\Staff::class)->create([
            'username' => $user->last()->username
        ]);

        $this->workflow_type = factory(\App\WorkflowType::class)->create();

        $this->student->workflows->first()->type()->associate($this->workflow_type)->save();
        
    }

    protected function tearDown():void
    {
        $this->student->delete();
        $this->staff->delete();
        $this->workflow_type->delete();
    }

    /**
     * Test DocumentDue notification sends
     *
     * @return void
     */
    public function testDocumentDueNotificationSends()
    {
        $this->student->notify(new \App\Notifications\DocumentDue($this->workflow_type));

        Notification::assertSentTo(
            $this->student, \App\Notifications\DocumentDue::class,
            function ($notification, $channels) {
                return $notification->workflow_type->id === $this->workflow_type->id;
            }
        );
    }

        
    /**
     * Test DocumentLate notification sends
     *
     * @return void
     */
    public function testDocumentLateNotificationSends()
    {
        $to_notify = collect([$this->student, $this->staff]);
        
        Notification::send($to_notify, new \App\Notifications\DocumentLate($this->workflow_type, $this->student));

        Notification::assertSentTo(
            $to_notify, \App\Notifications\DocumentLate::class,
            function ($notification, $channels) {
                return $notification->workflow_type->id === $this->workflow_type->id;
            }
        );
    }

    /**
     * Test DocumentSubmitted notification sends to both student, and staff
     *
     * @return void
     */
    public function testDocumentSubmittedNotificationSends()
    {
        $to_notify = collect([$this->student, $this->staff])->concat(
            factory(\App\IPAP::class, 2)->create(
                ['student_username' => $this->student->username]
            )
        );

        $document = factory(\App\Document::class)->create();
        
        Notification::send($to_notify, new \App\Notifications\DocumentSubmitted($document));

        Notification::assertSentTo(
            $to_notify, \App\Notifications\DocumentSubmitted::class,
            function ($notification, $channels) use ($document) {
                return $notification->document->id === $document->id;
            }
        );
    }

    public function testFeedbackLateNotificationSends()
    {
        // Create document two weeks previous
        $doc = factory(\App\Document::class)->create([
            'student_username' => $this->student,
            'created_at' => date_create('-1 fortnight')
        ]);
        
        $late_user = factory(\App\LUCS_User::class)->states('Staff')->create();

        $this->student->ipaps()->attach($this->staff->username);
        $this->student->ipaps()->attach($late_user->username);

        \App\Feedback::create([
            'DocumentId' => $doc->id,
            'Location' => 'path',
            'staff_username' => $this->staff->username
        ]);

        \App\Helpers\FeedbackLateHelper::check($this->student);

        Notification::assertSentTo(
            $late_user->staff, \App\Notifications\FeedbackLate::class
        );

        Notification::assertNotSentTo(
            $this->staff, \App\Notifications\FeedbackLate::class
        );

    }

    /** @dataProvider documentTimestamps */
    public function testFeedbackLateNotificationDoesNotSendForInvalidDocuments($timestamp)
    {
        $doc = factory(\App\Document::class)->create([
            'student_username' => $this->student,
            'created_at' => date_create($timestamp)
        ]);
        
        $ipap = factory(\App\IPAP::class)->create([
            'staff_username' => $this->staff->username,
            'student_username' => $this->student->username
        ]);

        \App\Helpers\FeedbackLateHelper::check($this->student);

        Notification::assertNotSentTo(
            $ipap, \App\Notifications\FeedbackLate::class
        );
    }

    public function documentTimestamps()
    {
        return array(
            ['-1 week'],
            ['-15 days'],
            ['-1 month']
        );
    }

    public function testFeedbackCompleteNotificationSends()
    {
        $to_notify = collect([$this->student, $this->staff]);

        $document = factory(\App\Document::class)->create();
        $document->type()->associate($this->student->workflows->first()->type)->save();
        factory(\App\Feedback::class, 2)->create([
            'DocumentId' => $document->id
        ]);

        Notification::send($to_notify, new \App\Notifications\FeedbackComplete($this->student->workflows->first()));

        Notification::assertSentTo($to_notify, \App\Notifications\FeedbackComplete::class);
    }

    public function testExemptionRequestedNotificationSends()
    {
        $admin = factory(\App\LUCS_User::class)->states('Admin')->create();

        $admin->notify(new \App\Notifications\ExemptionRequested($this->student));

        Notification::assertSentTo(
            $admin, \App\Notifications\ExemptionRequested::class
        );
    }

    public function testExemptionGrantedNotificationSends()
    {
        $to_notify = collect([$this->student]);

        $supervisor = factory(\App\Supervisor::class)->create([
            'staff_username' => $this->staff->username,
            'student_username' => $this->student->username
        ]);

        $to_notify->push($supervisor->staff);

        Notification::send($to_notify, new \App\Notifications\ExemptionGranted($this->student));

        Notification::assertSentTo(
            $to_notify, \App\Notifications\ExemptionGranted::class
        );
    }

    public function testProgressAllowedNotificationSends()
    {
        $to_notify = collect([$this->student, $this->staff]);

        Notification::send($to_notify, new \App\Notifications\ProgressAllowed($this->student));

        Notification::assertSentTo($to_notify, \App\Notifications\ProgressAllowed::class);
    }


    public function testProgressCommitteeAttentionRequiredNotificationSends()
    {
        $to_notify = collect([$this->student, $this->staff]);

        Notification::send($to_notify, new \App\Notifications\ProgressBlocked($this->student));

        Notification::assertSentTo($to_notify, \App\Notifications\ProgressBlocked::class);
    }
}

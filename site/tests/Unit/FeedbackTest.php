<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FeedbackTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        factory(\App\Status::class)->create();

        Event::fake();
    }
    
    public function testGetQuestionnaire()
    {
        $year = 1;
        
        $workflow_type = factory(\App\WorkflowType::class)->create();
        
        factory(\App\Document::class)->create([
            'workflow_type_id' => $workflow_type->first()->id
        ]);
        
        $questionnaire = factory(\App\Questionnaire::class)->create([
            'workflow_type_id' => $workflow_type->first()->id,
        ]);

        $this->assertEquals($questionnaire->first(), $workflow_type->questionnaire);
    }

    public function testFeedbackCanAccessQuestionnaire()
    {
        $document = factory(\App\Document::class)->create();
        $feedback = factory(\App\Feedback::class)->make();
        $document->feedback()->save($feedback);
        $questionnaire = factory(\App\Questionnaire::class)->create([
            'workflow_type_id' => $document->type->id
        ]);

        $this->assertEquals($questionnaire->id, $feedback->document->type->questionnaire->id);
    }

    public function testFeedbackUpload()
    {
        Storage::fake('public');
        
        $user = factory(\App\LUCS_User::class)->create();
        
        $student = factory(\App\Student::class)->create();

        $doc = factory(\App\Document::class)->create(
            ['student_username' => $student->username]
        );

        $type = factory(\App\WorkflowType::class)->create([
            'value' => 'PhD Plan'
        ]);
        $doc->type()->associate($type)->save();

        $response = $this->actingAs($user)->json('POST', '/feedback/upload', [
            'docId' => $doc->id,
            'comment' => 'comment',
        ]);

        $response->assertStatus(302);

        Event::assertDispatched(\App\Events\FeedbackSubmitted::class);
    }

    public function testFeedbackAndCommentsForProgressCommitteRelationship()
    {
        $feedback = factory(\App\Feedback::class)->create();
        $data = new \App\DataForProgressCommittee;
        $data->progress_committee_recommendation_id = 1;
        $data->feedback()->associate($feedback);
        $data->comments = 'Concerns';
        $data->save();

        $this->assertEquals(1, $feedback->fresh()->dataForProgressCommittee->count());
    }
}

<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Event;

class CheckLateDocumentsTest extends TestCase
{
    use RefreshDatabase;


    
    /**
     * @dataProvider jobDispatchConfig
     * @return void
     */
    public function testDocumentLateNotificationSendsCorrectly($workflow_type_value, $deadline, $should_send)
    {
        Notification::fake();
        Event::fake();
        
        $staff = factory(\App\Staff::class, 2)->create();
                
        $students = factory(\App\Student::class, 5)->create();
        $type = factory(\App\WorkflowType::class)->create();

        $students->each(function ($student) use ($deadline, $type) {
            $student->workflows()->save(factory(\App\Workflow::class)->create([
                'deadline' => $deadline,
                'workflow_type_id' => $type->id
            ]));
        });

        foreach ($students as $student) {
            foreach ($staff as $supervisor) {
                $student->supervisors()->attach($supervisor->username);
            }
        }

        $to_notify = collect([$student, $staff]);

        $job = new \App\Jobs\CheckLateDocuments(\App\Workflow::all());
        $job->handle();

        if ($should_send) {
            Notification::assertSentTo($to_notify, \App\Notifications\DocumentLate::class);
        } else {
            Notification::assertNotSentTo($to_notify, \App\Notifications\DocumentLate::class);
        }
    }

    public function jobDispatchConfig()
    {
        $eoy = 'End of Year Report';
        $plan = 'PhD Plan';
             
        return array(
            [$eoy, date_create('yesterday'), true],
            [$plan, date_create('yesterday'), true],
            [$eoy, date_create('today'), false],
            [$plan, date_create('today'), false],
            [$eoy, date_create('-2 days'), false],
            [$plan, date_create('-2 days'), false],
        );
    }
}

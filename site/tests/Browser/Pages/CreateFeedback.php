<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Faker;

class CreateFeedback extends Page
{
    private $document;
    
    public function __construct($document_id)
    {
        $this->document = \App\Document::findOrFail($document_id);
    }
    
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/' . $this->document->id . '/feedback';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@element' => '#selector',
        ];
    }

    public function enterFeedback(Browser $browser, \App\Questionnaire $questionnaire)
    {
        $browser->select('@grade', 'Unsatisfactory');

        $faker = Faker\Factory::create();
        foreach ($questionnaire->questions as $idx=>$question) {
            $browser->assertSee($question->value);
            $browser->value('@comment'.$idx, $faker->sentences(3, true));
        }
    }

    public function submitFeedback(Browser $browser)
    {
        $browser->click('@submit');
    }

    public function enterAndSubmitFeedback(Browser $browser, \App\Questionnaire $questionnaire)
    {
        $this->enterFeedback($browser, $questionnaire);
        $this->submitFeedback($browser);
    }
}

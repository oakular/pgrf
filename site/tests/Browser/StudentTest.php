<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Illuminate\Http\UploadedFile;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class StudentTest extends DuskTestCase
{
    use DatabaseMigrations;

    private $user;

    protected function setUp(): void
    {
        parent::setUp();

        $student = factory(\App\Student::class)->create();
        $status = factory(\App\Status::class)->create();
        
        factory(\App\Status::class)->create([
            'name' => 'Report Submitted'
        ]);
        
        $student->workflows->first()->status()->associate(\App\Status::all()->first())->save();
        $this->user = $student->lucsUser;

        $type = factory(\App\WorkflowType::class)->create([
            'value' => 'End of Year Report'
        ]);
        $student->workflows->first()->type()->associate($type)->save();

        $supervisor = factory(\App\LUCS_User::class)->create();
        $ipap = factory(\App\LUCS_User::class)->create();
        
        $student->supervisors()->attach($supervisor->username);
        $student->ipaps()->attach($ipap->username);

        $student->workflows()->save(factory(\App\Workflow::class)->make([
            'workflow_type_id' => $type->id
        ]));
    }
    
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testStudentHomePageLoads()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user);

            $browser->visit(route('home'))
                ->assertPathIs('/home')
                ->assertSee('Welcome ' . $this->user->firstnames . ' ' . $this->user->surname)
                ->screenshot('student-home-page')
                ->assertAuthenticated();
        });
    }

    public function testStudentDetailsLoads()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user);

            $browser->visit(
                route('student.show', ['id' => $this->user->username])
            )
                ->assertPathIs('/student/' . $this->user->username)
                ->screenshot('student-detail-page')
                ->assertPresent('@home-button')
                ->screenshot('new-home-page');
        });

    }

    public function testDocumentUploadAndView()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user);
            
            $browser->visit(route('home'))
                ->screenshot('student_document-upload')
                ->assertVisible('@document-upload-form')
                ->attach('@document-input', __DIR__ . '/test-submission.pdf')
                ->select('@type-select')->screenshot('student_document-upload-form-complete')
                ->click('@submit');

            $browser->screenshot('student_document-upload-form-submitted')->assertSee('Your document was uploaded successfully')
                ->assertVisible('@document-listing-' . $this->user->student->documents->first()->id)
                ->assertVisible('@document-show-link')->clickLink($this->user->student->documents->first()->type->value);
        });
    }
}

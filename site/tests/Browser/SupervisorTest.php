<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class SupervisorTest extends DuskTestCase
{
    use DatabaseMigrations;
    
    private $supervisor_user;

    protected function setUp(): void
    {
        parent::setUp();
        
        $this->supervisor_user = factory(\App\LUCS_User::class)->states('Staff')->create();

        factory(\App\Status::class, 9)->create();

        $student = factory(\App\Student::class)->create();
        $student->detail->YearOfStudy = rand(3,4);
        $student->detail->save();
        $student->workflows->first()->status()->associate(\App\Status::all()->random(1)->first())->save();

        $type = factory(\App\WorkflowType::class)->create([
            'value' => 'End of Year Report'
        ]);
        $student->workflows->first()->type()->associate($type)->save();
        $student->supervisors()->attach($this->supervisor_user->username);
    }

    public function testSupervisorHome()
    {
        $students = factory(\App\Student::class, 2)->create();

        foreach ($students as $student) {
            $student->supervisors()->attach($this->supervisor_user->username);
        }
        
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->supervisor_user);

            $browser->visit(route('home'))->screenshot('supervisor_home');

            foreach ($this->supervisor_user->staff->supervisingStudents()->get() as $student) {
                $browser->assertSee("{$student->lucsUser->surname} {$student->lucsUser->firstnames}");
            }
        });

    }
    
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testStudentDetails()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->supervisor_user);

            $supervisor = \App\Supervisor::where('staff_username', 'LIKE', $this->supervisor_user->username)->first();

            $browser->visit(route('student.show', ['student' => $supervisor->student->username]))
                                                         ->screenshot('supervisor_student-details-year3or4')
                                                         ->assertSee("{$supervisor->student->lucsUser->firstnames} {$supervisor->student->lucsUser->surname}")
                                                             ->assertPresent('@request-exemption');
            $browser->click('@request-exemption')->waitFor('@confirm-exemption', 2)
                ->screenshot('supervisor_student-details-confirm-exemption');
            
            $supervisor->student->detail->YearOfStudy = 1;
            $supervisor->student->detail->save();
            
            $browser->visit(route('student.show', ['student' => $supervisor->student->username]))
                                                            ->screenshot('supervisor_student-details-year1')
                                                            ->assertMissing('@request-exemption');
        });
    }

    public function testExemptionRequest()
    {
        $status = \App\Status::all()->last();
        $status->name = 'Exemption Requested';
        $status->save();
        
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->supervisor_user);
            $student = \App\Supervisor::where('staff_username', 'LIKE', $this->supervisor_user->username)->first()->student;
            
            $browser->visitRoute('home');
            $browser->visit(route('exemption.request', ['workflow' => $student->workflows->first()->id]));
            $browser->pause(300)->screenshot('supervisor_exemption-requested');
            $browser->assertUrlIs(url('home'))
                ->assertSee('Exemption Requested')
                ->assertDontSee('Request Exemption');
        });
    }

    public function testWorkflowsAndIpapMeetingInteraction()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->supervisor_user);
            $student = \App\Supervisor::where('staff_username', 'LIKE', $this->supervisor_user->username)->first()->student;
            
            $browser->visit(route('student.show', ['student' => $student->username]))
                                                             ->screenshot('supervisor_ipap-meeting')
                                                             ->assertVisible('@create-ipap-meeting')
                                                             ->click('@create-ipap-meeting')
                                                             ->waitFor('@create-ipap-meeting-modal')
                                                             ->keys('@ipap-meeting-date', '01012020')
                                                             ->screenshot('supervisor_ipap-meeting-form-filled')
                                                             ->click('@submit');
            
            $browser->pause(300)->assertRouteIs('student.show', ['student' => $student]);
            $browser->screenshot('supervisor_ipap-meeting-created')
                ->assertVisible('@update-ipap-meeting')
                ->assertSee('1st Jan 2020')
                ->click('@update-ipap-meeting')
                ->waitFor('@is-completed')->pause(200)->check('@is-completed')
                ->screenshot('supervisor_complete-ipap-meeting')->click('@submit');

            $browser->pause(300)->assertRouteIs('student.show', ['student' => $student]);
            $browser->screenshot('supervisor_ipap-meeting-updated')
                ->assertVisible('@ipap-meeting-complete-badge');
        });
    }

    public function testSupervisorCannotViewFeedback()
    {
        $this->browse(function (Browser $supervisor_browser, Browser $student_browser) {
            $student = \App\Supervisor::where('staff_username', 'LIKE', $this->supervisor_user->username)->first()->student;
            $student->ipaps()->attach(
                factory(\App\LUCS_User::class)->states('Staff')->create()->username
            );

            $student->documents()->save(factory(\App\Document::class)->make());

            factory(\App\Feedback::class)->create([
                'staff_username' => $student->ipaps->first()->username,
                'DocumentId' => $student->documents->first()->id
            ]);

            $supervisor_browser->loginAs($this->supervisor_user);
            $supervisor_browser->visit(route('student.show', ['student' => $student->username]));
            $supervisor_browser->screenshot('supervisor_cannot-view-feedback');
            $supervisor_browser->assertDontSee('View Feedback');

            $student_browser->loginAs($student->lucsUser);
            $student_browser->visitRoute('student.show', ['student' => $student->username]);
            $student_browser->assertSee('View Feedback');
        });
    }
}

<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AdminTest extends DuskTestCase
{
    use DatabaseMigrations;

    private $admin;
    
    protected function setUp(): void
    {
        parent::setUp();

        $this->admin = factory(\App\LUCS_User::class)->states('Admin')->create();

        factory(\App\Department::class, 2)->create();

        $statuses = collect(array(
            // Report, PhD Plan
            1 => 'Not Due',
            2 => 'Due',
            3 => 'Reminder Sent',
            4 => 'Report Submitted',
            5 => 'Report Late',
            6 => 'Awaiting IPAP Feedback',
            7 => 'IPAP Feedback Incomplete',
            8 => 'IPAP Feedback Complete',
            9 => 'Needs Attention of Progress Committee',

            // Report
            12 => 'Exemption Requested',
            13 => 'Exemption Granted',


            // PhD Workshop
            14 => 'Slides Submitted',

            // Research Presentation
            15 => 'Form Submitted',
            16 => 'Form Late',
            17 => 'Form complete and presentation incomplete',
            18 => 'Form complete and presentation complete'

        ));

        $statuses->each(function ($status_name) {
            factory(\App\Status::class)->create([
                'name' => $status_name
            ]);
        });

        $workflow_types = collect(array(
            'PhD Plan',
            'End of Year Report',
            'CS Research Presentation',
            'CS Workshop'
        ));

        $workflow_types->each(function ($type_value) {
            factory(\App\WorkflowType::class)->create([
                'value' => $type_value
            ]);
        });

        $students = factory(\App\Student::class, 50)->create();
        $xjtlu_students = factory(\App\Student::class, 10)->create();

        $xjtlu_students->each(function ($student) {
            $student->detail->is_xjtlu = true;
            $student->detail->save();
        });
        
        $students->each(function ($student) {
            $student->workflows->first()->type()->associate(\App\WorkflowType::all()->random(1)->first())->save();
            $student->workflows->first()->status()->associate(\App\Status::all()->random(1)->first())->save();
        });
    }
    
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testAdminHome()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->admin);
            $browser->visit(route('home'))
                ->screenshot('admin_home')
                ->assertSee("Welcome {$this->admin->firstnames} {$this->admin->surname}");
        });
    }

    public function testWorkflowTypeOverview()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->admin);

            foreach (\App\WorkflowType::all() as $type) {
                $browser->visit(route('workflow_type.show', [
                    'typeId' => $type->id
                ]))
                    ->screenshot("admin_workflow_type_{$type->id}-overview");
                $browser->assertSee("{$type->value} - Overview")
                    ->assertPresent('@workflow_type-overview-table');
            }
        });
    }

    public function testCreateStudentWithMinimalInfo()
    {
        factory(\App\LUCS_User::class, 10)->create();
        factory(\App\LUCS_User::class, 10)->create();
        
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->admin);

            $browser->visit(route('student.create'));

            $browser->value('@firstname', 'New')->value('@surname', 'User')
                ->value('@username', 'testcaseusername')
                ->value('@email', 'testcase')
                ->select('@department')
                ->keys('@start-date', '01012019');

            $browser->screenshot('admin_create-student-form-filled')
                ->click('@submit')->screenshot('admin_create-student-form-submitted');

            $browser->assertUrlIs(route('student.show', ['username' => 'testcaseusername']));
        });
    }

    public function testCreateStudentXjtlu()
    {
        factory(\App\LUCS_User::class, 10)->create();
        factory(\App\LUCS_User::class, 10)->create();
        
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->admin);

            $browser->visit(route('student.create'));

            $browser->value('@firstname', 'New')->value('@surname', 'User')
                ->value('@username', 'testcaseusername')
                ->value('@email', 'testcase')
                ->keys('@start-date', '01012019');

            $browser->check('@is-xjtlu');

            $browser->screenshot('admin_create-student-xjtlu-form-filled')
                ->click('@submit')->screenshot('admin_create-student-xjtlu-form-submitted');

            $browser->assertUrlIs(route('student.show', ['username' => 'testcaseusername']));
        });
    }

    public function testCreateStudentPartTime()
    {
        factory(\App\LUCS_User::class, 10)->create();
        factory(\App\LUCS_User::class, 10)->create();
        
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->admin);

            $browser->visit(route('student.create'));

            $browser->value('@firstname', 'New')->value('@surname', 'User')
                ->value('@username', 'testcaseusername')
                ->value('@email', 'testcase')
                ->keys('@start-date', '01012019');

            $browser->check('@is-part-time');

            $browser->screenshot('admin_create-student-part-time-form-filled')
                ->click('@submit')->screenshot('admin_create-student-part-time-form-submitted');

            $browser->assertUrlIs(route('student.show', ['username' => 'testcaseusername']));
        });
    }

    public function testCreateStudentWithSupervisorsAndIpaps()
    {
        factory(\App\LUCS_User::class, 10)->create();
        factory(\App\LUCS_User::class, 10)->create();
        
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->admin);

            $browser->visit(route('student.create'));

            $browser->value('@firstname', 'New')->value('@surname', 'User')
                ->value('@username', 'testcaseusername')
                ->value('@email', 'testcase')
                ->keys('@start-date', '01012019');

            for ($idx = 0; $idx < 2; $idx++) {
                $browser->select("@supervisors[{$idx}]")->select("ipaps[{$idx}]");
            }

            $browser->screenshot('admin_create-student-with-supervisors-and-ipaps-form-filled')
                ->click('@submit')->screenshot('admin_create-student-with-supervisors-and-ipaps-form-submitted');

            $browser->assertUrlIs(route('student.show', ['username' => 'testcaseusername']));
        });
    }

    public function testEditStudent()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->admin);

            $student = \App\Student::all()->random(1)->first();
            $browser->visit(route('student.edit', ['id' => $student->username]));
            $browser->screenshot('admin_edit-student')->assertSee('Edit')
                ->check('@is-xjtlu')
                ->click('@submit');

            $browser->screenshot('admin_edit-student-form-submitted')
                ->assertRouteIs('student.show', $student->username);

            $browser->visit(route('student.edit', ['id' => $student->username]))
                                                        ->clickLink('Cancel')
                                                        ->assertRouteIs('student.show', $student->username);
        });
    }

    public function testAddAndDeleteSupervisorAndIpaps()
    {
        factory(\App\LUCS_User::class, 10)->states('Staff')->create();
        factory(\App\LUCS_User::class, 10)->states('Staff')->create();
        
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->admin);

            $student = \App\Student::all()->random(1)->first();
            $browser->visit(route('student.show', ['id' => $student->username]));

            // Add Supervisor

            $browser->click('@add-supervisor')->waitFor('@add-staff-supervisor-modal')
                ->screenshot('admin_add-supervisor')
                ->select('@new-supervisor');

            $username = $browser->value('@new-supervisor');
            $browser->click('@submit-new-supervisor');

            $new_supervisor = \App\LUCS_User::find($username);

            $browser->assertRouteIs('student.show', $student->username)
                ->assertSee("{$new_supervisor->firstnames} {$new_supervisor->surname}");

            // Delete Supervisor

            $browser->click('@delete-supervisor0')
                ->screenshot('admin_delete-supervisor')
                ->assertDontSee("{$new_supervisor->firstnames} {$new_supervisor->surname}");

            // Add IPAP

            $browser->click('@add-ipap')->waitFor('@add-staff-ipap-modal')
                ->screenshot('admin_add-ipap')
                ->select('@new-ipap');

            $username = $browser->value('@new-ipap');
            $browser->click('@submit-new-ipap');

            $new_ipap = \App\LUCS_User::find($username);

            $browser->assertRouteIs('student.show', $student->username)
                ->assertSee("{$new_ipap->firstnames} {$new_ipap->surname}");

            // Delete IPAP

            $browser->click('@delete-ipap0')
                ->screenshot('admin_delete-ipap')
                ->assertDontSee("{$new_ipap->firstnames} {$new_ipap->surname}");
        });
    }

    public function testCreateUpdateAndDeleteWorkflow()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->admin);
            $student = \App\Student::all()->random(1)->first();

            // Create Workflow

            $browser->visit(route('workflow.create', ['student' => $student->username]));
            $browser->screenshot('admin_create-workflow');
            $browser->select('@workflow-type');
            $browser->select('@status');
            $browser->keys('@deadline', '12122020');

            $browser->click('@submit');
            $browser->screenshot('admin_workflow-created')->assertRouteIs('student.show', ['student' => $student->username]);

            // Update Workflow
            $workflow = $student->workflows->last();

            $browser->click('@edit-workflow-' . $workflow->id);
            $browser->waitFor('@update-workflow-' . $workflow->id)->pause(200)->screenshot('admin_update-workflow');
            $browser->keys('@deadline-' . $workflow->id, '13122020');
            $status = $browser->select('@status-' . $workflow->id)->value('@status-' . $workflow->id);
            $browser->click('@submit-' . $workflow->id)->assertRouteIs('student.show', ['student' => $workflow->student]);
            $this->assertEquals($status, $workflow->fresh()->status->id);

            // Delete Workflow
            $browser->click('@delete-workflow-' . $workflow->id)
                                                ->screenshot('admin_delete-workflow')->assertMissing('@delete-workflow-' . $workflow->id);
        });
    }
}

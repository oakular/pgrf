<?php

namespace Tests\Browser;

use Laravel\Dusk\Http\Controllers\UserController;

class LUCSUserController extends UserController
{
    public function login($userId, $guard = null)
    {
        Auth::loginUsingId($userId);
    }
}

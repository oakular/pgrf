<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Faker;

class FeedbackTest extends DuskTestCase
{
    use DatabaseMigrations;

    private $ipap_user;
    private $document;
    private $questionnaire;

    protected function setUp(): void
    {
        parent::setUp();

        factory(\App\ProgressCommitteeRecommendation::class)->create();
        
        $student = factory(\App\Student::class)->create();
        $status = factory(\App\Status::class)->create();
        $student->workflows->first()->status()->associate(\App\Status::all()->first())->save();

        $type = factory(\App\WorkflowType::class)->create([
            'value' => 'Year 1 End of Year Report'
        ]);
        $student->workflows->first()->type()->associate($type)->save();

        $workflow_types = factory(\App\WorkflowType::class, 4)->create();
        $workflow_types->first()->value = 'PhD Plan';
        $workflow_types->first()->save();
        $this->document = factory(\App\Document::class)->create();
        $this->document->type()->associate($type);
        $student->documents()->save($this->document);

        $this->questionnaire = factory(\App\Questionnaire::class)->create();
        $this->questionnaire->workflowType()->associate($this->document->type)->save();
        
        $this->ipap_user = factory(\App\LUCS_User::class)->states('Staff')->create();
        $student->ipaps()->attach($this->ipap_user->username);
        $this->ipap_user->save();
    }
        
    
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testCreateFeedback()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->ipap_user);

            $browser->visit(
                new Pages\CreateFeedback($this->document->id)
            )
                ->pause(500)
                ->screenshot('feedback-enter')
                ->assertDontSee('404');

            $browser->enterFeedback($this->questionnaire);

            $grade = $browser->value('@grade');
            
            $browser->screenshot('feedback-complete');

            $browser->submitFeedback();

            if ($grade === 'Unsatisfactory') {
                $browser->screenshot('feedback-progress-committee-comments');
                $browser->value('@comment', 'Recommendations for the committee');
                $browser->select('@recommendation');
                $browser->click('@submit');
            }
                
            $browser->assertRouteIs('home')->screenshot('feedback-uploaded')
                ->assertSee('Your feedback was uploaded successfully');
        });
    }

    public function testShowFeedback()
    {
        $this->browse(function (Browser $staff_browser, Browser $student_browser) {
            $staff_browser->loginAs($this->ipap_user);
            $staff_browser->visit(new Pages\CreateFeedback($this->document->id));
            $staff_browser->enterAndSubmitFeedback($this->questionnaire);

            $student_browser->loginAs($this->document->student->lucsUser);
            $student_browser->visit(route('feedback.view', [
                'feedbackId' => $this->document->feedback->first()->id,
                'studentId' => $this->document->student->username
            ]))
                ->assertPathIs("/{$this->document->feedback->first()->id}/{$this->document->student->username}/feedback")
                ->assertDontSee('404');

            $student_browser->screenshot('student_view-feedback');

        });
    }

    public function testIpapHome()
    {
        $students = factory(\App\Student::class, 2)->create();

        foreach ($students as $student) {
            $student->ipaps()->attach($this->ipap_user->username);
        }
        
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->ipap_user);

            $browser->visit(route('home'))->screenshot('ipap_home');

            foreach ($this->ipap_user->staff->ipapStudents()->get() as $student) {
                $browser->assertSee("{$student->lucsUser->surname} {$student->lucsUser->firstnames} ");
            }
        });
    }
}



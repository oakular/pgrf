<?php

namespace Tests;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Testing\RefreshDatabaseState;

trait MultipleDatabaseMigrations
{
    use DatabaseMigrations;

    public function runDatabaseMigrations()
    {
        $this->artisan('migrate:all');
        $this->app[Kernel::class]->setArtisan(null);
        
        $this->beforeApplicationDestroyed(function () {
            $dbs = ['people', 'pgrf'];

            foreach ($dbs as $db) {
                $this->artisan('migrate:rollback', [
                    '--database' => $db,
                    '--path' => 'database/migrations/' . $db . '/'
                ]);
            }
            
            RefreshDatabaseState::$migrated = false;
        });
    }
}

?>

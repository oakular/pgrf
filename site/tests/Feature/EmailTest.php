<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
class EmailTest extends TestCase
{
    use RefreshDatabase;
    
    private $student;

    protected function setUp(): void
    {
        parent::setUp();

        $this->student = factory(\App\Student::class)->create();
        $this->student->workflows->first()->type()->associate(
            factory(\App\WorkflowType::class)->create()
        )->save();
        $document = factory(\App\Document::class)->create();
        $document->type()->associate($this->student->workflows->first()->type);
        $this->student->documents()->save($document);
        $this->student->save();
    }

    public function testDocumentSubmittedStudent()
    {

        $mailable = new \App\Mail\DocumentSubmittedStudent($this->student->documents()->first());
        $mail = $mailable->build();

        $this->assertEquals($mail->subject, 'PGR: Document Submission Receipt');
    }

    public function testDocumentSubmittedStaff()
    {
        $mailable = new \App\Mail\DocumentSubmittedStaff($this->student->documents()->first());
        $mail = $mailable->build();

        $expectedSubject = 'PGR: Document Submitted by '
                         . $this->student->lucsUser->firstnames
                         . ' ' . $this->student->lucsUser->surname;

        $this->assertEquals($mail->subject, $expectedSubject);
    }


    public function testFeedbackCompleteStudent()
    {
        $mailable = new \App\Mail\FeedbackCompleteStudent($this->student->workflows()->first());
        $mail = $mailable->build();

        $this->assertEquals($mail->subject, 'PGR: Feedback Ready');
    }

    public function testFeedbackCompleteStaff()
    {
        $mailable = new \App\Mail\FeedbackCompleteStaff($this->student);
        $mail = $mailable->build();

        $expectedSubject = 'PGR: Feedback Ready for '
                         . $this->student->lucsUser->firstnames
                         . ' ' . $this->student->lucsUser->surname;

        $this->assertEquals($mail->subject, $expectedSubject);
    }

    public function testFeedbackLate()
    {
        $student = factory(\App\Student::class)->create();
        $mailable = new \App\Mail\FeedbackLate($this->student->documents()->first());
        $mail = $mailable->build();

        $expectedSubject = 'PGR: Feedback Submission Required';

        $this->assertEquals($mail->subject, $expectedSubject);
    }

    public function testDocumentLateStudent()
    {
        $workflow_type = factory(\App\WorkflowType::class)->create();
        $mailable = new \App\Mail\DocumentLateStudent($workflow_type, $this->student);
        $mail = $mailable->build();

        $this->assertEquals($mail->subject, 'PGR: Document Late');
    }

    public function testDocumentLateStaff()
    {
        $workflow_type = factory(\App\WorkflowType::class)->create();
        $mailable = new \App\Mail\DocumentLateStaff($workflow_type, $this->student);
        $mail = $mailable->build();

        $expectedSubject = 'PGR: No Document Submitted by '
                         . $this->student->lucsUser->firstnames
                         . ' ' . $this->student->lucsUser->surname;

        $this->assertEquals($mail->subject, $expectedSubject);
    }
}

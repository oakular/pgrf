<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;

class StudentControllerTest extends TestCase
{
    use RefreshDatabase;

    private $admin;
    private $request_data;

    protected function setUp(): void
    {
        parent::setUp();
        
        $this->admin = factory(\App\LUCS_User::class)->states('Admin')->create();
        factory(\App\WorkflowType::class)->create([
            'value' => 'PhD Plan'
        ]);
        factory(\App\Status::class)->create();

        factory(\App\Department::class)->create(['value' => 'EEE']);
        factory(\App\Department::class)->create(['value' => 'CS']);

        $this->request_data = [
            'firstName' => 'Test',
            'lastName' => 'User',
            'startDate' => date_create('today')->format('Y-m-d'),
            'department' => \App\Department::all()->random(1)->first()->id
        ];
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateStudentWithMinimalInfo()
    {
        $request_data = array_merge($this->request_data, [
            'username' => 'testcaseMinimal',
            'email' => 'testcaseMinimal'
        ]);

        $response = $this->actingAs($this->admin)->post(route('student.store'), $request_data);

        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
    }

    public function testCreateXjtluStudent()
    {
        $request_data = array_merge($this->request_data, [
            'username' => 'testcaseXjtlu',
            'email' => 'testcaseXjtlu',
            'is-xjtlu' => 'on'
        ]);

        $response = $this->actingAs($this->admin)->post(route('student.store'), $request_data);
        
        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
    }

    public function testCreatePartTimeStudent()
    {
        $request_data = array_merge($this->request_data, [
            'username' => 'testcasePartTime',
            'email' => 'testcasePartTime',
            'is-part-time' => 'on'
        ]);

        $response = $this->actingAs($this->admin)->post(route('student.store'), $request_data);

        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
    }

    public function testCreateStudentWithSupervisorsAndIpaps()
    {
        $supervisors = factory(\App\LUCS_User::class, 10)->create();
        $ipaps = factory(\App\LUCS_User::class, 10)->create();

        $request_data = array_merge($this->request_data, [
            'username' => 'testcaseFull',
            'email' => 'testcaseFull',
            'ipaps' => [$ipaps->first()->username,
                        $ipaps->last()->username],
            'supervisors' => [$supervisors->last()->username,
                              $supervisors->first()->username]
        ]);

        $response = $this->actingAs($this->admin)->post(route('student.store'), $request_data);

        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
    }

    public function testBulkCreateStudents()
    {
        $file = new \Illuminate\Http\UploadedFile(__DIR__ . '/bulk_students.csv', 'bulk_students.csv', 'text/csv');

        $new_student_num = 9;
        $user_count = \App\LUCS_User::all()->count(); 
        $student_count = \App\Student::all()->count();
        $student_detail_count = \App\StudentDetail::all()->count();

        $response = $this->actingAs($this->admin)->post(
            route('student.bulk.store'),
            ['student-data' => $file]
        );

        $response->assertSessionHasNoErrors();
        $response->assertRedirect('home');

        $this->assertEquals($user_count+$new_student_num, \App\LUCS_User::all()->count());
        $this->assertEquals($student_count+$new_student_num, \App\Student::all()->count());
        $this->assertEquals($student_detail_count+$new_student_num, \App\StudentDetail::all()->count());
    }


    public function testUpdateStudent()
    {
        $student = factory(\App\Student::class)->create();

        $new_firstname = 'Updated';
        $new_surname = 'User';
        $new_email = 'updatedemail';
        $new_year_of_study = rand(1,4);
        $new_start_date = strtotime('-3 months');

        $response = $this->actingAs($this->admin)->put(route('student.update', ['student' => $student->username]), [
            'firstName' => $new_firstname, 
            'lastName' => $new_surname,
            'email' => $new_email,
            'yearOfStudy' => $new_year_of_study,
            'startDate' => $new_start_date
        ]);

        $response->assertStatus(302);

        $student = $student->fresh();

        $this->assertEquals($new_firstname, $student->lucsUser->firstnames);
        $this->assertEquals($new_surname, $student->lucsUser->surname);
        $this->assertEquals($new_email . '@liverpool.ac.uk', $student->lucsUser->email);
        $this->assertEquals($new_year_of_study, $student->detail->YearOfStudy);
        $this->assertEquals(date('Y-m-d', strtotime($new_start_date)), $student->detail->StartDate);
    }
}

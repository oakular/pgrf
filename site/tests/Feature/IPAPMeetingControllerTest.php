<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
class IPAPMeetingControllerTest extends TestCase
{
    use RefreshDatabase;

    private $user;
    private $student;
    private $date;



    protected function setUp(): void
    {
        parent::setUp();

        $this->user = factory(\App\LUCS_User::class)->states('Staff')->create();
        $this->student = factory(\App\Student::class)->create();

        $type = factory(\App\WorkflowType::class)->create([
            'value' => 'End of Year Report'
        ]);

        $this->student->workflows()->save(
            factory(\App\Workflow::class)->make([
                'workflow_type_id' => $type->id
            ])
        );
        
        $this->date = date('Y-m-d H:i:s', strtotime('+6 months 17:00:00'));
    }
    
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSaveMeetingAsSupervisor()
    {
        $this->createSupervisor();
        
        $response = $this->actingAs($this->user)->json('POST', '/meeting', [
            'meeting-date' => $this->date,
            'workflow' => $this->student->workflows->last()->id
        ]);

        $response->assertStatus(302);

        $this->assertEquals($this->date, $this->student->workflows->last()->ipapMeeting->date);
    }

    public function testSaveMeetingNotAsSupervisor()
    {
        $response = $this->actingAs($this->user)->json('POST', '/meeting', [
            'meeting-date' => $this->date,
            'workflow' => $this->student->workflows->last()->id
        ]);

        $response->assertStatus(403);
    }

    public function testUpdateMeetingAsSupervisor()
    {
        $this->createSupervisor();
        
        $this->date = date('Y-m-d H:i:s', strtotime('+6 months 18:00:00'));

        $meeting = factory(\App\IPAPMeeting::class)->create();
        $this->student->workflows->last()->ipapMeeting()->save($meeting);

        $url = route('meeting.update', ['meeting' => $this->student->workflows->last()->ipapMeeting]);

        $response = $this->actingAs($this->user)->json('PUT', $url, [
            'meeting-date' => $this->date,
        ]);

        $response->assertStatus(302);
        $meeting = \App\IPAPMeeting::find($meeting->id);
        $this->assertEquals($this->date, $meeting->date);
    }

    public function testUpdateMeetingNotAsSupervisor()
    {
        $this->date = date('Y-m-d H:i:s', strtotime('+6 months 18:00:00'));

        $this->student->workflows->last()->ipapMeeting()->save(factory(\App\IPAPMeeting::class)->make());
        $url = route('meeting.update', ['meeting' => $this->student->workflows->last()->ipapMeeting]);

        $response = $this->actingAs($this->user)->json('PUT', $url, [
            'meeting-date' => $this->date,
        ]);

        $response->assertStatus(403);
    }

    private function createSupervisor()
    {
        $this->student->supervisors()->attach($this->user->username);
    }
}

<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Notification;
use Illuminate\Foundation\Testing\RefreshDatabase;
class CheckFeedbackCompleteTest extends TestCase
{
    use RefreshDatabase;
    
    protected function setUp(): void
    {
        parent::setUp();

        Notification::fake();

        factory(\App\Status::class, 8)->create();
        factory(\App\Status::class)->create(['name' => 'IPAP Feedback Complete']);

        $this->student = factory(\App\Student::class)->create();
        $this->student->workflows->first()->status()->associate(
            \App\Status::all()->first()
        )->save();

        $type = factory(\App\WorkflowType::class)->create();
        $this->student->workflows->first()->type()->associate(
            $type
        )->save();

        $ipaps = factory(\App\LUCS_User::class, 2)->create();
        $supervisors = factory(\App\LUCS_User::class, 2)->create();

        $ipaps->each(function ($ipap) {
            $this->student->ipaps()->attach($ipap->username);
            
        });

        $supervisors->each(function ($ipap) {
            $this->student->supervisors()->attach($ipap->username);
        });
    }

    public function testStatusIsFeedbackCompleteWhenAllFeedbackSubmitted()
    {
        $document = factory(\App\Document::class)->create();
        $document->type()->associate($this->student->workflows->first()->type)->save();
        $this->student->documents()->save($document);

        foreach ($this->student->ipaps as $ipap) {
            factory(\App\Feedback::class)->create([
                'DocumentId' => $this->student->documents->first()->id
            ]);
        }

        $job = new \App\Jobs\CheckFeedbackComplete([$this->student->documents->first()]);

        $job->handle();

        $this->student = \App\Student::find($this->student->username);

        // Notification::assertSentTo($this->student, \App\Notifications\FeedbackComplete::class);

        $this->assertEquals(
            \App\Status::where('name', 'LIKE', 'IPAP Feedback Complete')->first()->id,
            $this->student->workflows->first()->status->id
        );
    }

    public function testFeedbackCompleteEventChangesStatus()
    {
        $document = factory(\App\Document::class)->create();
        $document->type()->associate($this->student->workflows->first()->type)->save();
        $this->student->documents()->save($document);


        event(new \App\Events\FeedbackComplete($this->student, $document));

        $this->assertEquals(
            \App\Status::where('name', 'LIKE', 'IPAP Feedback Complete')->first()->id,
            $this->student->workflows->first()->fresh()->status->id,
            "Failed asserting that {$this->student->workflows->first()->status->name} equals 'IPAP Feedback Complete'"
        );
    }
}

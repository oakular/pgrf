<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;

class ExemptionControllerTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        
        Notification::fake();
    }
    
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExemptionRequest()
    {
        $user = factory(\App\LUCS_User::class)->states('Staff')->create();
        factory(\App\LUCS_User::class)->states('Admin')->create();

        factory(\App\Status::class, 9)->create();
        $status  = \App\Status::all()->last();
        $status->name = 'Exemption Requested';
        $status->save();

        $student = factory(\App\Student::class)->create();

        $student->supervisors()->attach($user->username);

        $student->workflows->first()->status()->associate(\App\Status::all()->first())->save();

        $response = $this->actingAs($user)->json('GET', '/exemption/' . $student->workflows->first()->id);

        // Refresh model
        $student = \App\Student::find($student->username);

        $response->assertStatus(302);
        $this->assertEquals('Exemption Requested', $student->workflows->first()->status->name);
    }

    public function testExemptionGrant()
    {
        $admin = factory(\App\LUCS_User::class)->states('Admin')->create();
        $supervisor = factory(\App\LUCS_User::class)->create();
        
        factory(\App\Status::class, 9)->create();
        $status  = \App\Status::all()->last();
        $status->name = 'Exemption Granted';
        $status->save();

        $status = \App\Status::all()->first();
        $status->name = 'Exemption Requested';
        $status->save();

        $student = factory(\App\Student::class)->create();

        $student->supervisors()->attach($supervisor->username);

        $student->workflows->first()->status()->associate($status)->save();

        $response = $this->actingAs($admin)->json('GET', '/exemption/grant/' . $student->username);

        $response->assertSuccessful(200);

        $student = \App\Student::find($student->username);
        $this->assertEquals('Exemption Granted', $student->workflows->first()->status->name);
    }
}

<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
class ProgressCommitteeTest extends TestCase
{
    use RefreshDatabase;
    
    public function BuildProgressCommittee()
    {
        $admin = factory(\App\LUCS_User::class)->states('Admin')->create();
        $progress_committee = factory(\App\ProgressCommitteeMember::class, 3)->create();
        $workflow = factory(\App\Workflow::class)->create();

        $response = $this->actingAs($admin)->post(
            route('committee.store'),
            [
                'committee' => $progress_committee,
                'workflow' => $workflow->id
            ]
        );

        $response->assertRedirect('home');

        foreach ($progress_committee as $member) {
            $this->assertEquals($workflow->id, $member->workflows()->first()->id);
        }
    }
}

<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
class SupervisorControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testDestroySupervisorRelationship()
    {
        $admin = factory(\App\LUCS_User::class)->states('Admin')->create();
        $student = factory(\App\Student::class)->create();
        $supervisor = factory(\App\LUCS_User::class)->states('Staff')->create();
        $student->supervisors()->attach($supervisor->username);

        $response = $this->actingAs($admin)->delete(route('supervisor.destroy', [
            'staff' => $student->supervisors->first()->username,
            'student' => $student->username
        ]));

        $response->assertStatus(302);

        $this->assertEmpty($student->supervisors()->get());
    }

    public function testStoreNewSupervisorRelationship()
    {
        $admin = factory(\App\LUCS_User::class)->states('Admin')->create();
        $student = factory(\App\Student::class)->create();
        $supervisor = factory(\App\LUCS_User::class)->states('Staff')->create();

        $response = $this->actingAs($admin)->post(route('supervisor.store'), [
            'student' => $student->username,
            'supervisor' => $supervisor->username
        ]);

        $response->assertStatus(302);

        $this->assertEquals($supervisor->username, $student->supervisors->first()->username);
    }
}

<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
class WorkflowControllerTest extends TestCase
{
    use RefreshDatabase;
    
    protected function setUp(): void
    {
        parent::setUp();
        
        factory(\App\Status::class, 9)->create();
        $this->type = factory(\App\WorkflowType::class)->create();
        $this->student = factory(\App\Student::class)->create();
        $this->admin = factory(\App\LUCS_User::class)->states('Admin')->create();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateNewWorkflow()
    {
        $response = $this->actingAs($this->admin)->post(route('workflow.store'), [
            'workflow-type' => $this->type->id,
            'student-username' => $this->student->username,
            'deadline' => date('Y-m-d', strtotime('+6 months')),
            'status' => \App\Status::all()->first()->id
        ]);

        $response->assertRedirect(route('student.show', ['student' => $this->student->username]));

        $this->student->fresh();
        $this->assertEquals(2, $this->student->workflows->count());
    }

    public function testCannotCreateWorkflowWhenUserIsNotAdmin()
    {
        $response = $this->actingAs($this->student->lucsUser)->post(route('workflow.store'), [
            'workflow-type' => $this->type->id,
            'student-username' => $this->student->username,
            'deadline' => date('Y-m-d', strtotime('+6 months')),
            'status' => \App\Status::all()->first()->id
        ]);

        $response->assertForbidden();
    }

    public function testAdminCanDeleteWorkflow()
    {
        $workflow = factory(\App\Workflow::class)->create([
            'workflow_type_id' => $this->type->id
        ]);
        $this->student->workflows()->save($workflow);

        $response = $this->actingAs($this->admin)->delete(
            route('workflow.destroy', [
                'workflow' => $workflow->id
            ])
        );

        $response->assertStatus(302);
        $this->assertTrue($workflow->fresh()->trashed());
    }

    public function testNonAdminCannotDeleteWorkflow()
    {
        $workflow = factory(\App\Workflow::class)->create([
            'workflow_type_id' => $this->type->id
        ]);
        $this->student->workflows()->save($workflow);

        $response = $this->actingAs($this->student->lucsUser)->delete(
            route('workflow.destroy', [
                'workflow' => $workflow->id
            ])
        );

        $response->assertForbidden();
    }

    public function testAdminCanEditWorkflow()
    {
        $workflow = factory(\App\Workflow::class)->create([
            'workflow_type_id' => $this->type->id
        ]);
        $this->student->workflows()->save($workflow);

        $date = date('Y-m-d', strtotime('+7 months'));
        $status = \App\Status::all()->random(1)->first();

        $response = $this->actingAs($this->admin)->put(
            route('workflow.update', ['workflow' => $workflow->id]), [
                'deadline' => $date,
                'status' => $status->id
            ]
        );

        $response->assertRedirect(route('student.show', ['student' => $this->student->username]));

        $this->assertEquals($date, $workflow->fresh()->deadline->format('Y-m-d'));
        $this->assertEquals($status->name, $workflow->fresh()->status->name);
    }

    public function testNonAdminCannotEditWorkflow()
    {
        $workflow = factory(\App\Workflow::class)->create([
            'workflow_type_id' => $this->type->id
        ]);
        $this->student->workflows()->save($workflow);

        $date = date('Y-m-d', strtotime('+7 months'));
        $status = \App\Status::all()->random(1)->first();

        $response = $this->actingAs($this->student->lucsUser)->put(
            route('workflow.update', ['workflow' => $workflow->id]), [
                'deadline' => $date,
                'status' => $status->id
            ]
        );

        $response->assertForbidden();
    }
}

<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\RefreshDatabase;
class PhDPlanApprovalControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithoutMiddleware;

    protected function setUp(): void
    {
        parent::setUp();

        $this->supervisor = factory(\App\LUCS_User::class)->states('Staff')->create();

        $this->student = factory(\App\Student::class)->create();

        $this->student->supervisors()->attach($this->supervisor->username);

        $type = factory(\App\WorkflowType::class)->create([
            'value' => 'PhD Plan'
        ]);

        $this->student->workflows()->first()->save();
        $this->student->workflows()->first()->type()->associate($type)->save();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSubmitPhDPlanApprovalAsSupervisor()
    {
        $response = $this->actingAs($this->supervisor)->post('/plan/approval', [
            'plan-description' => 'Phd Plan test description',
            'resource-description' => 'phd plan resource description',
            'risk-assessment-is-complete' => true,
            'risk-assessment-date' => date('Y-m-d', strtotime('-4 days')),
            'is-confirmed' => true,
            'workflow-id' => $this->student->workflows()->first()->id
        ]);

        $response->assertRedirect('home');

        $response->assertSessionHas(
            'flashMsg',
            "The {$this->student->workflows()->first()->type->value} has been approved"
        );
    }

    public function testNonSupervisorCannotApprovePhDPlan()
    {
        $user = factory(\App\LUCS_User::class)->create();

        $response = $this->actingAs($user)->post('/plan/approval', [
            'plan-description' => 'Phd Plan test description',
            'resource-description' => 'phd plan resource description',
            'risk-assessment-is-complete' => true,
            'risk-assessment-date' => date('Y-m-d', strtotime('-4 days')),
            'is-confirmed' => true,
            'workflow-id' => $this->student->workflows()->first()->id
        ]);
        

        $response->assertForbidden();
    }
}

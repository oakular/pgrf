<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
class IPAPControllerTest extends TestCase
{
    use RefreshDatabase;
    
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testDeleteIpap()
    {
        $admin = factory(\App\LUCS_User::class)->states('Admin')->create();
        $student = factory(\App\Student::class)->create();
        $ipap = factory(\App\LUCS_User::class)->states('Staff')->create();
        $student->ipaps()->attach($ipap->username);

        $response = $this->actingAs($admin)->delete(route('ipap.destroy', [
            'staff' => $student->ipaps->first()->username,
            'student' => $student->username
        ]));

        $response->assertStatus(302);

        $this->assertEmpty($student->ipaps()->get());
    }

 public function testStoreNewIpapRelationship()
    {
        $admin = factory(\App\LUCS_User::class)->states('Admin')->create();
        $student = factory(\App\Student::class)->create();
        $ipap = factory(\App\LUCS_User::class)->states('Staff')->create();

        $response = $this->actingAs($admin)->post(route('ipap.store'), [
            'student' => $student->username,
            'ipap' => $ipap->username
        ]);

        $response->assertStatus(302);

        $this->assertEquals($ipap->username, $student->ipaps->first()->username);
    }

}

<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
class MasqueradeControllerTest extends TestCase
{
    use RefreshDatabase;

    private $admin;
    private $to_masquerade;

    protected function setUp(): void
    {
        parent::setUp();
        
        $this->admin = factory(\App\LUCS_User::class)->states('Admin')->create();
        $this->to_masquerade = factory(\App\LUCS_User::class)->create();
    }

    public function testMasqueradeUserChangesUserAndSavesOldUserInCookie()
    {
        $response = $this->actingAs($this->admin)->get(
            route('masquerade', ['username' => $this->to_masquerade->username])
        );

        $response->assertRedirect('home');
        $response->assertCookie('masquerading_user', $this->admin->username);
    }

    public function testMasqueradeFailsIfUserIsNotAdmin()
    {
        $response = $this->actingAs($this->to_masquerade)->get(
            route('masquerade', ['username' => $this->admin->username])
        );

        $response->assertForbidden();
    }
}

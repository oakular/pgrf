@extends('layouts.app')

@section('title')
    @if($result)
        Upload Successful
    @else
        Upload Unsuccessful
    @endif
@endsection

@section('content')
    <div class="row-sm mt-3">
        <div class="jumbotron">
            <h1 class="display-3">
                @if ($result)
                    Your file has been uploaded successfully
                @else
                    Your file was not successfully uploaded
                @endif
            </h1>
            <hr>
            <p>
                <a class="btn btn-info btn-lg" href="{{ URL::route('home') }}">Return to home</a>
                <!-- <a class="btn btn-info btn-lg" href="{{-- Storage::download($file_url) --}}">View the file</a> -->
            </p>
        </div>
    </div>
@endsection

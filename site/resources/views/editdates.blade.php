@extends('layouts.app')

@section('title')
    {{ $studentUser->firstnames }} {{ $studentUser->surname }} - Change Key Dates
@endsection

@section('content')
    <div class="row-sm mt-3">
        @component('components.welcome-msg')
            @slot('msg')
                {{ $studentUser->firstnames }} {{ $studentUser->surname }} - Change Key Dates Info
            @endslot
        @endcomponent
    </div>

    <div class="row-sm">
        <div class="col-sm">
            <div class="row mt-3">
                <div class="alert alert-info alert-dismissible fade show" role="alert" style="width: 100%">
                    To retain current dates: leave fields empty
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                @component('components.alert')
                @slot('print_errors')
                {{ $errors->first() }}
                @endslot
                @endcomponent
            </div>

            <form action="{{ URL::route('duedates.update', ['username' => $studentUser->username]) }}" method="post">
                @csrf
                @foreach ($columns as $keydate)
                    @if ($loop->iteration < 3)
                        @continue
                    @endif

                    <div class="form-group">
                        @if($loop->last)
                            <label for="ipapMeetingCompleted">{{ $keydate }}</label>
                            <select class="custom-select" id="ipapMeetingCompleted" name="ipapMeetingCompleted">
                                <option value="">Do not change</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                            @break
                        @endif

                        <label for="keyDate[{{ $loop->iteration }}]">{{ $keydate }}</label>
                        <input id="keyDate[{{ $loop->iteration }}]" name="keyDate[{{ $loop->iteration }}]" class="form-control" type="date"/>
                    </div>
                @endforeach

                <button type="submit" class="btn btn-success mt-3 mb-3 float-right">Update</button>
            </form>
        </div>
    </div>
@endsection

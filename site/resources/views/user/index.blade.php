@extends('layouts.app')

@section('title')
    All Users
@endsection

@section('content')
    @component('components.user-list', ['users' => \App\LUCS_User::all()])
    @endcomponent
@endsection

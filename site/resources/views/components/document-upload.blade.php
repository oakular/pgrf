<div class="card">
    <div class="card-body">
        <h5 class="card-title">Document Upload</h5>
        <form class="form" action="{{ URL::route('document.store') }}" method="post" enctype="multipart/form-data" dusk="document-upload-form">
            @csrf
            <div class="form-group mt-3">
                <label for="documentType" class="col-form-label">Document Type</label>
                <select dusk="type-select" name="documentType" id="documentType" class="form-control custom-select">
                    @foreach ($workflows as $workflow)
                        <option value="{{ $workflow->type->id }}">{{ $workflow->type->value }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group custom-file mt-3">
                <label class="custom-file-label col-sm" for="docUpload" id="docUploadLabel">Choose a file</label>
                <input type="file" class="col-sm" dusk="document-input" name="document" id="docUpload"/>
            </div>
            <div class="form-group mt-3">
                <input class="btn btn-success float-right" dusk="submit" type="submit" value="Upload"/>
            </div>
        </form>
    </div>
</div>

<script>
 $("#docUpload").change(function () {
     $("#docUploadLabel").text($("#docUpload").val().split('\\').pop());
 });
</script>

@if ($errors->any())
    <div class="alert alert-danger" role="alert" style="width: 100%">
        {{ $print_errors }}
    </div>
@endif

<div class="mt-3 mb-3">
    <table class="display table table-responsive-sm table-hover" id="studentList{{ $id }}">
        <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Department</th>
                <th scope="col">Year of Study</th>
                <th scope="col">Latest Status</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($students as $student)
                <tr name="{{ $student->username }}">
                    <td>
                        <a href="{{ URL::route('student.show', ['id' => $student->username]) }}">{{ $student->lucsUser->getFullNameOrUsernameReversed() }}</a>
                        @if ($student->detail->is_xjtlu ?? false)
                            <span style="float: right;">(XJTLU)</span>
                        @endif
                    </td>
                    <td>{{ $student->detail->department->value ?? 'Unknown' }}</td>
                    <td align="center">{{ $student->detail->YearOfStudy ?? 'Unknown' }}</td>
                    <td align="center">{{ $student->workflows->sortByDesc('modified_at')->first()->status->name ?? 'None' }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<script>
 $(document).ready(function() {
     $('#studentList{{ $id }}').DataTable( {
         "order": [[ 2, "asc" ], [0, "asc"]],
     } );
 } );
</script>

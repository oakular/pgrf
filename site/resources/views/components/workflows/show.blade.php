<div class="card">
    <div class="card-body">
        <h4 class="card-title">
            <span style="text-align:left;">{{ $workflow->type->value ?? 'Unknown Workflow Type' }} - Info</span>

            <span style="float: right;">
                @if (auth()->user()->isAdmin())
                    <button dusk="edit-workflow-{{ $workflow->id }}" style="cursor: pointer; background: none; padding: 0px; border: none;" type="submit" data-toggle="modal" data-target="#update-workflow-{{ $workflow->id }}">
                        <i class="fas fa-pen-square text-primary"></i>
                    </button>
                    <form action="{{ URL::route('workflow.destroy', ['workflow' => $workflow->id]) }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button dusk="delete-workflow-{{ $workflow->id }}" style="cursor: pointer; background: none; padding: 0px; border: none;" type="submit">
                            <i class="fas fa-minus-square text-danger"></i>
                        </button>
                    </form>
                @endif
            </span>
        </h4>
    </div>
    <ul class="list-group list-group-flush">
        <li class="list-group-item">
            <span class="text-align: left;">Status: {{ $workflow->status->name ?? 'None'}}</span>
        </li>
        <li class="list-group-item">Deadline: {{ date_format(date_create($workflow->deadline), 'jS M Y') ?? 'None' }}</li>
        @if (\App\WorkflowType::report()->get()->contains('id', $workflow->type->id))
            <li class="list-group-item">IPAP Meeting:
                @if (is_null($workflow->ipapMeeting))
                    <span style="text-align:left;">Not Set</span>

                    @if($workflow->student->supervisors->contains('username', auth()->user()->username))
                        <span style="float: right;">
                            <button type="button" class="btn btn-danger btn-sm" dusk="create-ipap-meeting"
                                    data-toggle="modal" data-target="#create-ipap-meeting">Set Date</button>
                        </span>
                    @endif
                @elseif ($workflow->ipapMeeting->is_completed)
                    <span dusk="ipap-meeting-complete-badge" class="badge badge-success">Completed</span>
                @else
                    {{ date_format(date_create($workflow->ipapMeeting->date), 'jS M Y') }}
                    @if($workflow->student->supervisors->contains('username', auth()->user()->username))
                        <span style="float: right;">
                            <button type="button" class="btn btn-danger btn-sm" dusk="update-ipap-meeting"
                                    data-toggle="modal" data-target="#update-ipap-meeting">Update</button>
                        </span>
                    @endif
                @endif
            </li>
        @endif
    </ul>
    @if($workflow->student->supervisors->contains('username', auth()->user()->username))
        @if ($workflow->student->detail->YearOfStudy == 3 OR $workflow->student->detail->YearOfStudy == 4)
            @if ($workflow->type->value == "End of Year Report")
                @unless ($workflow->status->name == "Exemption Requested")
                    <div class="card-footer text-center">
                        <button type="button" class="btn btn-sm btn-danger" dusk="request-exemption"
                                data-toggle="modal" data-target="#confirm-exemption">Request Exemption</button>
                    </div>
                @endunless
            @endif
        @endif
    @endif

</div>

@if($workflow->student->supervisors->contains('username', auth()->user()->username))
    @if(\App\WorkflowType::report()->get()->contains($workflow->type->id))
        @if(is_null($workflow->ipapMeeting))
            @component('components.ipap_meeting.create', ['workflow' => $workflow])
            @endcomponent
        @else
            @component('components.ipap_meeting.update', ['workflow' => $workflow])
            @endcomponent
        @endif
    @endif
@endif

@if(auth()->user()->isAdmin())
    @component('components.workflows.edit', ['workflow' => $workflow])
    @endcomponent
@endif

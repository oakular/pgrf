<div class="modal fade" tabindex="-1" role="dialog" id="update-workflow-{{ $workflow->id }}" dusk="update-workflow-{{ $workflow->id }}">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update {{ $workflow->type->value }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form" action="{{ URL::route('workflow.update', ['workflow' => $workflow->id]) }}" method="post" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="form-group mt-3">
                        <label for="meeting-date">Deadline</label>
                        <input id="deadline" name="deadline" class="form-control" type="date" dusk="deadline-{{ $workflow->id }}"/>
                    </div>
                    <div class="form-group mt-3">
                       <label for="status">Status</label> 
                       <select class="custom-select" name="status" id="status" dusk="status-{{ $workflow->id }}">
                           @foreach(\App\Status::all() as $status)
                               <option value="{{ $status->id }}">{{ $status->name }}</option>
                           @endforeach
                       </select>
                    </div>
                    <div class="form-group modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" dusk="submit-{{ $workflow->id }}" class="btn btn-success">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
 $('#update-workflow-{{ $workflow->id }}').on('shown.bs.modal', function (e) {
     $('input[name=status]').val('{{ $workflow->status->name }}').change(); 
     $('input[name=deadline]').val('{{ $workflow->deadline->format('d-m-Y') }}').change(); 
});
</script>

<div class="modal fade" tabindex="-1" role="dialog" id="create-ipap-meeting" dusk="create-ipap-meeting-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Set IPAP Meeting Date</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form" action="{{ URL::route('meeting.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group mt-3">
                        <label for="meeting-date">IPAP Meeting Date</label>
                        <input id="meeting-date" name="meeting-date" dusk="ipap-meeting-date" class="form-control" type="date" required/>
                        <input id="workflow" name="workflow" hidden value="{{ $workflow->id }}"/>
                    </div>
                    <div class="form-group modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" dusk="submit" class="btn btn-primary">Set Date</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="update-ipap-meeting" dusk="update-ipap-meeting-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Set IPAP Meeting Date</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form" action="{{ URL::route('meeting.update', ['ipap_meeting' => $workflow->ipapMeeting->id]) }}" method="post" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="form-group mt-3">
                        <label for="meeting-date">IPAP Meeting Date</label>
                        <input id="meeting-date" name="meeting-date" class="form-control" type="date"/>
   
                        <input id="student-username" name="student-username" hidden value="{{ $workflow->student->username }}"/>
                    </div>
                    <div class="form-check mt-3">
                        <input type="checkbox" id="is-completed" name="is-completed" dusk="is-completed" class="form-check-input"/>
                        <label for="is-completed" dusk="ipap-meeting-complete-badge" class="form-check-label">Meeting Completed</label>
                    </div>
                    <div class="modal-footer mt-3">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" dusk="submit" class="btn btn-primary">Update Date</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

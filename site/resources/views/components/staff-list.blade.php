<h4>{{ $subTitle }}</h4>

@forelse ($staffMembers as $staff)
    <div class="card mt-3">
        <div class="card-body">
            <h5 class="card-title">
                <span style="text-align: left">{{ $staff->lucsUser->firstnames }} {{ $staff->lucsUser->surname }}</span>
                @if (auth()->user()->isAdmin())
                    <span style="float: right;">
                        <form action="{{ URL::route($route, ['staff' => $staff->username, 'student' => $student->username]) }}" method="POST">
                            @method('DELETE')
                            @csrf
                            <button dusk="delete-{{ $relationship }}{{ $loop->index }}" style="cursor: pointer; background: none; padding: 0px; border: none;" type="submit">
                                <i class="fas fa-minus-square text-danger"></i>
                            </button>
                        </form>
                    </span>
                @endif
            </h5>
        </div>

        <ul class="list-group list-group-flush">
            <li class="list-group-item">
                Email:
                <a href="mailto:{{ $staff->lucsUser->email }}">
                    {{ $staff->lucsUser->email }}
                </a>
            </li>
            <li class="list-group-item">
                Office: {{ $staff->office }}
            </li>
        </ul>
    </div>
@empty
    <div class="card mt-3">
        <div class="card-body">
            <p>No {{ $relationship }}s have been added</p>
        </div>
    </div>
@endforelse

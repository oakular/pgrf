@section('enter-feedback')
    @forelse ($doc->feedback()->get() as $feedback)
        @if ($feedback->staff_username == auth()->user()->username)
            <li class="list-group-item">Feedback Submitted</li>
            @break
        @endif

        @if ($loop->last)
            <li class="list-group-item">
                <a href="{{ URL::route('feedback.enter',['docId' => $doc->id]) }}">Enter Feedback</a>
            </li>
        @endif
    @empty
        <li class="list-group-item">
            <a href="{{ URL::route('feedback.enter',['docId' => $doc->id]) }}">Enter Feedback</a>
        </li>
    @endforelse
@endsection

@section('view-feedback')
    @if ($doc->feedback->count() == $doc->student->ipaps->count())
        @unless($doc->student->supervisors->contains('username', auth()->user()->username))
            @forelse ($doc->feedback()->get() as $feedback)
                <li class="list-group-item">
                    <a href="{{ URL::route('feedback.view', ['feedbackId' => $feedback->id, 'studentId' => $doc->student->username]) }}">View Feedback by {{ \App\LUCS_User::find($feedback->staff_username)->firstnames }} {{ \App\LUCS_User::find($feedback->staff_username)->surname }}</a>
                </li>
            @empty
                <li class="list-group-item">No Feedback Submitted</li>
            @endforelse
        @endunless
    @endif
@endsection

<div class="card mt-3" dusk="document-listing-{{ $doc->id }}">
    <div class="card-body">
        <a dusk="document-show-link" href="{{ URL::route('document.show', ['id' => $doc->id]) }}">
            <h5 class="card-title">
                {{ $doc->type->value }}
            </h5>
        </a>
    </div>
    
    <ul class="list-group list-group-flush">

        @unless($doc->type->value == 'CS Research Presentation')
            @if ($doc->student->ipaps->contains('username', auth()->user()->username))
                @yield('enter-feedback')
            @else
                @yield('view-feedback')
            @endif
        @endunless
    </ul>
</div>

<div class="mt-3 mb-3">
    <table class="display table table-responsive-sm table-hover" id="userList">
        <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Roles</th>
                <th scope="col"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($users as $user)
                <tr name="{{ $user->username }}">
                    <td>
                        <p>{{ $user->firstnames ?? $user->username }} {{ $user->surname ?? ''}}</p>
                    </td>
                    <td>
                        @forelse($user->roles() as $role)
                            @if($loop->last)
                                {{ $role }}
                            @else
                                {{ $role . ',' }}
                            @endif
                        @empty
                            {{ 'None' }}
                        @endforelse
                    </td>
                    <td class="text-center">
                        <a href="{{ URL::route('masquerade', ['username' => $user->username]) }}" class="text-danger">Masquerade</a>
                    </td>
                    
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<script>
 $(document).ready(function() {
     $('#userList').DataTable( {
         "order": [[ 2, "asc" ]]
     } );
 } );
</script>

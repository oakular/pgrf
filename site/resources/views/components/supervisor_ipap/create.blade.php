<div class="modal fade" id="add-staff-{{ strtolower($relationship) }}" tabindex="-1" role="dialog" aria-labelledby="add-staff-{{ $relationship }}" aria-hidden="true" dusk="add-staff-{{ strtolower($relationship) }}-modal">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add {{ ucfirst($relationship) }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form" action="{{ $url }}" method="post" enctype="multipart/form-data">
                @csrf

                <div class="modal-body">

                    <div class="form-group">
                        <select class="custom-select" dusk="new-{{ strtolower($relationship) }}" name="{{ strtolower($relationship) }}" id="new-{{ strtolower($relationship) }}" required>
                            {{ $options }}
                        </select>
                        <input value="{{ $student->username }}" name="student" hidden/>
                    </div>
                    
                    <div class="modal-footer">
                        <button class="btn btn-secondary" data-dismiss="modal" type="button">Cancel</button>
                        <button type="submit" class="btn btn-success" dusk="submit-new-{{ strtolower($relationship) }}">Add {{ ucfirst($relationship) }}</button>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>

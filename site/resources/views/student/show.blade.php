@extends('layouts.app')

@section('title')
    {{ $student->firstnames }} {{ $student->surname }} - Details
@endsection

@section('profile')
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <span style="text-align:left;">
                    {{ $student->firstnames ?? $student->username }} {{ $student->surname ?? '' }} 
                </span>
                <span style="float: right;">
                    <a href="{{ URL::route('student.edit', ['id' => $student->username]) }}">
                        <i class="fas fa-pen-square text-primary"></i>
                    </a>
                </span>
            </h4>
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">
                Email:
                <a href="mailto:{{ $student->email ?? '' }}">{{ $student->email ?? 'Unknown' }}</a>
            </li>
            <li class="list-group-item">Department: {{ $student->student->detail->department->value ?? 'Unknown' }}</li>
            <li class="list-group-item">Start Date: {{ date_format(date_create($student->student->detail->StartDate), 'jS M Y') ?? 'Unknown' }}</li>
            <li class="list-group-item">Study Year: {{ $student->student->detail->YearOfStudy ?? 'Unknown' }}</li>
            @if ($student->student->detail->is_xjtlu ?? false)
                <li class="list-group-item">XJTLU</li>
            @endif
            @if ($student->student->detail->is_part_time ?? false)
                <li class="list-group-item">Part Time</li>
            @endif
        </ul>
    </div>
@endsection

@section('workflows')
    <h4>
        <span style="text-align: left;">Workflows</span>
        @if (auth()->user()->isAdmin())
            <span style="float: right;">
                <a class="btn btn-sm btn-success" type="button" href="{{ URL::route('workflow.create', ['student' => $student->username]) }}">Add</a>
            </span>
        @endif
    </h4>
    @forelse ($student->student->workflows as $workflow)
        <div class="mb-3">
            @component('components.workflows.show', ['workflow' => $workflow])
            @endcomponent
        </div>
    @empty
            <div class="card">
                <div class="card-body text-center">
                    <p>No workflows exist</p>
                </div>
            </div>
    @endforelse
@endsection

@section('submitted-documents')
    <h4>Submitted Documents</h4>
    @forelse($student->student->documents as $document)
        @component('components.document', ['doc' => $document])
        @endcomponent
    @empty
        <div class="card mt-3">
            <div class="card-body">
                <p class="card-title">No documents have been submitted yet</p>
            </div>
        </div>
    @endforelse
@endsection

@section('supervisors')
    @component('components.staff-list', ['student' => $student->student, 'staffMembers' => $student->student->supervisors, 'route' => 'supervisor.destroy'])
        @slot('subTitle')
            <span style="text-align: left;">Supervisors</span>
            @if (auth()->user()->isAdmin())
                <span style="float: right;">
                    <button class="btn btn-sm btn-success" type="button" data-toggle="modal" data-target="#add-staff-supervisor" dusk="add-supervisor">Add</button>
                </span>
            @endif
        @endslot

        @slot('relationship')
            supervisor
        @endslot
    @endcomponent
@endsection

@section('ipaps')
    @component('components.staff-list', ['student' => $student->student, 'staffMembers' => $student->student->ipaps, 'route' => 'ipap.destroy'])
        @slot('subTitle')
            <span style="text-align: left;">IPAP Members</span>

            @if (auth()->user()->isAdmin())
                <span style="float: right;">
                    <button class="btn btn-sm btn-success" type="button" data-toggle="modal" data-target="#add-staff-ipap" dusk="add-ipap">Add</button>
                </span>
            @endif
        @endslot

        @slot('relationship')
            ipap
        @endslot
    @endcomponent
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-4 mt-3">
            @yield('profile')
        </div>

        <div class="col-sm-8 mt-3">
            <div class="mb-5">
                @yield('workflows')
                <hr/>
            </div>

            <div class="mb-5">
                @yield('submitted-documents')
                <hr/>
            </div>

            <div class="mb-3">
                @yield('supervisors')
            </div>

            @yield('ipaps')
        </div>
    </div>

    <div class="modal fade" id="confirm-exemption" tabindex="-1" role="dialog" aria-labelledby="confirm-exemption" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Please confirm</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Only request an exemption if the student has completed more than 50% of the write up of
                    their thesis.
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal" type="button">Cancel</button>
                    <a type="button" class="btn btn-danger" href="{{ route('exemption.request', ['username' => $student->username]) }}" dusk="confirm-exemption">Request Exemption</a>
                </div>
            </div>
        </div>
    </div>

    @if(auth()->user()->isAdmin())
        @component('components.supervisor_ipap.create', ['student' => $student->student, 'url' => URL::route('supervisor.store')])
            @slot('relationship')
                supervisor
            @endslot

            @slot('options')
                @foreach(\App\LUCS_User::staffMembers()->orderBy('surname', 'asc')->get() as $supervisor)
                    <option value="{{ $supervisor->username }}">{{ $supervisor->firstnames }}  {{ $supervisor->surname }}</option>
                @endforeach
            @endslot
        @endcomponent
    @endif

    @if(auth()->user()->isAdmin())
        @component('components.supervisor_ipap.create', ['student' => $student->student, 'url' => URL::route('ipap.store')])
            @slot('relationship')
                IPAP
            @endslot

            @slot('options')
                @foreach(\App\LUCS_User::staffMembers()->orderBy('surname', 'asc')->get() as $ipap)
                    <option value="{{ $ipap->username }}">{{ $ipap->firstnames }}  {{ $ipap->surname }}</option>
                @endforeach
            @endslot
        @endcomponent
    @endif
@endsection

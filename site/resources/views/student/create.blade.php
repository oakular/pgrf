@extends('layouts.app')

@section('title')
    New Student
@endsection

@section('content')
    <div class="row-sm mt-3">
        @component('components.welcome-msg')
            @slot('msg')
                <h4 class="mb-3">
                    <span style="text-align:left;">
                        Create New Student 
                    </span>
                    <span style="float:right;">
                        <a class="btn btn-info" href="{{ URL::route('student.bulk.create') }}">Bulk Create</a>
                    </span>
                </h4>
            @endslot
        @endcomponent
    </div>
    <div class="row-sm mt-3">
        <div class="col-sm">
            @component('components.alert')
                @slot('print_errors')
                    @foreach ($errors->all() as $error)
                        {{ $error }}
                        @unless($loop->last)
                    <hr/>
                        @endunless
                    @endforeach
                @endslot
            @endcomponent

            <form action="{{ URL::route('student.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="names">Student Name</label>
                    <div id="names" class="form-row">
                        <div class="col-sm">
                            <input type="text" dusk="firstname" name="firstName" class="form-control" placeholder="Full name" required>
                        </div>
                        <div class="col-sm">
                            <input type="text" dusk="surname" name="lastName" class="form-control" placeholder="Last name" required>
                        </div>
                    </div>
                </div>

                <div class="form-group mt-3">
                    <div class="form-row">
                        <div class="col-sm">
                            <label for="email">Username</label>
                            <div class="input-group">
                                <input id="username" dusk="username" name="username" type="text" class="form-control" placeholder="Username" required/>
                            </div>
                        </div>
                        <div class="col-sm">
                            <label for="email">Email Address</label>
                            <div class="input-group">
                                <input id="email" dusk="email" name="email" type="text" class="form-control" placeholder="Email" required/>
                                <div class="input-group-append"><span class="input-group-text">{{ '@liverpool.ac.uk' }}</span></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group mt-3">
                    <div class="form-row">
                        <div class="col-sm-3">
                            <label for="startDate">Start Date</label>
                            <input dusk="start-date" id="startDate" name="startDate" class="form-control" type="date" required/>
                        </div>
                        <div class="col-sm-3 ml-4">
                            <label for="department">Department</label>
                            <select name="department" id="department" class="custom-select" dusk="department" required>
                                @foreach (\App\Department::all() as $department)
                                    <option value="{{ $department->id }}">{{ $department->value }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-2 mt-5 ml-2">
                            <div class="form-check">
                                <input dusk="is-xjtlu" id="is-xjtlu" name="is-xjtlu" class="form-check-input" type="checkbox"/>
                                <label for="is-xjtlu" class="form-check-label">XJTLU</label>
                            </div>
                        </div>
                        <div class="col-sm-2 mt-5">
                            <div class="form-check">
                                <input dusk="is-part-time" id="is-part-time" name="is-part-time" class="form-check-input" type="checkbox"/>
                                <label for="is-part-time" class="form-check-label">Part Time</label>
                            </div>
                        </div>
                    </div>
                </div>

                <hr/>

                <div class="form-row">
                    @for ($incr = 0; $incr < 2; $incr++)
                        <div class="col-sm">
                            <div class="form-group mt-3">
                                <label for="supervisors[{{ $incr }}]">Supervisor {{ $incr+1 }}</label>
                                <select dusk="supervisors[{{ $incr }}]" class="custom-select" id="supervisors[{{ $incr }}" name="supervisors[{{ $incr }}]" required>
                                    <option value="null" selected>None</option>
                                    @foreach (\App\Staff::all() as $supervisor)
                                        <option value="{{ $supervisor->username }}">{{ $supervisor->lucsUser->getFullNameOrUsername() }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endfor
                </div>

                <div class="form-row">
                    @for ($incr = 0; $incr < 2; $incr++)
                        <div class="col-sm">
                            <div class="form-group mt-3">
                                <label for="ipaps[{{ $incr }}]">IPAP Member {{ $incr+1 }}</label>
                                <select dusk="ipaps[{{ $incr }}]" class="custom-select" id="ipaps[{{ $incr }}]" name="ipaps[{{ $incr }}]" required>
                                    <option value="null" selected>None</option>
                                    @foreach (\App\Staff::all() as $ipap)
                                        <option value="{{ $ipap->username }}">{{ $ipap->lucsUser->getFullNameOrUsername() }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endfor
                </div>

                <button dusk="submit" type="submit" class="btn btn-success mt-3 mb-3 float-right">Create</button>
            </form>
        </div>
    </div>
@endsection

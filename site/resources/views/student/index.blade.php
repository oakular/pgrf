@extends('layouts.app')

@section('title')
    All Students
@endsection

@section('content')
    @component('components.student-list', ['students' => \App\Student::all(), 'id' => 1])
    @endcomponent
@endsection

@extends('layouts.app')

@section('title')
    Edit Details - {{ $student->lucsUser->firstnames }} {{ $student->lucsUser->surname }}
@endsection

@section('content')
    <div class="row-sm mt-3">
        @component('components.welcome-msg')
        @slot('msg')
        Edit Details - {{ $student->lucsUser->firstnames }} {{ $student->lucsUser->surname }}
        @endslot
        @endcomponent
    </div>
    <div class="row-sm mt-3">
        <div class="col-sm">
            @component('components.alert')
            @slot('print_errors')
            @foreach ($errors->all() as $error)
                {{ $error }}
                @unless($loop->last)
                    <hr/>
                @endunless
            @endforeach
            @endslot
            @endcomponent

            <form action="{{ URL::route('student.update', ['id' => $student->username]) }}" method="post">
                @method('PUT')
                @csrf
                <div class="form-group">
                    <label for="names">Student Name</label>
                    <div id="names" class="row">
                        <div class="col-sm-5">
                            <input type="text" name="firstName" class="form-control" value="{{ $student->lucsUser->firstnames }}" placeholder="First name" required/>
                        </div>
                        <div class="col-sm-5">
                            <input type="text" name="lastName" class="form-control" value="{{ $student->lucsUser->surname }}" placeholder="Last name" required/>
                        </div>
                    </div>
                </div>

                <div class="form-group mt-3">
                    <div class="row">
                        <div class="col-sm-5">
                            <label for="email">Email Address</label>
                            <div class="input-group">
                                <input id="email" name="email" type="text" class="form-control" value="{{ str_before($student->lucsUser->email, '@') }}" placeholder="Username" required/>
                                <div class="input-group-append"><span class="input-group-text">{{ '@liverpool.ac.uk' }}</span></div>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <label for="startDate">Start Date</label>
                            <input id="startDate" name="startDate" class="form-control" value="{{ $student->detail->StartDate }}" type="date" required/>
                        </div>

                        <div class="col-sm-2">
                            <label for="yearOfStudy">Year of Study</label>
                            <input class="form-control" id="yearOfStudy" name="yearOfStudy" type="number" value="{{ $student->detail->YearOfStudy }}" min="1" max="4" required/>
                        </div>
                    </div>
                </div>

                <div class="form-group mt-3">
                    <div class="form-row">
                        <div class="col-sm-3">
                            <div class="form-check">
                                <input dusk="is-xjtlu" id="is-xjtlu" name="is-xjtlu" class="form-check-input" type="checkbox" {{ $student->detail->is_xjtlu ? 'checked' : ''}}/>
                                <label for="is-xjtlu" class="form-check-label">XJTLU</label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-check">
                                <input dusk="is-part-time" id="is-part-time" name="is-part-time" class="form-check-input" type="checkbox" {{ $student->detail->is_part_time ? 'checked' : ''}}/>
                                <label for="is-part-time" class="form-check-label">Part Time</label>
                            </div>
                        </div>
                    </div>
                </div>

                <hr/>
                <a href="{{ URL::route('student.show', ['id' => $student->username]) }}" dusk="cancel" class="btn btn-secondary float-left">Cancel</a>
                <button type="submit" dusk="submit" class="btn btn-success mt-3 mb-3 float-right">Submit</button>
            </form>
        </div>
    </div>
@endsection

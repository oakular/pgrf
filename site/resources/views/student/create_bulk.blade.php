@extends('layouts.app')

@section('title')
    New Student
@endsection

@section('content')
    <div class="row-sm mt-3">
        @component('components.welcome-msg')
            @slot('msg')
                Bulk Create Students
            @endslot
        @endcomponent

        @component('components.alert')
            @slot('print_errors')
                @foreach ($errors->all() as $error)
                    {{ $error }}
                    @unless($loop->last)
                <hr/>
                    @endunless
                @endforeach
            @endslot
        @endcomponent
    </div>
    <form class="row col-sm mt-3" action="{{ URL::route('student.bulk.store') }}" enctype="multipart/form-data" method="POST">
        @csrf 
        <div class="form-group custom-file mt-3">
                <label class="custom-file-label col-sm" for="student-data">Choose a file</label>
                <input type="file" class="col-sm" dusk="student-data" name="student-data" id="student-data"/>
        </div>
        <div class="form-group mt-3">
            <input class="btn btn-success float-right" dusk="submit" type="submit" value="Upload"/>
        </div>
    </form>
@endsection

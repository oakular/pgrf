@component('mail::message')
# Document Submission Receipt

This email confirms that your document was submitted successfully.

@component('mail::button', ['url' => $url])
View Submission
@endcomponent

{{ config('app.name') }}
@endcomponent

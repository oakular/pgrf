@component('mail::message')
# New Document Submission

{{ $student->lucsUser->firstnames  }} {{ $student->lucsUser->surname }} has submitted a new document.
Click the button below to view it, and offer your feedback.

@component('mail::button', ['url' => $url])
Enter Feedback
@endcomponent

{{ config('app.name') }}
@endcomponent

@component('mail::message')
# Document Late Alert for {{ $student->lucsUser->firstnames }} {{ $student->lucsUser->surname }}

This email is to alert you that no document was submitted by {{ $student->lucsUser->firstnames }} {{ $student->lucsUser->surname }}
for their {{ $workflow_type->value }}.

@component('mail::button', ['url' => $url])
Login
@endcomponent

{{ config('app.name') }}
@endcomponent

@component('mail::message')
# Document Late

We did not receive a submission from you for your {{ $workflow_type->value }} yesterday.
Please submit a document as soon as possible by logging into the PGR system.

@component('mail::button', ['url' => $url])
Login
@endcomponent

{{ config('app.name') }}
@endcomponent

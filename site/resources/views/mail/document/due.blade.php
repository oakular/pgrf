@component('mail::message')
# Your Document is Due

Your document is due to be submitted in one month. Don't forget to submit
_before_ the deadline

{{-- @component('mail::button', ['url' => ''])
Button Text
@endcomponent --}}

{{ config('app.name') }}
@endcomponent

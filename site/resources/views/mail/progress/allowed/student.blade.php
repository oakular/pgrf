@component('mail::message')
# Progression Confirmed!

Congratulations, your IPAP members have confirmed that you may progress. 

{{ config('app.name') }}
@endcomponent

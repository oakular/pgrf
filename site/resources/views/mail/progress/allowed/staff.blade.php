@component('mail::message')
# Progression Confirmed for {{ $student->lucsUser->firstnames }} {{ $student->lucsUser->surname }}

IPAP member {{ $student->lucsUser->firstnames }} {{ $student->lucsUser->surname }} for have confirmed their progression.

{{ config('app.name') }}
@endcomponent

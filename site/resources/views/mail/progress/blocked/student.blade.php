@component('mail::message')
# Progress Committee Attention Required

This alert is to inform you that your IPAP members have referred your
progression to the Progress Committee for further review.

{{ config('app.name') }}
@endcomponent

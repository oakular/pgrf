@component('mail::message')
# Progress Committee Attention Required

The progression of {{ $student->lucsUser->firstnames }} {{ $student->lucsUser->surname }} is being
referred to the Progress Committee for further review.

{{ config('app.name') }}
@endcomponent

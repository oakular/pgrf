@component('mail::message')
# Feedback Submission Late

    You have outstanding feedback for the student {{ $student->lucsUser->firstnames }} {{ $student->lucsUser->surname }}.
    Please submit your feedback as soon as possible.

@component('mail::button', ['url' => $url])
Enter feedback
@endcomponent

{{ config('app.name') }}
@endcomponent

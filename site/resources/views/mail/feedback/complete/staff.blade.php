@component('mail::message')
# Introduction

Feedback is ready for {{ $student->lucsUser->firstnames }} {{ $student->lucsUser->surname }}

@component('mail::button', ['url' => $url])
View Feedback
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

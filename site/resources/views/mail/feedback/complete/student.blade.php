@component('mail::message')
# Introduction

Feedback is ready for your {{ $document->type->value }}

@component('mail::button', ['url' => $url])
View Feedback
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

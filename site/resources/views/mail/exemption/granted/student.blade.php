@component('mail::message')
# Exemption Granted

You have been granted an exemption from the IPAP report and meeting.

{{ config('app.name') }}
@endcomponent

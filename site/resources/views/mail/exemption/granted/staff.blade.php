@component('mail::message')
# Exemption Granted for {{ $student->lucsUser->firstname }} {{ $student->lucsUser->surname }}

You request to exempt {{ $student->lucsUser->firstname }} {{ $student->lucsUser->surname }}
from their IPAP report and meeting has been approved.

{{ config('app.name') }}
@endcomponent

@component('mail::message')
# An exemption has been requested for {{ $student->lucsUser->firstnames }} {{ $student->lucsUser->surname }}

Click below to grant the exemption.

@component('mail::button', ['url' => $url])
Grant Exemption
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

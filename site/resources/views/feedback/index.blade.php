@extends('layouts.app')

@section('title')
    {{ $document->student->lucsUser->firstnames ?? $document->student->username }} {{ $document->student->lucsUser->surname ?? '' }} - {{ $document->type->value ?? 'Unknown Document Type' }} Feedback
@endsection

@section('content')
    <div class="row mt-3">
        @component('components.alert')
            @slot('print_errors')
                {{ $errors->first() }}
            @endslot
        @endcomponent
    </div>

    <div class="row mt-3">
        <div class="col-md-10">
            @component('components.welcome-msg')
                @slot('msg')
                    {{ $document->student->lucsUser->firstnames ?? $document->student->username }} {{ $document->student->lucsUser->surname ?? '' }} - {{ $document->type->value ?? 'Unknown Document Type' }} Feedback
                @endslot
            @endcomponent
        </div>

        <div class="col-md-2">
            <a class="btn btn-info" href="{{ URL::route('document.show', ['id' => $document->id]) }}">View Document</a>
        </div>
    </div>

    <div class="row-sm mt-3 mb-3">
        <div class="col-sm">
            @yield('feedback.content')
        </div>
    </div>
@endsection

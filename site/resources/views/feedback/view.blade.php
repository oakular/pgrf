@extends('feedback.index')

@section('feedback.content')
    @if (str_contains($feedback->document->type->value, 'End of Year Report'))
        <div class="form-group">
            <div class="row mt-3">
                <div class="card" style="width: 100%">
                    <div class="card-body">
                        Grading: {{ $feedback->grade->value }}
                    </div>
                </div>
            </div>
        </div>
    @endif

    @foreach ($questions as $question)
        <div class="row mt-3">
            <div class="card" style="width: 100%" id="comment-group{{ $loop->iteration }}">
                <div class="card-body">
                    <h6 class="card-title">{{ $loop->iteration }}. {{ $question->value }}</h6>
                    <hr>
                    <p class="card-text">{{ $answers[$loop->index] }}</p>
                </div>
            </div>
        </div>
    @endforeach

    @if($feedback->grade->value == "Unsatisfactory" && $feedback->document->isEndOfYearReport())
        @unless(auth()->user()->isStudent())
            <a href="{{ URL::route('feedback.committee_data.show', ['data' => $feedback->dataForProgressCommittee->id]) }}" type="button" class="btn btn-info mt-3">View Comments for Progress Committee</a>
        @endunless
    @endif
@endsection

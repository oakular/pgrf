@extends('layouts.app')

@section('content')
    <form action="{{ URL::route('feedback.committee_data.store') }}" method="post">
        @csrf
        <div class="form-group mt-3">
            <div class="card">
                <div class="card-body">
                    <label for="comment">
                        <h6 class="card-title">Comments for the Progress Committee</h6>
                    </label>
                    <textarea dusk="comment" class="form-control" rows="10" placeholder="Enter feedback..." name="comment" required></textarea>
                </div>
            </div>
        </div>

        <div class="form-group mt-3">
            <div class="card">
                <div class="card-body">
                    <label for="recommendation">Recommended action for the Progress Committee</label>
                    <select class="custom-select" name="recommendation" dusk="recommendation">
                        @foreach(\App\ProgressCommitteeRecommendation::all() as $recommendation)
                            <option value="{{ $recommendation->id }}">{{ $recommendation->value }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <input type="text" value="{{ $feedback->id }}" name="feedback" hidden/>
        <button dusk="submit" type="submit" class="btn btn-success mt-3 mb-3 float-right">Submit</button>
    </form>
@endsection

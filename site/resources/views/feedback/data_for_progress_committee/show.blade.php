@extends('layouts.app')

@section('title')
    {{ 'Comments for the Progress Committee' }}
@endsection

@section('content')
    <div class="row mt-3">
        <div class="card" style="width: 100%">
            <div class="card-body">
                <h6 class="card-title">Comments for the Progress Committee</h6>
                <hr>
                <p class="card-text">{{ $data->comments ?? 'None' }}</p>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="card" style="width: 100%">
            <div class="card-body">
                <h6 class="card-title">Recommendation for the Progress Committee</h6>
                <hr>
                <p class="card-text">{{ $data->recommendation->value ?? 'None' }}</p>
            </div>
        </div>
    </div>
@endsection

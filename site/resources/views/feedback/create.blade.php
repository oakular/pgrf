@extends('feedback.index')

@section('feedback.content')
    <form action="{{ URL::route('feedback.upload') }}" method="post">
        @csrf
        @if (str_contains($document->type->value, 'End of Year Report'))
            <div class="form-group">
                <div class="row mt-3">
                    <div class="card" style="width: 100%">
                        <div class="card-body">
                            <h6 for="grade" class="card-title">Grading</h6>
                            <select style="width: 30%" dusk="grade" class="custom-select" id="grade" name="grade" required>
                                @foreach (array('Excellent', 'Good', 'Satisfactory', 'Unsatisfactory') as $key=>$grade)
                                    <option value="{{ $grade }}">{{ $grade }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @foreach ($questions as $question)
            <div class="form-group" id="comment-group{{ $loop->iteration }}">
                <div class="row mt-3">
                    <div class="card" style="width: 100%">
                        <div class="card-body">
                            <label for="comment[{{ $loop->iteration }}]">
                                <h6 class="card-title">{{ $loop->iteration }}. {{ $question->value }}</h6>
                            </label>
                            <textarea dusk="comment{{ $loop->index }}" class="form-control" rows="10" placeholder="Enter feedback..." name="comment[{{ $loop->iteration }}]" required></textarea>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

        <select class="form-control" name="docId")" hidden>
            <option value="{{ $document->id }}" selected>{{ $document->id }}</option>
        </select>
        <button dusk="submit" type="submit" class="btn btn-success mt-3 mb-3 float-right">Submit</button>
    </form>
@endsection

@extends('layouts.app')

@section('title')
    {{ 'Progress Committee Tasks' }}
@endsection

@section('content')
    <div class="row-sm mt-3">
        <h4 class="mb-3">
            <span style="text-align:left;">
                Students Requiring Attention
            </span>
        </h4>
        <div class="mt-3 mb-3">
            @forelse ($workflows as $workflow)
                {{ $workflow->student->lucsUser->firstnames ?? $workflow->student->username }} {{ $workflow->student->lucsUser->surname ?? '' }}
                @component('components.document', ['doc' => $workflow->student->documents->where('workflow_type_id', $workflow->type->id)->first()])
                @endcomponent
            @empty
                <div class="card">
                    <div class="card-body">
                        <p>No students currently need your attention</p>
                    </div>
                </div>
            @endforelse
        </div>
    </div>
@endsection

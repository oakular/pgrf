@extends('layouts.app')

@section('title')
    {{ 'My Supervising Students' }}
@endsection

@section('content')
    <div class="row-sm mt-3">
        <h4>Currently Supervising Students</h4>
        @component('components.student-list', ['students' => auth()->user()->staff->supervisingStudents, 'id' => 1])
        @endcomponent
    </div>
@endsection

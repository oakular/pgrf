@extends('layouts.app')

@section('title')
    {{ 'My IPAP Students' }}
@endsection

@section('content')
    <div class="row-sm mt-3">
        <h4>My IPAP Students</h4>
        @component('components.student-list', ['students' => auth()->user()->staff->ipapStudents, 'id' => 1])
        @endcomponent
    </div>
@endsection

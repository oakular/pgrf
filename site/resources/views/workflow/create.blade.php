@extends('layouts.app')

@section('title')
    New Workflow for {{ $student->lucsUser->firstnames ?? $student->username }} {{ $student->lucsUser->surname ?? '' }}
@endsection

@section('content')
<div class="row-sm mt-3">
    @component('components.welcome-msg')
        @slot('msg')
            Create New Workflow for {{ $student->lucsUser->firstnames ?? $student->username }} {{ $student->lucsUser->surname ?? '' }}
        @endslot
    @endcomponent
</div>

<div class="row col-sm mt-3 justify-content-center">
    <form action="{{ URL::route('workflow.store') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="workflow-type">Workflow Type</label>
            <select name="workflow-type" class="custom-select" dusk="workflow-type">
                @foreach (\App\WorkflowType::all() as $type)
                    <option value="{{ $type->id }}">{{ $type->value }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="status">Status</label>
            <select name="status" class="custom-select" dusk="status">
                @foreach (\App\Status::all() as $status)
                    <option value="{{ $status->id }}">{{ $status->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="deadline">Deadline</label>
            <input name="deadline" type="date" required class="form-control" dusk="deadline"/>
        </div>

        <input class="form-control" name="student-username" value="{{ $student->username }}" hidden/>

        <a class="btn btn-secondary" href="{{ URL::previous() }}">Cancel</a>
        <button class="btn btn-success float-right" type="submit" dusk="submit">Create</button>
    </form>
</div>
@endsection

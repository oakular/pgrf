@extends('layouts.app')

@section('title')
    {{ $workflow_type->value ?? 'Unknown Workflow Type' }} - Overview
@endsection

@section('content')
    <div class="row-sm mt-3">
        <div class="row">
            <h4>
                <span style="text-align: left">
                    {{ $workflow_type->value ?? 'Unknown Workflow Type' }} - Overview
                </span>
            </h4>
        </div>
        <div class="mt-3 mb-3">
            {{ $workflows = \App\Workflow::where('workflow_type_id', '=', $workflow_type->id)->paginate(50) }}
            <table class="display table table-responsive-sm table-hover" id="workflow_type-overview-table" dusk="workflow_type-overview-table">
                <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Start Date</th>
                        <th scope="col">Year of Study</th>
                        <th scope="col">Status</th>
                    </tr>
                </thead>

                <tbody>

                    @foreach ($workflows as $workflow)
                        <tr name="{{ $workflow->student->username }}" class="@if (\App\Helpers\StatusColorHelper::shouldWarn($workflow->status)) bg-warning @endif">
                            <td><a href="{{ URL::route('student.show', ['id' => $workflow->student->username]) }}">{{ $workflow->student->lucsUser->firstnames ?? $workflow->student->username }} {{ $workflow->student->lucsUser->surname ?? '' }}</a></td>
                            <td>{{ date_format(date_create($workflow->student->detail->StartDate), 'jS M Y') ?? 'Unknown' }}</td>
                            <td align="center">{{ $workflow->student->detail->YearOfStudy ?? 'Unknown' }}</td>
                            <td align="center">{{ $workflow->status->name ?? 'Unknown' }}</td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
            {{ $workflows->links() }}
        </div>
    </div>

    <script>
     $(document).ready(function() {
         $('#workflow_type-overview-table').DataTable( {
             "order": [[ 2, "asc" ], [0, 'asc']]
         } );
     } );
    </script>
@endsection

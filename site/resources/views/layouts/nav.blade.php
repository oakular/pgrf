@section('navbar')
    <nav class="navbar navbar-expand-lg navbar-dark" style="background: #031C69">
        <a class="navbar-brand" href="{{ URL::route('intrahome') }}">Intranet Home</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li dusk="home-button" class="nav-item {{ Request::routeIs('home') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ URL::route('home') }}">Home</a>
                </li>
                @if (auth()->user()->isStudent())
                    <li class="nav-item {{ Request::routeIs('student.show') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ URL::route('student.show', ['id' => auth()->user()->username]) }}">My Details</a>
                    </li>
                @elseif (auth()->user()->isAdmin())
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Summaries
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            @foreach (\App\WorkflowType::all() as $workflow_type)
                                <a class="dropdown-item" href="{{ URL::route('workflow_type.show', ['id' => $workflow_type->id]) }}">{{ $workflow_type->value }}</a>
                            @endforeach
                        </div>
                    </li>
                    <li class="nav-item" {{ Request::routeIs('user.index') ? 'active' : '' }}>
                        <a class="nav-link" href="{{ URL::route('user.index') }}">All Users</a>
                    </li>
                @endif
                @if(auth()->user()->isIpap())
                    <li class="nav-item {{ Request::routeIs('staff_views.students.ipap.show') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ URL::route('staff_views.students.ipap.show') }}">My IPAP Students</a>
                    </li>
                @endif
                @if(auth()->user()->isSupervisor())
                    <li class="nav-item {{ Request::routeIs('staff_views.students.supervising.show') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ URL::route('staff_views.students.supervising.show') }}">My Supervising Students</a>
                        </li>
                @endif
                @if (Cookie::has('masquerading_user'))
                    <li class="nav-item mr-sm-2 ml-auto">
                        <a class="btn btn-danger" href="{{ URL::route('masquerade.stop') }}">Stop Masquerading</a>
                    </li>
                @endif
            </ul>
        </div>
    </nav>
@show

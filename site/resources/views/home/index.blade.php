@extends('layouts.app')

@section('title')
    Home - {{ auth()->user()->firstnames }} {{ auth()->user()->surname }}
@endsection

@section('content')
    <div class="row-sm mt-3">
        <div class="col-sm">
            @component('components.welcome-msg')
                @slot('msg')
                    Welcome {{ auth()->user()->firstnames }} {{ auth()->user()->surname }}
                @endslot
            @endcomponent
        </div>
    </div>

    <div class="row-sm mt-3">
        <div class="col">
            @component('components.alert')
            @slot('print_errors')
            @foreach ($errors->all() as $error)
                File upload failed: {{ $error }}
            @endforeach
            @endslot
            @endcomponent
        </div>
    </div>

    <div class="row-sm">
        <div class="col">
            @yield('home.content')
        </div>
    </div>
@endsection

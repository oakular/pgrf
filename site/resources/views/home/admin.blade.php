@extends('home.index')

@section('home.content')
    <div class="row-sm mt-3">
        <h4 class="mb-3">
            <span style="text-align:left;">
                All Students
            </span>
            <span style="float:right;">
                <a class="btn btn-info" href="{{ URL::route('student.create') }}">New Student</a>
            </span>
        </h4>
        {{-- $students->links() --}}
        @component('components.student-list', ['students' => $students])
        @endcomponent
        {{-- $students->links() --}}
    </div>
@endsection

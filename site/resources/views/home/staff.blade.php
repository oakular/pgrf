@extends('home.index')

@section('home.content')
    @if(auth()->user()->isIpap())
        <div class="row-sm">
            <h4>Outstanding Feedback</h4>
            @forelse(auth()->user()->staff->documentsNeedingAttention() as $document)
                <h5>{{ $document->student->lucsUser->firstnames ?? $document->student->username }} {{ $document->student->lucsUser->surname ?? '' }}</h5>
                @component('components.document', ['doc' => $document])
                @endcomponent
            @empty
                <div class="card">
                    <div class="card-body">
                        <p>You have no outstanding feedback</p>    
                    </div>
                </div>
            @endforelse
        </div>
        <div class="row-sm mt-3">
            <h4>My IPAP Students</h4>
            @component('components.student-list', ['students' => auth()->user()->staff->ipapStudents, 'id' => 1])
            @endcomponent
        </div>
    @endif

    @if(auth()->user()->isSupervisor())
        <div class="row-sm mt-3">
            <h4>Currently Supervising Students</h4>
            @component('components.student-list', ['students' => auth()->user()->staff->supervisingStudents, 'id' => 2])
            @endcomponent
        </div>
    @endif

    @if(auth()->user()->isAdmin())
        <h4 class="mb-3">
            <span style="text-align:left;">
                All Students
            </span>
            <span style="float:right;">
                <a class="btn btn-info" href="{{ URL::route('student.create') }}">New Student</a>
            </span>
        </h4>
        @component('components.student-list', ['students' => \App\Student::all(), 'id' => 3])
        @endcomponent
    @endif
@endsection

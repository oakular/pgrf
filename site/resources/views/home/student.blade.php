@extends('home.index')

@section('home.content')
    <div class="row-sm">
        <h4>Submitted Documents</h4>
        @forelse(auth()->user()->student->documents as $document)
            @component('components.document', ['doc' => $document])
            @endcomponent
        @empty
            <div class="card mt-3">
                <div class="card-body">
                    <p class="card-title">No documents have been submitted yet</p>
                </div>
            </div>
        @endforelse
    </div>
    
    @unless($user->student->workflows->count() == 0)
        <div class="row-sm mt-3 mb-3">
            @include('components.document-upload', ['workflows' => $user->student->workflows])
        </div>
    @endunless
@endsection

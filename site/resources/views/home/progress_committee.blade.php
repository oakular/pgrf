@extends('home.index')

@section('home.content')
    <div class="row-sm mt-3">
        <h4 class="mb-3">
            <span style="text-align:left;">
                Students Requiring Attention
            </span>
        </h4>
        {{-- $workflows->links() --}}
        <div class="mt-3 mb-3">
            @foreach ($workflows as $workflow)
                {{ $workflow->student->lucsUser->firstnames ?? $workflow->student->username }} {{ $workflow->student->lucsUser->surname ?? '' }}
                @component('components.document', ['doc' => $workflow->student->documents->where('workflow_type_id', $workflow->type->id)->first()])
                @endcomponent
            @endforeach
        </div>
        {{-- $workflows->links() --}}
    </div>
@endsection

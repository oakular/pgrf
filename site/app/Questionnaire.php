<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    protected $connection = 'pgrf';

    public $timestamps = false;

    public function questions()
    {
        return $this->belongsToMany('App\Question', 'questions_questionnaires');
    }

    public function workflowType()
    {
        return $this->belongsTo('App\WorkflowType');
    }
}

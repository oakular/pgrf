<?php

namespace App\Policies;

use App\LUCS_User;
use App\Student;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\DB;

class StudentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the student.
     *
     * @param  \App\LUCS_User  $user
     * @param  \App\Student  $student
     * @return mixed
     */
    public function view(LUCS_User $user, Student $student)
    {
        if ($user->isAdmin()) {
            return true;
        } else if ($user->username === $student->username) {
            return true;
        } else if (DB::table('supervisor')->where([['staff_username', '=', $user->username], ['student_username', '=', $student->username]])->exists()) {
            return true;
        } else if (DB::table('ipap')->where([['staff_username', '=', $user->username], ['student_username', '=', $student->username]])->exists()) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can create students.
     *
     * @param  \App\LUCS_User  $user
     * @return mixed
     */
    public function create(LUCS_User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can update the student.
     *
     * @param  \App\LUCS_User  $user
     * @param  \App\Student  $student
     * @return mixed
     */
    public function update(LUCS_User $user, Student $student)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can delete the student.
     *
     * @param  \App\LUCS_User  $user
     * @param  \App\Student  $student
     * @return mixed
     */
    public function delete(LUCS_User $user, Student $student)
    {
        return $user->isAdmin();
    }
}

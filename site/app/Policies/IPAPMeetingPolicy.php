<?php

namespace App\Policies;

use App\LUCS_User;
use App\IPAPMeeting;
use Illuminate\Auth\Access\HandlesAuthorization;

class IPAPMeetingPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any IPAP meetings.
     *
     * @param  \App\LUCS_User  $user
     * @return mixed
     */
    public function viewAny(LUCS_User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the IPAP meeting.
     *
     * @param  \App\LUCS_User  $user
     * @param  \App\IPAPMeeting  $iPAPMeeting
     * @return mixed
     */
    public function view(LUCS_User $user, IPAPMeeting $iPAPMeeting)
    {
        //
    }

    /**
     * Determine whether the user can create IPAP meetings.
     *
     * @param  \App\LUCS_User  $user
     * @return mixed
     */
    public function create(LUCS_User $user)
    {

    }

    /**
     * Determine whether the user can update the IPAP meeting.
     *
     * @param  \App\LUCS_User  $user
     * @param  \App\IPAPMeeting  $iPAPMeeting
     * @return mixed
     */
    public function update(LUCS_User $user, IPAPMeeting $ipap_meeting)
    {
        $student = $ipap_meeting->workflow->student;
        return $student->supervisors->contains('username', $user->username);
    }

    /**
     * Determine whether the user can delete the IPAP meeting.
     *
     * @param  \App\LUCS_User  $user
     * @param  \App\IPAPMeeting  $iPAPMeeting
     * @return mixed
     */
    public function delete(LUCS_User $user, IPAPMeeting $iPAPMeeting)
    {
        //
    }

    /**
     * Determine whether the user can restore the IPAP meeting.
     *
     * @param  \App\LUCS_User  $user
     * @param  \App\IPAPMeeting  $iPAPMeeting
     * @return mixed
     */
    public function restore(LUCS_User $user, IPAPMeeting $iPAPMeeting)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the IPAP meeting.
     *
     * @param  \App\LUCS_User  $user
     * @param  \App\IPAPMeeting  $iPAPMeeting
     * @return mixed
     */
    public function forceDelete(LUCS_User $user, IPAPMeeting $iPAPMeeting)
    {
        //
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Document extends Model
{
    protected $connection = 'pgrf';
    protected $table = 'documents';
    protected $fillable = ['student_username', 'workflow_type_id', 'Location'];
    
    public $timestamps = false;

    public function feedback()
    {
        return $this->hasMany('App\Feedback', 'DocumentId');
    }

    public function student()
    {
        return $this->belongsTo('App\Student', 'student_username');
    }

    public function type()
    {
        return $this->belongsTo('App\WorkflowType', 'workflow_type_id');
    }

    public function isEndOfYearReport(): bool
    {
        if (str_contains($this->type->value, 'End of Year Report')) {
            return true;
        }

        return false;
    }

    public function upload($file): bool
    {
        $result = Storage::disk('public')->putFileAs(
            $this->getDirectory(),
            $file,
            $file->hashName()
        );
        
        $this->Location = $file->hashName();

        return $result;
    }

    public function getLocationAttribute($value)
    {
        return $this->getDirectory() . $value;
    }

    private function getDirectory()
    {
        return $this->student->username . '/';
    }
}

<?php

namespace App\Listeners;

use App\Events\Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class SendExemptionGrantedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() { }

    /**
     * Handle the event.
     *
     * @param  \App\Events\DocumentSubmitted  $event
     * @return void
     */
    public function handle(\App\Events\ExemptionGranted $event)
    {
        $to_notify = collect([$event->workflow->student]);

        foreach ($event->workflow->student->supervisors()->get() as $staff) {
            $to_notify->push($staff);
        }
        
        Notification::send($to_notify, new \App\Notifications\ExemptionGranted($event->workflow->student));
    }
}

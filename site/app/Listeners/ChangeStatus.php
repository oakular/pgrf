<?php

namespace App\Listeners;

use App\Events\DocumentSubmitted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChangeStatus
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DocumentSubmitted  $event
     * @return void
     */
    public function handle(\App\Events\ChangeableStatus $event)
    {
        $this->changeStatus($event);
    }

    /**
     * Change the status of the student, if an event requires it
     * @param $event
     * @return void
     */
    private function changeStatus($event)
    {
        $event->workflow->status()->associate($event->getNextStatus())->save();
    }
}

<?php

namespace App\Listeners;

use App\Events\Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class SendExemptionRequestedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() { }

    /**
     * Handle the event.
     *
     * @param  \App\Events\DocumentSubmitted  $event
     * @return void
     */
    public function handle(\App\Events\ExemptionRequested $event)
    {
        Notification::send(
            \App\LUCS_User::admins()->get(),
            new \App\Notifications\ExemptionRequested($event->workflow->student)
        );
    }
}

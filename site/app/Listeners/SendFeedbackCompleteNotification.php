<?php

namespace App\Listeners;

use App\Events\Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class SendFeedbackCompleteNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() { }

    /**
     * Handle the event.
     *
     * @param  \App\Events\FeedbackComplete  $event
     * @return void
     */
    public function handle(\App\Events\FeedbackComplete $event)
    {
        logger('Send feedback complete notification for ' . $event->workflow->student);
        
        $student = $event->workflow->student;
        $to_notify = collect([$student]);

        foreach ($student->supervisors as $staff) {
            $to_notify->push($staff);
        }
        
        Notification::send($to_notify, new \App\Notifications\FeedbackComplete($event->workflow));
    }

    private function checkToNotify(\App\Workflow $workflow): bool
    {
        $feedback_complete_status = \App\Status::where(
            'name', 'LIKE', 'IPAP Feedback Complete')->first();

        if ($workflow->status->id == $feedback_complete_status->id) {
            return true;
        }

        return false;
    }
}

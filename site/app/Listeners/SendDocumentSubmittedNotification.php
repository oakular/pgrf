<?php

namespace App\Listeners;

use App\Events\Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class SendDocumentSubmittedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() { }

    /**
     * Handle the event.
     *
     * @param  \App\Events\DocumentSubmitted  $event
     * @return void
     */
    public function handle(\App\Events\DocumentSubmitted $event)
    {
        $student = $event->workflow->student;
        $to_notify = collect([$student]);

        foreach ($student->ipaps()->get() as $staff) {
            $to_notify->push($staff);
        }
        
        Notification::send($to_notify, new \App\Notifications\DocumentSubmitted($event->document));
    }
}

<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class FeedbackComplete extends ChangeableStatus
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $workflow;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(\App\Student $student, \App\Document $document)
    {
        $this->workflow = $student->workflows->where(
            'workflow_type_id', '=', $document->type->id
        )->first();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    public function getNextStatus()
    {
        return \App\Status::where('name', 'LIKE', 'IPAP Feedback Complete')->first()->id;
    }
}

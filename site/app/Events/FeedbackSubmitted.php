<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class FeedbackSubmitted extends ChangeableStatus
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $student;
    public $workflow;
    private $document;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(\App\Student $student, \App\Feedback $feedback)
    {
        logger('Creating \App\Events\FeedbackSubmitted instance for' . $student->username);
        $this->student = $student;
        $this->document = $feedback->document;

        $this->workflow = $student->workflows->where('workflow_type_id', '=', $feedback->document->type->id)->first();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    public function getNextStatus()
    {
        logger('Getting next feedback status');

        $incomplete_feedback = \App\Status::where('name', 'LIKE', 'IPAP Feedback Incomplete')->firstOrFail();
        switch ($this->workflow->status->id) {
        case $incomplete_feedback->id:
            return \App\Status::where('name', 'LIKE', 'IPAP Feedback Complete')->first()->id;
        case \App\Status::where('name', 'LIKE', 'IPAP Feedback Late')->first()->id:
            if ($this->document->feedback->count() != $this->document->student->ipaps->count()) {
                return \App\Status::where('name', 'LIKE', 'IPAP Feedback Incomplete')->first()->id;
            } else {
                return \App\Status::where('name', 'LIKE', 'IPAP Feedback Complete')->first()->id;
            }
        }

        return $incomplete_feedback->id;
    }
}

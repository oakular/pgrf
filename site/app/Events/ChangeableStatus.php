<?php

namespace App\Events;

abstract class ChangeableStatus
{
    public $student;
    public $workflow;

    /** 
     * Get the next status for the current workflow that has triggered the event
     * @return \App\Status;
     */
    abstract public function getNextStatus();
}

?>

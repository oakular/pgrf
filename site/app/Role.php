<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $connection = 'pgrf';
    protected $table = "roles";
    protected $guarded = [];

    public $timestamps = false;

    public function lucsUser()
    {
        return $this->belongsTo('App\LUCS_User', 'UserId');
    }
}

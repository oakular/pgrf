<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Notifications\Notifiable;

class Staff extends Model
{
    use Notifiable;
    
    public $timestamps = false;
    public $incrementing = false;

    protected $table = 'staff';
    protected $primaryKey = 'username';
    protected $keyType = 'string';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new \App\Scopes\StaffScope);
    }

    public function lucsUser()
    {
        return $this->belongsTo('App\LUCS_User', 'username');
    }

    public function supervisingStudents()
    {
        return $this->belongsToMany('App\Student', 'supervisor', 'staff_username', 'student_username');
    }

    public function ipapStudents()
    {
        return $this->belongsToMany('App\Student', 'ipap', 'staff_username', 'student_username');
    }

    public function documentsNeedingAttention()
    {
        return \App\Document::whereIn('student_username', $this->ipapStudents->pluck('username'))
                   ->whereDoesntHave('feedback', function (Builder $query) {
                       $query->where('staff_username', 'LIKE', $this->username);
                   })->whereHas('type', function (Builder $query) {
                       $query->where('requires_feedback', true);
                   })->get();
    }

    /**
     * Route notifications for the mail channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForMail($notification)
    {
        return $this->lucsUser->email;
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DocumentLateStaff extends Mailable
{
    use Queueable, SerializesModels;

    private $student;
    private $workflow_type;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(\App\WorkflowType $workflow_type, \App\Student $student)
    {
        $this->student = $student;
        $this->workflow_type = $workflow_type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'PGR: No Document Submitted by '
                 . $this->student->lucsUser->firstnames
                 . ' ' . $this->student->lucsUser->surname;
        
        return $this->subject($subject)
            ->markdown('mail.document.late.staff', [
                'url' => route('home'),
                'student' => $this->student,
                'workflow_type' => $this->workflow_type
            ]);
    }
}

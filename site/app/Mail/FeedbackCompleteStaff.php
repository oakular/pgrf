<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FeedbackCompleteStaff extends Mailable
{
    use Queueable, SerializesModels;

    private $student;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(\App\Student $student)
    {
        $this->student = $student;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'PGR: Feedback Ready for '
                 . $this->student->lucsUser->firstnames
                 . ' ' . $this->student->lucsUser->surname;
        
        return $this->markdown('mail.feedback.complete.staff', [
            'student' => $this->student,
            'url' => route('student.show', ['userId' => $this->student->id])
        ])->subject($subject);
    }
}

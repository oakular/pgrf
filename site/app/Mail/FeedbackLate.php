<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FeedbackLate extends Mailable
{
    use Queueable, SerializesModels;

    private $document;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($document)
    {
        $this->document = $document;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('PGR: Feedback Submission Required')
            ->markdown('mail.feedback.late', [
                'url' => route('feedback.enter', ['docId' => $this->document->id]),
                'student' => $this->document->student
            ]);
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DocumentSubmittedStaff extends Mailable
{
    use Queueable, SerializesModels;

    private $document;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($document)
    {
        $this->document = $document;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'PGR: Document Submitted by '
                 . $this->document->student->lucsUser->firstnames
                 . ' ' . $this->document->student->lucsUser->surname;
        
        return $this->markdown('mail.document.submitted.staff', [
            'url' => route('feedback.enter', ['docId' => $this->document->id]),
            'student' => $this->document->student
        ])->subject($subject);
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ExemptionGrantedStaff extends Mailable
{
    use Queueable, SerializesModels;

    private $student;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(\App\Student $student)
    {
        $this->student = $student;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'PGR: Exemption Granted for ' .
                 $this->student->lucsUser->firstnames . ' ' .
                 $this->student->lucsUser->surname;
        
        return $this->subject($subject)
            ->markdown('mail.exemption.granted.staff', [
                'student' => $this->student
            ]);
    }
}

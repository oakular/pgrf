<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DocumentLateStudent extends Mailable
{
    use Queueable, SerializesModels;

    private $workflow_type;
    private $student;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(\App\WorkflowType $workflow_type, \App\Student $student)
    {
        $this->student = $student;
        $this->workflow_type = $workflow_type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('PGR: Document Late')
            ->markdown('mail.document.late.student', [
                'url' => route('home'),
                'student' => $this->student,
                'workflow_type' => $this->workflow_type
            ]);
    }
}

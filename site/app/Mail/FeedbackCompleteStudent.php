<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FeedbackCompleteStudent extends Mailable
{
    use Queueable, SerializesModels;

    private $workflow;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(\App\Workflow $workflow)
    {
        $this->workflow = $workflow;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('PGR: Feedback Ready')
            ->markdown('mail.feedback.complete.student', [
                'document' => $this->workflow,
                'url' => route('student.show', ['userId' => $this->workflow->student->id])
            ]);
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DocumentSubmittedStudent extends Mailable
{
    use Queueable, SerializesModels;

    private $document;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($document)
    {
        $this->document = $document;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('PGR: Document Submission Receipt')
            ->markdown('mail.document.submitted.student', [
                'url' => route('document.show', ['docId' => $this->document->id])
            ]);
    }
}

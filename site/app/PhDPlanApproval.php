<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhDPlanApproval extends Model
{
    protected $connection = 'pgrf';
    protected $table = 'phd_plan_approvals';

    public function workflow()
    {
        return $this->belongsTo('App\Workflow', 'workflow_id');
    }
}

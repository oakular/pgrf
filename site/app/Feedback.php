<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Feedback extends Model
{
    protected $connection = 'pgrf';
    protected $table = 'feedback';
    public $timestamps = false;
    protected $guarded = [];

    public function document()
    {
        return $this->belongsTo('App\Document', 'DocumentId');
    }

    public function staff()
    {
        return $this->belongsTo('App\Staff', 'staff_username');
    }

    public function dataForProgressCommittee()
    {
        return $this->hasOne('App\DataForProgressCommittee', 'feedback_id');
    }

    public function grade()
    {
        return $this->hasOne('App\Grade', 'feedback_id');
    }

    public function saveGrade($grade_value)
    {
        $grade = new \App\Grade;
        $grade->value = $grade_value;
        $grade->student()->associate($this->document->student->detail->username);

        $this->grade()->save($grade);
    }

    public function getAnswers(): array
    {
        $answers = Storage::disk('public')->get($this->Location);
        $answers = json_decode($answers, true);
        return array_values($answers);
    }

    public function upload($data)
    {
        $json = json_encode($data);

        $this->Location = $this->getFeedbackFilePath();

        $result = Storage::disk('public')->put($this->Location, $json);
        abort_unless($result, 500, 'Failed to save feedback on server');

    }

    private function getFeedbackFilePath()
    {
        $type_value = snake_case($this->document->type->value);
        $file_name = "fb_{$type_value}_{$this->staff_username}";

        return $this->document->student->username . '/' . $file_name . '.json';
    }
}

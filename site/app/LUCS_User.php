<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class LUCS_User extends Authenticatable
{
    use Notifiable;
    
    public $timestamps = false;
    public $incrementing = false;

    protected $table = 'people';
    protected $primaryKey = 'username';
    protected $keyType = 'string';
    protected $guarded = [];

    public function student()
    {
        return $this->hasOne('App\Student', 'username');
    }

    public function staff()
    {
        return $this->hasOne('App\Staff', 'username');
    }        

    public function scopeAdmins($query)
    {
        return $query->whereIn('username', \App\Admin::all()->pluck('username'));
    }

    public function scopeStaffMembers($query)
    {
        $staff = \App\Staff::all()->pluck('username');

        return $query->whereIn('username', $staff);
    }

    public function isAdmin(): bool
    {
        return \App\Admin::where('username', 'LIKE', $this->username)->count() > 0;
    }
    
    public function isSupervisor(): bool
    {
        return \App\Supervisor::where('staff_username', 'LIKE', $this->username)->count() > 0;
    }
    
    public function isIpap(): bool
    {
        return \App\IPAP::where('staff_username', 'LIKE', $this->username)->count() > 0;
    }

    public function isStudent(): bool
    {
        return \App\Student::where('username', 'LIKE', $this->username)->count() > 0;
    }

    public function getFullNameOrUsername(): string
    {
        if (is_null($this->firstnames) || is_null($this->surname)) {
            return $this->username;
        }

        return $this->firstnames . ' ' . $this->surname;
    }

    public function getFullNameOrUsernameReversed(): string
    {
        if (is_null($this->firstnames) || is_null($this->surname)) {
            return $this->username;
        }

        return $this->surname . ' ' . $this->firstnames;
    }

    public function roles(): array
    {
        if ($this->isStudent()) { return ['Student']; }

        $roles = array();

        if ($this->isAdmin()) { array_push($roles, 'Admin'); }
        if ($this->isIPAP()) { array_push($roles, 'IPAP'); }
        if ($this->isSupervisor()) { array_push($roles, 'Supervisor'); }
        
        return $roles;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataForProgressCommittee extends Model
{
    protected $connection = 'pgrf';
    protected $table = 'data_for_progress_committee';

    public function feedback()
    {
        return $this->belongsTo('App\Feedback', 'feedback_id');
    }

    public function recommendation()
    {
        return $this->belongsTo('App\ProgressCommitteeRecommendation', 'progress_committee_recommendation_id');
    }
}

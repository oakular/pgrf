<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],
        'App\Events\DocumentDue' => [
          'App\Listeners\ChangeStatus'
        ],
        'App\Events\DocumentSubmitted' => [
            'App\Listeners\ChangeStatus',
            'App\Listeners\SendDocumentSubmittedNotification'
        ],
        'App\Events\DocumentLate' => [
          'App\Listeners\ChangeStatus'
        ],
        '\App\Events\FeedbackSubmitted' => [
            'App\Listeners\ChangeStatus'
        ],
        '\App\Events\FeedbackComplete' => [
            'App\Listeners\ChangeStatus'
        ],
        'App\Events\ExemptionRequested' => [
            'App\Listeners\ChangeStatus',
            'App\Listeners\SendExemptionRequestedNotification'
        ],
        'App\Events\ExemptionGranted' => [
            'App\Listeners\ChangeStatus',
            'App\Listeners\SendExemptionGrantedNotification'
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}

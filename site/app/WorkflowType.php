<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkflowType extends Model
{
    protected $connection = 'pgrf';
    
    public $timestamps = false;

    public function statuses()
    {
        return $this->belongsToMany('App\Status', 'status_workflow_type', 'workflow_type_id', 'status_id');
    }

    public function workflows()
    {
        return $this->hasMany('App\Workflow', 'workflow_type_id');
    }

    public function documents()
    {
        return $this->hasMany('App\Document', 'workflow_type_id');
    }

    public function questionnaire()
    {
        return $this->hasOne('App\Questionnaire', 'workflow_type_id');
    }

    public function scopeReport($query)
    {
        return $query->where('value', 'LIKE', '%Report%');
    }
}

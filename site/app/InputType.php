<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InputType extends Model
{
    protected $connection = 'pgrf';
    public $timestamps = false;

    public function questions()
    {
        return $this->hasMany('App\Question');
    }
}

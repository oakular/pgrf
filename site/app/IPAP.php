<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IPAP extends Model
{
    protected $connection = 'pgrf';
    protected $table = 'ipap';
    public $timestamps = false;

    public function student()
    {
        return $this->belongsTo('App\Student', 'student_username');
    }

    public function feedback()
    {
        return $this->hasMany('App\Feedback', 'staff_username');
    }

    public function staff()
    {
        return $this->belongsTo('App\Staff', 'staff_username');
    }
}

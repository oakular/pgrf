<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Workflow extends Model
{
    use SoftDeletes;
    
    protected $connection = 'pgrf';
    protected $dates = ['deleted_at', 'deadline'];
    
    public function status()
    {
        return $this->belongsTo('App\Status', 'status_id');
    }

    public function student()
    {
        return $this->belongsTo('App\Student', 'student_username');
    }

    public function type()
    {
        return $this->belongsTo('App\WorkflowType', 'workflow_type_id');
    }
    
    public function ipapMeeting()
    {
        return $this->hasOne('App\IPAPMeeting', 'workflow_id');
    }

    public function getDeadline($value): \DateTime
    {
        return date_create($value);
    }

    public function isDue(): bool
    {
        return $this->compareDeadlineTo(date_create('+1 month'));
    }

    public function isLate(): bool
    {
        return $this->compareDeadlineTo(date_create('yesterday'));
    }

    private function compareDeadlineTo(\DateTime $date)
    {
        return $this->deadline->format('Y-m-d') == $date->format('Y-m-d');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supervisor extends Model
{
    public $timestamps = false;

    protected $connection = 'pgrf';
    protected $table = 'supervisor';

    public function student()
    {
        return $this->belongsTo('App\Student', 'student_username');
    }

    public function staff()
    {
        return $this->belongsTo('App\Staff', 'staff_username');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $connection = 'pgrf';

    public $timestamps = false;
    public $incrementing = false;

    protected $table = "status";

    public function next()
    {
        return $this->hasOne('App\Status', '');
    }

    public function workflow()
    {
        return $this->hasMany('App\Workflow', 'status_id');
    }

    public function workflowType()
    {
        return $this->belongsToMany('App\WorkflowType',
                                    'status_workflow_type',
                                    'status_id', 'workflow_type_id');
    }
}

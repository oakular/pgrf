<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgressCommitteeRecommendation extends Model
{
    protected $connection = 'pgrf';
}

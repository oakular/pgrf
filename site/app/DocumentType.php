<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentType extends Model
{
    protected $connection = 'pgrf';
    public $timestamps = false;
    
    protected $fillable = ['value'];

    public function questionnaires()
    {
        return $this->hasMany('App\Questionnaire', 'document_type_id');
    }
}

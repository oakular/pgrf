<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $connection = 'pgrf';
    
    public $timestamps = false;

    public function questionnaires()
    {
        return $this->belongsToMany('App\Questionnaire', 'questions_questionnaires');
    }

    public function inputType()
    {
        return $this->belongsTo('App\InputType', 'input_type_id');
    }
}

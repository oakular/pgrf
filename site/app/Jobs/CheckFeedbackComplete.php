<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Notification;

class CheckFeedbackComplete implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $documents;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(iterable $documents)
    {
        $this->documents = $documents;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->documents as $document) {
            if ($this->checkFeedbackIsComplete($document)) { continue; }

            $this->checkForLateFeedback($document);
        }
    }

    private function checkForLateFeedback(\App\Document $document)
    {
        $result = strcmp(
            date_create($document->created_at)->format('Y-m-d'),
            date_create('-2 weeks')->format('Y-m-d')
        );

        // Only check for uploads exactly two weeks old
        if ($result !== 0) { return; }
            
        $to_notify = array();

        foreach (\App\LUCS_User::admins()->get() as $admin) {
            array_push($to_notify, $admin);
        }
            
        foreach ($document->student->ipaps as $ipap) {
            $feedback_num = \App\Feedback::where([
                ['staff_username', 'LIKE', $ipap->username],
                ['DocumentId', '=', $document->id]
            ])->count();

            if ($feedback_num > 0) { continue; }

            array_push($to_notify, $ipap);
        }

        Notification::send($to_notify, new \App\Notifications\FeedbackLate($document));

    }

    private function checkFeedbackIsComplete(\App\Document $document): bool
    {
        logger('Checking feedback is complete for: ' . $document->id);

        $ipaps = $document->student->ipaps()->get();

        if ($document->feedback->count() != $ipaps->count()) {
            return false;
        }

        logger('Feedback is complete for ' . $document->id);
        // event(new \App\Events\FeedbackComplete($document->student, $document));


        $workflow = $document->student->workflows->where(
            'workflow_type_id', '=', $document->type->id
        )->first();

        $workflow->status()->associate(
            \App\Status::where('name', 'LIKE', 'IPAP Feedback Complete')->first()
        )->save();
        
        return true;
    }
}

<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class CheckLateDocuments extends ChecksDocuments
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $workflows;

    /**
     * Create a new CheckLateDocuments instance.
     * @param $workflows The set of workflows to check for late documents
     * @return void
     */
    public function __construct(iterable $workflows)
    {
        $this->workflows = $workflows;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        $late_workflows = array();

        foreach ($this->workflows as $workflow) {
            if (!$workflow->isLate()) { continue; }

            array_push($late_workflows, $workflow);
        }

        $this->notify($late_workflows);
    }

    protected function notify(iterable $late_workflows): void
    {
        foreach ($late_workflows as $workflow) {
            $student = $workflow->student;
            $workflow_type = $workflow->type;

            event(new \App\Events\DocumentLate($workflow));

            $to_notify = collect([$student]);

            foreach ($student->supervisors as $supervisor) {
                Log::debug('Adding supervisors to notify of late document submission');
                $to_notify->push($supervisor);
            }
                
            Log::debug('Sending DocumentLate notification to ' . $to_notify . ' for ' . $student->username);

            Notification::send($to_notify, new \App\Notifications\DocumentLate($workflow_type, $student));
        }
    }
}

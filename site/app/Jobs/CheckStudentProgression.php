<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Log;

class CheckStudentProgression implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(\App\Student $student)
    {
        $this->student = $student;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $to_notify = $this->getToNotify();

        $notification;

        $result = $this->student->detail->grades->contains('value', 'Unsatisfactory');

        if ($result) {
            Log::debug('Requiring Attention of Progress Committee for ' . $this->student->username);

            // TODO: Iterate over all workflows, handle progression for each type
            $this->student->workflow()->status = \App\Status::where('name', 'LIKE', 'Needs Attention of Progress Committee')->first();

            $notification = new \App\Notifications\ProgressBlocked($this->student);
        } else {
            $notification = new \App\Notifications\ProgressAllowed($this->student);
        }

        Notification::send($to_notify, $notification);
    }

    private function getToNotify(): \Illuminate\Support\Collection
    {
        $to_notify = collect([$this->student]);

        foreach ($this->student->supervisors as $supervisor) {
            $to_notify->push($supervisor);
        }

        return $to_notify;
    }
}

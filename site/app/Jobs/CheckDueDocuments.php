<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class CheckDueDocuments extends ChecksDocuments
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $workflows;

    /**
     * Create a new CheckLateDocuments instance.
     * @param $workflows The set of workflows to check for late documents
     * @return void
     */
    public function __construct(iterable $workflows)
    {
        $this->workflows = $workflows;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        $due_workflows = array();

        foreach ($this->workflows as $workflow) {
            if (!$workflow->isDue()) { continue; }

            array_push($due_workflows, $workflow);
        }

        $this->notify($due_workflows);
    }

    protected function notify(iterable $due_workflows): void
    {
        foreach ($due_workflows as $workflow) {
            event(new \App\Events\DocumentDue($workflow));

            Notification::send($workflow->student,
                               new \App\Notifications\DocumentDue($workflow->type));
        }
    }
}

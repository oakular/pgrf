<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Student extends Model
{
    use Notifiable;

    public $timestamps = false;
    public $incrementing = false;

    protected $table = 'staff';
    protected $primaryKey = 'username';
    protected $keyType = 'string';
    protected $guarded = [];

    protected $attributes = [
        'status' => 'phd',
        'office' => NULL
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new \App\Scopes\StudentScope);
    }
    
    public function lucsUser()
    {
        return $this->belongsTo('App\LUCS_User', 'username');
    }

    public function detail()
    {
        return $this->hasOne('App\StudentDetail', 'username');
    }
    
    public function ipaps()
    {
        return $this->belongsToMany('App\Staff', 'ipap', 'student_username', 'staff_username');
    }

    public function supervisors()
    {
        return $this->belongsToMany('App\Staff', 'supervisor', 'student_username', 'staff_username');
    }

    public function documents()
    {
        return $this->hasMany('App\Document', 'student_username');
    }

    public function workflows()
    {
        return $this->hasMany('App\Workflow', 'student_username');
    }

    public function canUpload(\App\WorkflowType $type): bool
    {
        if ($this->detail->YearOfStudy > 1 && $type->value == 'PhD Plan')
        {
            return false;
        }

        return true;
    }

    public function createNewStarterStudentDetail($start_date, \App\Department $department, bool $is_xjtlu = false, bool $is_part_time = false)
    {
        $detail = new \App\StudentDetail;
        $detail->StartDate = $start_date;
        $detail->YearOfStudy = 1;
        $detail->status_id = 1;
        $detail->department()->associate($department);
        $detail->is_xjtlu = $is_xjtlu;
        $detail->is_part_time = $is_part_time;

        $this->detail()->save($detail);
    }

    /**
     * Route notifications for the mail channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForMail($notification)
    {
        return $this->lucsUser->email;
    }
}

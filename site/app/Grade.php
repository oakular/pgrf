<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $connection = 'pgrf';
    public $timestamps = false;
    protected $table = 'grades';

    public function student()
    {
        return $this->belongsTo('App\StudentDetail', 'student_detail_username');
    }

    public function feedback()
    {
        return $this->belongsTo('App\Feedback', 'feedback_id');
    }

    public function isUnsatisfactory()
    {
        if ($this->value === 'Unsatisfactory') { return true; }

        return false;
    }
}

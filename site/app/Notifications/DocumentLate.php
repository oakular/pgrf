<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Log;

class DocumentLate extends Notification
{
    // use Queueable;

    public $workflow_type;
    public $student;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(\App\WorkflowType $workflow_type, \App\Student $student)
    {
        $this->workflow_type = $workflow_type;
        $this->student = $student;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        Log::debug('Preparing to send DocumentLate email to ' . $notifiable->username);

        if (is_a($notifiable, '\App\Student')) {
            return (new MailMessage)
                ->subject('PGR: Document Late')
                ->markdown('mail.document.late.student', [
                    'url' => route('home'),
                    'student' => $this->student,
                    'workflow_type' => $this->workflow_type
                ]);
        } else if (is_a($notifiable, '\App\Staff')) {
            return (new MailMessage)
                ->subject('PGR: No Document Submitted by ' . $this->student->lucsUser->firstnames . ' ' . $this->student->lucsUser->surname)
                ->markdown('mail.document.late.staff', [
                    'url' => route('home'),
                    'student' => $this->student,
                    'workflow_type' => $this->workflow_type
                ]);
        }

        Log::debug('No DocumentLate email to send to ' . $notifiable->username);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ProgressAllowed extends Notification
{
    // use Queueable;

    public $student;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(\App\Student $student)
    {
        $this->student = $student;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if (is_a($notifiable, '\App\Student')) {
            return (new MailMessage)
                ->subject('PGR: Progression Confirmed!')
                ->markdown('mail.progress.allowed.student');
        } else if (is_a($notifiable, '\App\Staff')) {
            return (new MailMessage)
                ->subject('PGR: Progression Allowed for ' . $this->student->lucsUser->firstnames . ' ' . $this->student->lucsUser->surname)
                ->markdown('mail.progress.allowed.staff');
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

<?php

namespace App\Helpers;

class StatusColorHelper
{
    public static function shouldWarn(\App\Status $status): bool
    {
        if (str_contains($status->name, 'Not Due')) { return false; }

        if (str_contains($status->name, 'Late')) { return true; }

        if (str_contains($status->name, 'Report Due')) { return true; }

        if (str_contains($status->name, 'Due')) { return true; }

        return false;
    }
}

?>

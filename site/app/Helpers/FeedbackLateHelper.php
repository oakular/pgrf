<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Notification;

class FeedbackLateHelper
{
    public static function check(\App\Student $student): void
    {
        $document = \App\Document::where([
            ['student_username', 'LIKE', $student->username],
            ['created_at', '=', date_create('-1 fortnight')]
        ])->first();

        if (is_null($document)) { return; }

        $late_ipaps = array();
        foreach ($student->ipaps()->get() as $ipap) {
            $count = \App\Feedback::where([
                ['DocumentId', '=', $document->id],
                ['staff_username', '=', $ipap->username]
            ])->count();

            if ($count === 0) {
                array_push($late_ipaps, $ipap);
            }
        }

        if (count($late_ipaps) === 0) { return; }

        Notification::send($late_ipaps, new \App\Notifications\FeedbackLate($document));
    }
}

?>

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IPAPMeeting extends Model
{
    protected $connection = 'pgrf';
    protected $table = 'ipap_meetings';

    public function workflow()
    {
        return $this->belongsTo('App\Workflow', 'workflow_id');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Storage;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return \App\Document::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        abort_unless(auth()->user()->isStudent(), 403, 'Only students can upload documents');

        $validator = $request->validate(['document' => 'required|mimes:pdf|max:10000']);

        $type = \App\WorkflowType::findOrFail($request->documentType);

        $result = auth()->user()->student->canUpload($type);
        abort_unless($result, 403, "You cannot upload a document of type {$type->value}");

        $document = \App\Document::firstOrNew([
            'student_username' => auth()->user()->username,
            'workflow_type_id' => $type->id
        ]);

        $result = $document->upload($request->document);
        abort_unless($result, 500, 'Failed saving document on the server');

        $document->save();

        event(new \App\Events\DocumentSubmitted(auth()->user()->student, $document));

        return redirect()->route('home')->with('flashMsg', 'Your document was uploaded successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $doc = \App\Document::findOrFail($id);

        return response()->file(public_path('storage/' . $doc->Location, ['Content-Disposition' => 'inline; filename="'. $doc->type->value .'"']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

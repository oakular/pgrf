<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cookie;

use App\Student;
use App\Staff;
use App\IPAP;
use App\Document;
use App\Feedback;
use App\LUCS_User;
use App\Helpers\FeedbackHelper;

class FeedbackController extends Controller
{
    public function create($docId)
    {
        $document = Document::findOrFail($docId);

        return view('feedback.create', [
            'title' => 'Feedback View',
            'document' => $document,
            'questions' => $document->type->questionnaire->questions->all()
        ]);
    }

    public function show($feedbackId, $studentId)
    {
        $feedback = Feedback::find($feedbackId);

        return view('feedback.view', [
            'title' => 'Feedback View',
            'feedback' => $feedback,
            'document' => $feedback->document,
            'questions' => $feedback->document->type->questionnaire->questions->all(),
            'answers' => $feedback->getAnswers()
        ]);
    }
    
    public function store(Request $request)
    {
        $this->validateRequest($request);

        $document = Document::findOrFail($request->input('docId'));
        
        $feedback = \App\Feedback::make([
            'staff_username' => auth()->user()->username,
            'DocumentId' => $document->id,
        ]);

        $feedback->upload($request->comment);

        $feedback->save();
        event(new \App\Events\FeedbackSubmitted($document->student, $feedback));
        
        if ($document->isEndOfYearReport()) {
            $feedback->saveGrade($request->grade);

            if ($feedback->grade->isUnsatisfactory()) {
                return redirect()->route('feedback.committee_data.create', ['feedback' => $feedback->id]);
            }
        }

        return redirect()->route('home')->with('flashMsg', 'Your feedback was uploaded successfully');
    }

    public function validateRequest(Request $request)
    {
        $rules = ['comment.*' => 'required'];
        $messages = ['required' => 'All fields must contain some feedback'];
        $validator = Validator::make($request->all(), $rules, $messages)->validate();
    }
}

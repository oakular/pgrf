<?php

namespace App\Http\Controllers;

use App\PhDPlanApproval;
use Illuminate\Http\Request;

class PhDPlanApprovalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $workflow = \App\Workflow::findOrFail($request->input('workflow-id'));

        $result = $workflow->student->supervisors->contains('username', auth()->user()->username);
        abort_unless($result, 403, "You must be a supervisor of this student to approve their PhD Plan");

        $approval = new \App\PhDPlanApproval;

        $approval->plan_description = $request->input('plan-description');

        if ($request->has('resource-description')) {
            $approval->resource_description = $request->input('resource-description');
        }

        if ($request->has('risk-assessment-is-complete')) {
            $approval->risk_assessment_date = $request->input('risk-assessment-date');
        }

        if ($request->has('is-confimed')) {
            $approval->is_confirmed = true;
        } else {
            $approval->is_confirmed = false;
        }

        $approval->workflow()->associate($workflow);

        $approval->save();

        return redirect()->route('home')->with(
            'flashMsg',
            "The {$workflow->type->value} has been approved"
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PhDPlanApproval  $phDPlanApproval
     * @return \Illuminate\Http\Response
     */
    public function show(PhDPlanApproval $phDPlanApproval)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PhDPlanApproval  $phDPlanApproval
     * @return \Illuminate\Http\Response
     */
    public function edit(PhDPlanApproval $phDPlanApproval)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PhDPlanApproval  $phDPlanApproval
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PhDPlanApproval $phDPlanApproval)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PhDPlanApproval  $phDPlanApproval
     * @return \Illuminate\Http\Response
     */
    public function destroy(PhDPlanApproval $phDPlanApproval)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StaffViewsController extends Controller
{
    public function showProgressCommitteeView()
    {
        $workflows = \App\ProgressCommitteeMember::findOrFail(auth()->user()->username)->workflows;
        return view('staff_views.progress_committee', ['workflows' => $workflows]);
    }

    public function showIpapStudentsView()
    {
        return view('staff_views.ipap_students');
    }

    public function showSupervisingStudentsView()
    {
        return view('staff_views.supervising_students');
    }
}

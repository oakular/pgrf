<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Student;
use App\Staff;
use App\IPAP;
use App\Supervisor;
use App\LUCS_User;

/* use Illuminate\Foundation\Bus\DispatchesJobs;
 * use Illuminate\Routing\Controller as BaseController;
 * use Illuminate\Foundation\Validation\ValidatesRequests;
 * use Illuminate\Foundation\Auth\Access\AuthorizesRequests;*/

class HomeController extends Controller
{
    public function show(Request $request)
    {
        $user = auth()->user();

        if ($user->isStudent()) {
            return view('home.student', ['user' => $user]);
        }

        return view ('home.staff');
    }

    private function render_ipap($user)
    {
        // $ipap = IPAP::where('staff_username', $user->username)->first();
        // $students = $ipap->students()->get();
        $students = $user->staff->students()->get();
        
        return view('home.staff', ['students' => $students]);
    }
    
    private function render_supervisor($user)
    {
        // $supervisor = Supervisor::where(
        //     'staff_username',
        //     $user->username
        // )->first();

        // $students = $supervisor->students()->get();
        $students = $user->staff->students()->get();

        return view('home.staff', ['students' => $students]);
    }
}

?>

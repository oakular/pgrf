<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProgressCommitteeController extends Controller
{
    public function store(Request $request)
    {
        abort_unless(auth()->user()->isAdmin(), 403);
        
        $request->committee->each(function ($member) use ($request) {
            $member = \App\ProgressCommitteeMember::findOrFail($member->username);
            $member->workflows()->attach($request->workflow);
        });

        return redirect()->route('home')->with('flashMsg', 'Progress Committee assigned');
    }
}

<?php

namespace App\Http\Controllers;

use App\LUCS_User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        abort_unless(auth()->user()->isAdmin(), 403, 'You cannot view all users');
        
        return view('user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LUCS_User  $lUCS_User
     * @return \Illuminate\Http\Response
     */
    public function show(LUCS_User $lUCS_User)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LUCS_User  $lUCS_User
     * @return \Illuminate\Http\Response
     */
    public function edit(LUCS_User $lUCS_User)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LUCS_User  $lUCS_User
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LUCS_User $lUCS_User)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LUCS_User  $lUCS_User
     * @return \Illuminate\Http\Response
     */
    public function destroy(LUCS_User $lUCS_User)
    {
        //
    }
}

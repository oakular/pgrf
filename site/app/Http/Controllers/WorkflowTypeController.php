<?php

namespace App\Http\Controllers;

use App\WorkflowType;
use Illuminate\Http\Request;

class WorkflowTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WorkflowType  $workflowType
     * @return \Illuminate\Http\Response
     */
    public function show(WorkflowType $workflowType)
    {
        return view('workflow_types.phd_plan.show', [
            'workflow_type' => $workflowType
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WorkflowType  $workflowType
     * @return \Illuminate\Http\Response
     */
    public function edit(WorkflowType $workflowType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WorkflowType  $workflowType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WorkflowType $workflowType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WorkflowType  $workflowType
     * @return \Illuminate\Http\Response
     */
    public function destroy(WorkflowType $workflowType)
    {
        //
    }
}

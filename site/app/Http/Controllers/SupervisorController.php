<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SupervisorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $student = \App\Student::findOrFail($request->student);

        if ($student->supervisors->contains('username', $request->supervisor)) {
            $supervisor_user = \App\LUCS_User::find($request->supervisor);
            return back()->with('errorMsg', "{$supervisor_user->getFullNameOrUsername()} is already a supervisor of {$student->lucsUser->getFullNameOrUsername()}");
        }
        
        $student->supervisors()->attach($request->supervisor);
            
        return redirect()->route('student.show', ['id' => $student->username]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($staff, $student)
    {
        $student = \App\Student::findOrFail($student);

        $student->supervisors()->detach($staff);

        return redirect()->route('student.show', ['id' => $student->username]);
    }
}

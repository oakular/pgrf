<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class MasqueradeController extends Controller
{
    public function masqueradeAs($username)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        $masquerading_user_username = auth()->user()->username;

        Auth::logout();
        Auth::loginUsingId($username);
        Cookie::queue(Cookie::make('masquerading_user', $masquerading_user_username));

        return redirect()->route('home');
    }

    public function stopMasquerade()
    {
        abort_unless(Cookie::has('masquerading_user'), 404, 'No user is being masqueraded');

        $username = Cookie::get('masquerading_user');
        Cookie::queue(Cookie::forget('masquerading_user'));
        Auth::loginUsingId($username);
        
        return redirect()->route('user.index');
    }
}

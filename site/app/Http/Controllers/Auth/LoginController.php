<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Storage;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return redirect('https://student.csc.liv.ac.uk/login.php?PATH='. route('login.success'));
    }

    public function login()
    {
        if(!App::environment('local')) {
            $uname = str_before($_COOKIE['LUCS_Login'], '-');
        }

        if (!Auth::loginUsingId($uname)) {
            abort(403, 'Not authenticated');
        }

        $uname = auth()->user()->username;

        if (!Storage::disk('public')->exists($uname)) {
            Storage::disk('public')->makeDirectory($uname);
            Storage::disk('public')->makeDirectory($uname . '/' . 'feedback');
        }
        
        return redirect()->route('home');
    }
}

<?php

namespace App\Http\Controllers;

use App\Workflow;
use Illuminate\Http\Request;

class WorkflowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(\App\Student $student)
    {
        return view('workflow.create', ['student' => $student]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        abort_unless(auth()->user()->isAdmin(), 403, 'Only admins can create new student workflows');

        $student = \App\Student::findOrFail($request->input('student-username'));

        $workflow = new \App\Workflow;
        $workflow->student()->associate($student);
        $workflow->status()->associate(\App\Status::findOrFail($request->input('status')));
        $workflow->type()->associate(\App\WorkflowType::findOrFail($request->input('workflow-type')));
        $workflow->deadline = $request->input('deadline');

        $workflow->save();
        
        return redirect()->route('student.show', ['student' => $student->username]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Workflow  $workflow
     * @return \Illuminate\Http\Response
     */
    public function show(Workflow $workflow)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Workflow  $workflow
     * @return \Illuminate\Http\Response
     */
    public function edit(Workflow $workflow)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Workflow  $workflow
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Workflow $workflow)
    {
        abort_unless(auth()->user()->isAdmin(), 403, 'Only admins may edit workflows');
        
        if ($request->filled('deadline')) {
            $workflow->deadline = $request->deadline;
        }

        if ($request->filled('status')) {
            $workflow->status()->associate(\App\Status::findOrFail($request->status));
        }
        
        $workflow->save();
        
        return redirect()->route('student.show', ['student' => $workflow->student->username])->with('flashMsg', $workflow->type->value . ' updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Workflow  $workflow
     * @return \Illuminate\Http\Response
     */
    public function destroy(Workflow $workflow)
    {
        abort_unless(auth()->user()->isAdmin(), 403, 'Only admins may delete workflows');
        
        $type = $workflow->type;
        $workflow->delete();

        return redirect()->back()->with('flashMsg', $type->value . ' deleted successfully');
    }
}

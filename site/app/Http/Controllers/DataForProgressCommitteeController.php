<?php

namespace App\Http\Controllers;

use App\DataForProgressCommittee;
use Illuminate\Http\Request;

class DataForProgressCommitteeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(\App\Feedback $feedback)
    {
        return view('feedback.data_for_progress_committee.create', ['feedback' => $feedback]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $feedback = \App\Feedback::findOrFail($request->feedback);

        $pc_data = new \App\DataForProgressCommittee;
        $pc_data->comments = $request->input('comment');
        $pc_data->feedback()->associate($feedback);
        $pc_data->recommendation()->associate(\App\ProgressCommitteeRecommendation::findOrFail($request->recommendation));
        $pc_data->save();

        event(new \App\Events\FeedbackSubmitted($feedback->document->student, $feedback));

        return redirect()->route('home')->with('flashMsg', 'Your feedback was uploaded successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DataForProgressCommittee  $dataForProgressCommittee
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = \App\DataForProgressCommittee::findOrFail($id);
        return view('feedback.data_for_progress_committee.show', ['data' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DataForProgressCommittee  $dataForProgressCommittee
     * @return \Illuminate\Http\Response
     */
    public function edit(DataForProgressCommittee $dataForProgressCommittee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DataForProgressCommittee  $dataForProgressCommittee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DataForProgressCommittee $dataForProgressCommittee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DataForProgressCommittee  $dataForProgressCommittee
     * @return \Illuminate\Http\Response
     */
    public function destroy(DataForProgressCommittee $dataForProgressCommittee)
    {
        //
    }
}

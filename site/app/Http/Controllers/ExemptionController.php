<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ExemptionController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @return \Illuminate\Http\Response
     */
    public function request($workflow_id)
    {
        $workflow = \App\Workflow::findOrFail($workflow_id);
        $student = $workflow->student;
        $supervisors = $student->supervisors;

        $result = $student->supervisors->contains('username', auth()->user()->username);
        abort_unless($result, 403);

        event(new \App\Events\ExemptionRequested($workflow));

        return redirect()->back()->with('flashMsg', 'Your exemption has been requested');
    }

    public function grant($student_username)
    {
        $student = \App\Student::findOrFail($student_username);

        abort_unless(auth()->user()->isAdmin(), 403);

        $workflow = $student->workflows->where(
            'status_id', \App\Status::where('name', 'LIKE', 'Exemption Requested')->first()->id
        )->first();

        event(new \App\Events\ExemptionGranted($workflow));

        return route('home');
    }
}

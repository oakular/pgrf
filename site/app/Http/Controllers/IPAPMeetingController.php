<?php

namespace App\Http\Controllers;

use App\IPAPMeeting;
use Illuminate\Http\Request;

class IPAPMeetingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $workflow = \App\Workflow::findOrFail($request->workflow);
        
        abort_unless($workflow->student->supervisors->contains('username', auth()->user()->username), 403);

        $meeting = new \App\IPAPMeeting;
        $meeting->workflow()->associate($workflow);
        $meeting->date = $request->input('meeting-date');
        $meeting->save();

        return redirect()->route('student.show', ['student_username' => $workflow->student->username]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\IPAPMeeting  $ipap_meeting
     * @return \Illuminate\Http\Response
     */
    public function show(IPAPMeeting $ipap_meeting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\IPAPMeeting  $ipap_meeting
     * @return \Illuminate\Http\Response
     */
    public function edit(IPAPMeeting $ipap_meeting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\IPAPMeeting  $ipap_meeting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Int $ipap_meeting)
    {
        $ipap_meeting = \App\IPAPMeeting::findOrFail($ipap_meeting);

        abort_unless(auth()->user()->can('update', $ipap_meeting), 403);

        if (!is_null($request->input('meeting-date'))) { 
            $ipap_meeting->date = $request->input('meeting-date');
        }

        if ($request->has('is-completed')) {
            $ipap_meeting->is_completed = true;
        }

        $ipap_meeting->save();

        return redirect()->route('student.show', ['student_username' => $ipap_meeting->workflow->student->username]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IPAPMeeting  $ipap_meeting
     * @return \Illuminate\Http\Response
     */
    public function destroy(IPAPMeeting $ipap_meeting)
    {
        //
    }
}

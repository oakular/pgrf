<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Carbon;

use App\Student;

/* use Illuminate\Foundation\Bus\DispatchesJobs;
 * use Illuminate\Routing\Controller as BaseController;
 * use Illuminate\Foundation\Validation\ValidatesRequests;
 * use Illuminate\Foundation\Auth\Access\AuthorizesRequests;*/

class StudentController extends Controller
{
    public function index()
    {
        return view('student.index');
    }

    public function show($id)
    {
        abort_unless(auth()->user()->can('view', \App\Student::find($id)), 403, 'Not authenticated');

        $student = \App\LUCS_User::findOrFail($id);

        abort_unless($student->student->status === 'phd', 404);

        return view('student.show', [
            'student' => $student,
        ]);
    }

    public function create()
    {
        abort_unless(auth()->user()->can('create', \App\Student::class), 403, 'Not authenticated');

        return view('student.create');
    }

    public function store(Request $request)
    {
        $this->validateStoreRequest($request);
        
        $user = new \App\LUCS_User;
        $user->username = $request->username;
        $user->firstnames = $request->firstName;
        $user->surname = $request->lastName;
        $user->email = $request->email . '@liverpool.ac.uk';
        $user->save();

        $user->student()->save(\App\Student::make());

        $user->student->createNewStarterStudentDetail(
            $request->startDate,
            \App\Department::findOrFail($request->department),
            $request->has('is-xjtlu'),
            $request->has('is-part-time')
        );

        if ($request->has('ipaps')) {
            foreach ($request->ipaps as $username) {
                if (strcmp($username, 'null') == 0) { continue; }

                $user->student->ipaps()->attach($username);
            }
        }

        if ($request->has('supervisors')) {
            foreach ($request->supervisors as $username) {
                if (strcmp($username, 'null') == 0) { continue; }
                
                $user->student->supervisors()->attach($username);
            }
        }

        return redirect()->route('student.show', ['id' => $user->username])->with('flashMsg', 'A new student was added successfully');
    }

    public function createBulk()
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        return view('student.create_bulk');
    }

    public function storeBulk(Request $request)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        $rules = ['student-data' => 'required|mimes:csv,plain'];
        $messages = ['student-data.mimes' => 'The uploaded file must be in CSV format'];

        Validator::make($request->all(), $rules, $messages);

        $csv = $this->csvDataToArray($request->file('student-data'));
        
        foreach ($csv as $data) {
            $user = \App\LUCS_User::make([
                'username' => $data['username'] ?? '',
                'firstnames' => $data['firstnames'] ?? NULL,
                'surname' => $data['surname'] ?? NULL,
                'email' => $data['email'] ?? NULL

            ] ?? NULL);

            $user->student()->save(\App\Student::make());
            $user->student->createNewStarterStudentDetail(
                $data['start-date'] ?? NULL,
                \App\Department::where('value', 'LIKE', $data['department'] ?? '')->first(),
                $data['is-xjtlu'] ?? false,
                $data['is-part-time'] ?? false
            );

            $user->save();
        }

        return redirect()->route('home')->with('flashMsg', 'The students have been created successfully');
        }

    public function edit($id)
    {
        abort_unless(auth()->user()->can('update', \App\Student::find($id)), 403, 'Not authenticated');

        $student = \App\Student::findOrFail($id);

        return view('student.edit', ['student' => $student]);
    }

    public function update(Request $request, $id)
    {
        $this->validateUpdateRequest($request);

        $student = \App\Student::findOrFail($id);

        abort_unless(auth()->user()->can('update', $student), 403, 'Only admins may update student data');

        $student->lucsUser->update([
            'firstnames' => $request->firstName,
            'surname' => $request->lastName,
            'email' => $request->email . '@liverpool.ac.uk',
        ]);

        $student->detail->update([
            'StartDate' => date('Y-m-d', strtotime($request->startDate)),
            'YearOfStudy' => $request->yearOfStudy
        ]);

        return redirect()->route('student.show', ['id' => $student->username])->with('flashMsg', 'Student details updated successfully');
    }

    private function validateStoreRequest(Request $request)
    {
        abort_unless(auth()->user()->can('create', \App\Student::class), 403, 'Not authenticated');

        $rules = [
            'firstName' => 'alpha',
            'lastName' => 'alpha',
            'username' => 'alpha_num',
            'email' => 'alpha_dash|unique:people,email',
            'startDate' => 'date',
            'supervisors.*' => 'nullable',
            'ipaps.*' => 'nullable',
            'is-xjtlu' => 'nullable',
            'is-part-time' => 'nullable'
        ];

        $messages = [
            'firstName.alpha' => "The student's first name must be entirely alphabetical characters",
            'lastName.alpha' => "The student's last name must be entirely alphabetical characters",
            'email.alpha_dash' => 'The email address can contain only letters, numbers, dashes and underscores',
            'ipaps.distinct' => 'The selected IPAP Members must be different',
            'supervisors.distinct' => 'The selected Supervisors must be different'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $validator->validate();
    }

    private function validateUpdateRequest(Request $request)
    {
        $rules = [
            'firstName' => 'alpha',
            'lastName' => 'alpha',
            'email' => 'unique:people,email',
        ];

        $messages = [
            'firstName.alpha' => "The student's first name must be entirely alphabetical characters",
            'lastName.alpha' => "The student's last name must be entirely alphabetical characters",
            'email.alpha_dash' => 'The email address can contain only letters, numbers, dashes and underscores',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();

    }

    private function csvDataToArray($csv_file): array
    {
        $csv = array_map('str_getcsv', file($csv_file));

        array_walk($csv, function(&$a) use ($csv) {
            $a = array_combine($csv[0], $a);
        });
        array_shift($csv); # remove column header

        return $csv;
    }
}

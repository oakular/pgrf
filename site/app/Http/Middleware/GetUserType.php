<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Cookie;

use Closure;
use App\LUCS_User;

class GetUserType
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $lucsUname = str_before(Cookie::get('LUCS_Login'), '-');
        Cookie::queue('uname', $lucsUname);

        /* $user = LUCS_User::where('Username', $lucsUname)->first();
         * Cookie::queue(Cookie::make('id', $user->id)); */

        return $response;
    }
}

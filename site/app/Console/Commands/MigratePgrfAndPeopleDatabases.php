<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MigratePgrfAndPeopleDatabases extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate both the pgrf and people databases';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $databases = ['pgrf', 'people'];
        
        foreach ($databases as $database) {
            $this->call('migrate:fresh', [
                '--database' => $database,
                '--path' => 'database/migrations/' . $database . '/'
            ]);
        }
    }
}

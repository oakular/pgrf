<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgressCommitteeMember extends LUCS_User
{
    public function workflows()
    {
        return $this->belongsToMany('\App\Workflow',
                                    'progress_committee_member_workflow',
                                    'progress_committee_member_username');
    }
}

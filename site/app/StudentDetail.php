<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentDetail extends Model
{
    public $timestamps = false;
    public $incrementing = false;

    protected $connection = 'pgrf';    
    protected $table = 'student_details';
    protected $primaryKey = 'username';
    protected $keyType = 'string';
    protected $guarded = [];

    public function status()
    {
        return $this->belongsTo('App\Status', 'status_id');
    }
    
    public function grades()
    {
        return $this->hasMany('App\Grade', 'student_detail_username');
    }

    public function department()
    {
        return $this->belongsTo('App\Department', 'department_id');
    }
}
